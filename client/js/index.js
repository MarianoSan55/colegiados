$(document).ready(function() {


    $('#datepicker').datepicker({
        dateFormat: "dd-mm-yy"
    });
    /* ----------------------------------evnetos para inicio de sesion--------------------------------*/
    $(".btn-iniciar").click(function() {
        var usuario = $("#usuario").val();
        var clave = $("#clave").val();
        var clave_ncryp = $.md5(clave);

        login(usuario, clave);

    });

    $('#clave').bind('keypress', function(e) {
        if (e.keyCode == 13) {
            var usuario = $("#usuario").val();
            var clave = $("#clave").val();
            var clave_ncryp = $.md5(clave);

            login(usuario, clave);
        }
    });

    $("#recuclave").click(function() {
        $(".c-form-recu").fadeIn("fast");
        $("#correo-recu").val('');
    });

    $(".cerrar-form-recu").click(function() {
        $(".c-form-recu").fadeOut("fast");
    });

    $(".btn-enviar-recu").click(function() {
        var correo_recu = $("#correo-recu").val();
        recuperar_clave(correo_recu);
    });

    $('.btn-modal-pago').click(function() {
        $('.c-modal-adelanto').fadeIn('fast');
    });

    $('.btn-cancelar-pago').click(function() {
        $('.c-modal-adelanto').fadeOut('fast');
    });

    $('.btn-ok-comunic').click(function() {
        $('.c-modal-comunicado').fadeOut('slow');
    });

    /* ----------------------------------eventos de mensajeria--------------------------------*/
    $('.c-mensajes').click(function() {
        var id_colegiado = $(this).attr("id_colegiado");
        $(".modal-noti").slideDown();
        load_mensajeria(id_colegiado);
    });


    eventos_panel();


    /* ----------------------------------eventos de vistas--------------------------------*/
    $(".c-loader").fadeOut("fast");

    /* ----------------------------------eventos pagos y refinanciacion-------------------*/
    /*eventos de calculo automatico*/

    var total_general = recalcular_monto();
    $(".total-comprobante").html("S/. " + total_general);
    $(".total-comprobante").attr("total_a_pagar", total_general);

    $(".btn-del-item-pago").click(function() {
        //var id_concepto_aportacion = $(this).attr("id_concepto_aportacion");

        $(this).parents("tr").remove();
        $(".btn-del-item-pago").last().attr("disabled", false);

        var total_general = recalcular_monto();
        $(".total-comprobante").html("S/. " + total_general);
        $(".total-comprobante").attr("total_a_pagar", total_general);
    });

    $(".input-nro-items").change(function() {
        var total_general = recalcular_monto();
        $(".total-comprobante").html("S/. " + total_general);
        $(".total-comprobante").attr("total_a_pagar", total_general);
    });

    $(".input-nro-items").keyup(function() {
        var total_general = recalcular_monto();
        $(".total-comprobante").html("S/. " + total_general);
        $(".total-comprobante").attr("total_a_pagar", total_general);
    });

    /*EVENTOS PARA REFINANCIACION EN ZONA DE PAGOS*/
    $(".btn-ver-cuotas-atra").click(function() {
        $(".c-lista-c-atrazadas").slideDown("slow");
    });
    $(".cerrar-lista").click(function() {
        $(".c-lista-c-atrazadas").slideUp("fast");
    });

    $("#id_year").change(function() {
        var year = $(this).val();
        var id_colegiado = $(this).attr('id_colegiado');
        load_cuotas(year, id_colegiado);
    });

    $(".ver-comprobante").click(function() {
        var id_aportacion = $(this).attr("id");
        var array_aportacion = id_aportacion.split("v");
        var url_boucher = $(this).attr("url_boucher");

        $("#vmod" + array_aportacion[1]).fadeIn("fast");

    });

    $(".cerrar-mod-img").click(function() {
        var id_aportacion = $(this).attr("id");
        var array_aportacion = id_aportacion.split("close");
        $("#vmod" + array_aportacion[1]).fadeOut("fast");

    });
    $('.check-cuota').click(function() {

        var id_cuota = $(this).attr("id");

        $(".modal-usu-confirm").slideDown("slow");
        $(".aceptar-dar-alta").attr("id_cuota", id_cuota);
    });
    $(".aceptar-dar-alta").click(function() {
        var id_cuota = $(this).attr("id_cuota");
        var id_colegiado = $(this).attr("id_colegiado");
        var id_year = $('#id_year').val();
        dar_de_alta(id_cuota, id_colegiado, id_year);
    });
    $('.btn-cerrar-confir').click(function() {
        $(".modal-usu-confirm").slideUp("slow");
    });

    $(".btn-add-con").click(function() {
        var id_colegiado = $(this).attr("id_colegiado");
        var id_concepto_aportacion = $(this).attr("id_concepto_aportacion");

        var nombre_concepto_aportacion = $(this).attr("nombre_concepto_aportacion");
        var cantidad_concepto_aportacion = $(this).attr("cantidad_concepto_aportacion");
        var duracion_meses = $(this).attr("duracion_meses");
        var fecha_inicio = $(this).attr("fecha_inicio");
        var tipo_aportacion = $(this).attr("tipo_aportacion");
        var id_exitente = $("#id-item-pagar" + id_concepto_aportacion).attr("class");
        var id_existe_onlig = 0;

        if (!id_exitente) {

            if (tipo_aportacion == 1) {
                $(".class-item-pagar").each(function() {
                    var valor_exis = $(this).attr("tipo_aportacion");
                    if (valor_exis == 1) {
                        id_existe_onlig++;
                    }
                });
            }

            //alert(tipo_aportacion+"---"+id_existe_onlig);

            if (id_existe_onlig == 0) {

                /*CONSULTAR POR CUOTAS REFINANCIADAS*/

                if (tipo_aportacion == 1 && duracion_meses == 1) {
                    $.ajax({
                        data: {
                            "id_colegiado": id_colegiado,
                            "consulta_ref": 'consulta_ref'
                        },
                        url: "../../client/views/ajax.php",
                        type: 'post',
                        beforeSend: function() {
                            $(".c-loader").fadeIn("fast");
                        },
                        success: function(data) {
                            var res = data.split("$");
                            var monto_cuota = '';
                            if (res[0] == '') {
                                res[0] = 0;
                            } else {
                                monto_cuota = '<p class="can-cuota-ref mb-0" title="Cuota de Refinanciacion" >S/. ' + res[1] + '</p>';
                            }


                            var item = '<tr class="class-item-pagar p-0" id_cuota_refinanciacion="' + data + '" duracion_meses="' + duracion_meses + '" tipo_aportacion="' + tipo_aportacion + '" id="id-item-pagar' + id_concepto_aportacion + '" id_concepto_aportacion="' + id_concepto_aportacion + '">' +
                                '<th class="p-0" scope="row">' + nombre_concepto_aportacion + '</th>' +
                                '<td class="p-0"><p class="id_fecha_inicio" fecha_inicio="' + fecha_inicio + '">' + fecha_inicio + '</p></td>' +
                                '<td class="p-0"><p class="id_cantidad_concepto_aportacion mb-0" cantidad_concepto_aportacion="' + cantidad_concepto_aportacion + '">S/. ' + cantidad_concepto_aportacion + '</p>' + monto_cuota + '</td>' +
                                '<td class="p-0"><input class="input-nro-items" type="text" num_c_pend="' + res[2] + '" value="1" onkeypress="return validanum(event)"/></td>' +
                                '<td class="p-0"><input class="btn-del-item-pago btn btn-danger p-0" type="button" id_concepto_aportacion="' + id_concepto_aportacion + '" value="X"></td>' +
                                '</tr>';
                            $("#lista-items-pago").append(item);

                            var total_general = recalcular_monto();
                            $(".total-comprobante").html("S/. " + total_general);
                            $(".total-comprobante").attr("total_a_pagar", total_general);

                            $(".btn-del-item-pago").click(function() {
                                //var id_concepto_aportacion = $(this).attr("id_concepto_aportacion");

                                $(this).parents("tr").remove();
                                $(".btn-del-item-pago").last().attr("disabled", false);

                                var total_general = recalcular_monto();
                                $(".total-comprobante").html("S/. " + total_general);
                                $(".total-comprobante").attr("total_a_pagar", total_general);
                            });

                            $(".input-nro-items").change(function() {
                                var total_general = recalcular_monto();
                                $(".total-comprobante").html("S/. " + total_general);
                                $(".total-comprobante").attr("total_a_pagar", total_general);
                            });

                            $(".input-nro-items").keyup(function() {
                                var total_general = recalcular_monto();
                                $(".total-comprobante").html("S/. " + total_general);
                                $(".total-comprobante").attr("total_a_pagar", total_general);
                            });

                            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
                        },
                        complete: function(data) {
                            $(".c-loader").fadeOut("fast");
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log("La solicitud a fallado: " + textStatus);
                        }
                    });
                } else {
                    var item = '<tr class="class-item-pagar p-0" id_cuota_refinanciacion="0" duracion_meses="' + duracion_meses + '" tipo_aportacion="' + tipo_aportacion + '" id="id-item-pagar' + id_concepto_aportacion + '" id_concepto_aportacion="' + id_concepto_aportacion + '">' +
                        '<th class="p-0" scope="row">' + nombre_concepto_aportacion + '</th>' +
                        '<td class="p-0"><p class="id_fecha_inicio" fecha_inicio="' + fecha_inicio + '">' + fecha_inicio + '</p></td>' +
                        '<td class="p-0"><p class="id_cantidad_concepto_aportacion mb-0" cantidad_concepto_aportacion="' + cantidad_concepto_aportacion + '">S/. ' + cantidad_concepto_aportacion + '</p></td>' +
                        '<td class="p-0"><input class="input-nro-items" type="text" num_c_pend="0" value="1" onkeypress="return validanum(event)"/></td>' +
                        '<td class="p-0"><input class="btn-del-item-pago btn btn-danger p-0" type="button" id_concepto_aportacion="' + id_concepto_aportacion + '" value="X"></td>' +
                        '</tr>';
                    $("#lista-items-pago").append(item);

                    var total_general = recalcular_monto();
                    $(".total-comprobante").html("S/. " + total_general);
                    $(".total-comprobante").attr("total_a_pagar", total_general);

                    $(".btn-del-item-pago").click(function() {
                        //var id_concepto_aportacion = $(this).attr("id_concepto_aportacion");

                        $(this).parents("tr").remove();
                        $(".btn-del-item-pago").last().attr("disabled", false);

                        var total_general = recalcular_monto();
                        $(".total-comprobante").html("S/. " + total_general);
                        $(".total-comprobante").attr("total_a_pagar", total_general);
                    });

                    $(".input-nro-items").change(function() {
                        var total_general = recalcular_monto();
                        $(".total-comprobante").html("S/. " + total_general);
                        $(".total-comprobante").attr("total_a_pagar", total_general);
                    });

                    $(".input-nro-items").keyup(function() {
                        var total_general = recalcular_monto();
                        $(".total-comprobante").html("S/. " + total_general);
                        $(".total-comprobante").attr("total_a_pagar", total_general);
                    });

                }

            }
        }

        $(".btn-del-item-pago").click(function() {
            //var id_concepto_aportacion = $(this).attr("id_concepto_aportacion");

            $(this).parents("tr").remove();
            $(".btn-del-item-pago").last().attr("disabled", false);

            var total_general = recalcular_monto();
            $(".total-comprobante").html("S/. " + total_general);
            $(".total-comprobante").attr("total_a_pagar", total_general);
        });

        $(".input-nro-items").change(function() {
            var total_general = recalcular_monto();
            $(".total-comprobante").html("S/. " + total_general);
            $(".total-comprobante").attr("total_a_pagar", total_general);
        });

        $(".input-nro-items").keyup(function() {
            var total_general = recalcular_monto();
            $(".total-comprobante").html("S/. " + total_general);
            $(".total-comprobante").attr("total_a_pagar", total_general);
        });

    });



    $("#tag-conceptos1").click(function() {

        if ($(this).prop('checked')) {
            $("#list-conceptos-id1").css("display", "block");
            $("#list-conceptos-id2").css("display", "none");
        } else {
            $("#list-conceptos-id1").css("display", "none");
            $("#list-conceptos-id2").css("display", "block");
        }

    });

    $("#tag-conceptos2").click(function() {

        if ($(this).prop('checked')) {
            $("#list-conceptos-id1").css("display", "none");
            $("#list-conceptos-id2").css("display", "block");
        } else {
            $("#list-conceptos-id1").css("display", "block");
            $("#list-conceptos-id2").css("display", "none");
        }

    });

    $('#descuento').bind('keyup', function(e) {

        monto = $("#monto-ref-id").attr("monto");

        /*$("input:checkbox").each(function(){
          var cantidad_cuota_actual=$(this).val();
          
          if($(this).prop('checked')){
            monto = monto + parseInt(cantidad_cuota_actual);
          }
          
        });*/

        var descuento = $('#descuento').val();
        if (descuento == '') {
            descuento = 0;
        }
        var cuotas = $('#cuotas_id').val();
        if (cuotas == '') {
            cuotas = 0;
        }

        var nuevo_monto = monto - descuento;

        if (nuevo_monto <= 0) {
            nuevo_monto = 0;
        }

        //document.getElementById('monto-ref-id').innerHTML='S/. '+monto;
        //$('#monto-ref-id').attr('monto',monto);

        var monto_cuota = nuevo_monto / cuotas;

        monto_cuota = Number(monto_cuota.toFixed(2));

        $('#mon_x_cuota').val(monto_cuota);

    });

    $('#cuotas_id').change(function(e) {
        var monto = $('#monto-ref-id').attr('monto');

        var descuento = $('#descuento').val();
        if (descuento == '') {
            descuento = 0;
        }

        var cuotas = $('#cuotas_id').val();
        if (cuotas == '') {
            cuotas = 0;
        }

        var nuevo_monto = monto - descuento;

        if (nuevo_monto <= 0) {
            nuevo_monto = 0;
        }

        var monto_cuota = nuevo_monto / cuotas;

        monto_cuota = Number(monto_cuota.toFixed(2));

        $('#mon_x_cuota').val(monto_cuota);

    });

    $(".btn-gen-ref").click(function() {



        var diferencia = $(this).attr("diferencia");
        var id_colegiado = $(this).attr("id_colegiado");
        var array_meses = new Array();
        var array_years = new Array();
        var monto = $('#monto-ref-id').attr("monto");
        var monto_bruto = $('#monto-ref-id').attr("monto_bruto");
        var descuento = $('#descuento').val();
        var nro_operacion = $("#nro_operacion").val();
        var fecha_contrato = $("#fecha_contrato").val();
        if (descuento == '') {
            descuento = 0;
        }
        var cuotas = $('#cuotas_id').val();
        if (cuotas == '') {
            cuotas = 0;
        }
        var mon_x_cuota = $('#mon_x_cuota').val();

        $("input:checkbox:checked").each(function() {
            var mes = $(this).attr("mes");
            var year = $(this).attr("year");
            array_meses.push(mes);
            array_years.push(year);
        });

        if (diferencia >= 13 && diferencia <= 48) {
            var monto_descuento = monto * 0.2;
            if (descuento >= monto_descuento) {
                $(".c-modal-img-boucher").slideDown("slow");
                guardar_refinanciacion(id_colegiado, nro_operacion, fecha_contrato, array_meses, array_years, monto, monto_bruto, descuento, cuotas, mon_x_cuota);

            } else {
                mensaje_dialogo('rojo', 'El adelanto debe ser como minimo de: S/.' + monto * 0.2);
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
            }
        } else if (diferencia >= 49 && diferencia <= 144) {
            var monto_descuento = monto * 0.1;
            if (descuento >= monto_descuento) {
                $(".c-modal-img-boucher").slideDown("slow");
                guardar_refinanciacion(id_colegiado, nro_operacion, fecha_contrato, array_meses, array_years, monto, monto_bruto, descuento, cuotas, mon_x_cuota);

            } else {
                mensaje_dialogo('rojo', 'El adelanto debe ser como minimo de: S/.' + monto * 0.1);
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
            }
        } else if (diferencia >= 145) {
            var monto_descuento = monto * 0.05;
            if (descuento >= monto_descuento) {
                $(".c-modal-img-boucher").slideDown("slow");
                guardar_refinanciacion(id_colegiado, nro_operacion, fecha_contrato, array_meses, array_years, monto, monto_bruto, descuento, cuotas, mon_x_cuota);

            } else {
                mensaje_dialogo('rojo', 'El adelanto debe ser como minimo de: S/.' + monto * 0.05);
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
            }
        }
    });

    $(".btn-culqi-ref").click(function() {
        var diferencia = $(".btn-culqi-ref").attr("diferencia");
        var descuento1 = $('#descuento').val();
        descuento = descuento1 * 100;
        var monto = $('#monto-ref-id').attr("monto");

        if (diferencia >= 13 && diferencia <= 48) {
            var monto_descuento = monto * 0.2;
            if (descuento1 >= monto_descuento) {
                var monto_adelanto = $("#descuento").val();
                monto_adelanto = monto_adelanto * 100;
                Culqi.settings({
                    title: 'Colegio de Economistas',
                    currency: 'PEN',
                    description: 'Cajamarca',
                    amount: monto_adelanto
                });
                Culqi.open();
                e.preventDefault();


            } else {
                mensaje_dialogo('rojo', 'El adelanto debe ser como minimo de: S/.' + monto * 0.2);
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
            }
        } else if (diferencia >= 49 && diferencia <= 144) {
            var monto_descuento = monto * 0.1;
            if (descuento1 >= monto_descuento) {
                var monto_adelanto = $("#descuento").val();
                monto_adelanto = monto_adelanto * 100;
                Culqi.settings({
                    title: 'Colegio de Economistas',
                    currency: 'PEN',
                    description: 'Cajamarca',
                    amount: monto_adelanto
                });
                Culqi.open();
                e.preventDefault();


            } else {
                mensaje_dialogo('rojo', 'El adelanto debe ser como minimo de: S/.' + monto * 0.1);
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
            }
        } else if (diferencia >= 145) {
            var monto_descuento = monto * 0.05;
            if (descuento1 >= monto_descuento) {
                var monto_adelanto = $("#descuento").val();
                monto_adelanto = monto_adelanto * 100;
                Culqi.settings({
                    title: 'Colegio de Economistas',
                    currency: 'PEN',
                    description: 'Cajamarca',
                    amount: monto_adelanto
                });
                Culqi.open();
                e.preventDefault();

            } else {
                mensaje_dialogo('rojo', 'El adelanto debe ser como minimo de: S/.' + monto * 0.05);
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
            }
        }



    });

    $('input:radio[name=modo-select]').change(function() {
        var cantidad = $(this).attr("cantidad");
        var posicion = $(this).attr("posicion");
        var descuento = $('#descuento').val();
        if (descuento == '') {
            descuento = 0;
        }
        var cuotas = $('#cuotas_id').val();
        if (cuotas == '') {
            cuotas = 0;
        }

        var modo_busqueda = $(this).val();

        if (modo_busqueda == 'todosm') {

            $('.mes-ref').prop("checked", true);
            $('.mes-ref').attr("disabled", true);

            var monto = calculo_refinanciado(cantidad, posicion, descuento, cuotas);

            document.getElementById('monto-ref-id').innerHTML = 'S/. ' + monto;
            $('#monto-ref-id').attr('monto', monto);

            var monto_cuota = monto / cuotas;

            monto_cuota = Number(monto_cuota.toFixed(2));

            $('#mon_x_cuota').val(monto_cuota);

        } else if (modo_busqueda == 'manualm') {

            document.getElementById('monto-ref-id').innerHTML = 'S/. ' + 0;
            $('#monto-ref-id').attr('monto', 0);
            $('#mon_x_cuota').val(0);

            $('.mes-ref').prop("checked", false);
            $('#mes-atra-ref0').attr("disabled", false);
        }

    });

    $(".cerrar-mpago").click(function() {
        $(".modal-noti").slideUp("slow");
    });

    $(".btn-guardar-pago").click(function() {
        var nro_operacion = $("#nro_operacion").val();

        var nuevo_total = $(".total-comprobante").attr("total_a_pagar");
        nuevo_total = nuevo_total * 100;
        Culqi.settings({
            title: 'Colegio de Economistas',
            currency: 'PEN',
            description: 'Cajamarca',
            amount: nuevo_total
        });
        Culqi.open();
        e.preventDefault();
    });

    $(".btn-guardar-pago1").click(function() {
        var id_colegiado = $(this).attr("id_colegiado");
        var nro_operacion = $("#nro_operacion").val();
        var total_a_pagar = $(".total-comprobante").attr("total_a_pagar");

        var array_id_cuota_refinanciacion = new Array();
        var array_tipo_aportacion = new Array();
        var array_id_concepto_aportacion = new Array();
        var array_id_fecha_inicio = new Array();
        var array_id_cantidad_concepto_aportacion = new Array();
        var array_id_duracion_meses = new Array();
        var array_input_nro_items = new Array();

        $(".class-item-pagar").each(function() {

            var id_cuota_refinanciacion = $(this).attr("id_cuota_refinanciacion");
            var tipo_aportacion = $(this).attr("tipo_aportacion");
            var id_concepto_aportacion = $(this).attr("id_concepto_aportacion");
            var id_fecha_inicio = $(this).children("td").children(".id_fecha_inicio").attr("fecha_inicio");
            var id_cantidad_concepto_aportacion = $(this).children("td").children(".id_cantidad_concepto_aportacion").attr("cantidad_concepto_aportacion");
            var id_duracion_meses = $(this).attr("duracion_meses");
            var input_nro_items = $(this).children("td").children(".input-nro-items").val();

            //alert(tipo_aportacion+"---"+id_concepto_aportacion+"---"+id_fecha_inicio+"---"+id_cantidad_concepto_aportacion+"---"+id_cuota_refinanciacion+"---"+id_duracion_meses+"---"+input_nro_items);

            array_id_cuota_refinanciacion.push(id_cuota_refinanciacion);
            array_tipo_aportacion.push(tipo_aportacion);
            array_id_concepto_aportacion.push(id_concepto_aportacion);
            array_id_fecha_inicio.push(id_fecha_inicio);
            array_id_cantidad_concepto_aportacion.push(id_cantidad_concepto_aportacion);
            array_id_duracion_meses.push(id_duracion_meses);
            array_input_nro_items.push(input_nro_items);

        });

        guardar_boucher(id_colegiado, nro_operacion, array_id_cuota_refinanciacion, array_tipo_aportacion, array_id_concepto_aportacion, array_id_fecha_inicio, array_id_cantidad_concepto_aportacion, array_id_duracion_meses, array_input_nro_items, total_a_pagar, cuotas_descartadas);




    });

    $(".btn-guardar-pago-total").click(function() {
        var id_colegiado = $(this).attr("id_colegiado");
        var nro_operacion = $("#nro_operacion").val();
        var id_refinanciacion_atrasada = $(this).attr("id_refinanciacion_atrasada");
        var fecha_nuevo_inicio_at = $(this).attr("fecha_nuevo_inicio_at");
        var fecha_actual_ultpago = $(this).attr("fecha_actual_ultpago");
        var monto_cuotas_pen_atra = $(this).attr("monto_cuotas_pen_atra");
        var total_a_pagar = $(".total-comprobante1").attr("total_a_pagar");

        guardar_boucher_total(id_colegiado, nro_operacion, id_refinanciacion_atrasada, fecha_nuevo_inicio_at, fecha_actual_ultpago, monto_cuotas_pen_atra, total_a_pagar);
    });

    /*EVENTO BOTON EMERGENCIA*/
    $(".btn-emergencia1").click(function() {

        $(".c-modal-pago").slideDown("slow");

        //var token = Culqi.token.id;
        //var email = Culqi.token.email;

        var id_colegiado = $(this).attr("id_colegiado");
        var nro_operacion = $("#nro_operacion").val();
        var total = $(".total-comprobante").attr("total_a_pagar");
        var existencia=1;
        /*if(!total){
            total = $(".total-comprobante1").attr("total_a_pagar");
            existencia=0;
        }*/

        total = total * 100;

        

        var array_id_cuota_refinanciacion = new Array();
        var array_tipo_aportacion = new Array();
        var array_id_concepto_aportacion = new Array();
        var array_id_fecha_inicio = new Array();
        var array_id_cantidad_concepto_aportacion = new Array();
        var array_id_duracion_meses = new Array();
        var array_input_nro_items = new Array();



        $(".class-item-pagar").each(function() {

            var id_cuota_refinanciacion = $(this).attr("id_cuota_refinanciacion");
            var tipo_aportacion = $(this).attr("tipo_aportacion");
            var id_concepto_aportacion = $(this).attr("id_concepto_aportacion");
            var id_fecha_inicio = $(this).children("td").children(".id_fecha_inicio").attr("fecha_inicio");
            var id_cantidad_concepto_aportacion = $(this).children("td").children(".id_cantidad_concepto_aportacion").attr("cantidad_concepto_aportacion");
            var id_duracion_meses = $(this).attr("duracion_meses");
            var input_nro_items = $(this).children("td").children(".input-nro-items").val();

            //alert(tipo_aportacion+"---"+id_concepto_aportacion+"---"+id_fecha_inicio+"---"+id_cantidad_concepto_aportacion+"---"+id_cuota_refinanciacion+"---"+id_duracion_meses+"---"+input_nro_items);

            array_id_cuota_refinanciacion.push(id_cuota_refinanciacion);
            array_tipo_aportacion.push(tipo_aportacion);
            array_id_concepto_aportacion.push(id_concepto_aportacion);
            array_id_fecha_inicio.push(id_fecha_inicio);
            array_id_cantidad_concepto_aportacion.push(id_cantidad_concepto_aportacion);
            array_id_duracion_meses.push(id_duracion_meses);
            array_input_nro_items.push(input_nro_items);

        });


        var formData = new FormData($("#form-adel-pago")[0]);
        var ruta = "../../server/pago-culqi.php";

        var jsonArray_id_cuota_refinanciacion = JSON.stringify(array_id_cuota_refinanciacion);
        var jsonArray_tipo_aportacion = JSON.stringify(array_tipo_aportacion);
        var jsonArray_id_concepto_aportacion = JSON.stringify(array_id_concepto_aportacion);
        var jsonArray_id_fecha_inicio = JSON.stringify(array_id_fecha_inicio);
        var jsonArray_id_cantidad_concepto_aportacion = JSON.stringify(array_id_cantidad_concepto_aportacion);
        var jsonArray_id_duracion_meses = JSON.stringify(array_id_duracion_meses);
        var jsonArray_input_nro_items = JSON.stringify(array_input_nro_items);

        formData.append('id_colegiado', id_colegiado);
        formData.append('nro_operacion', nro_operacion);

        formData.append('array_id_cuota_refinanciacion', jsonArray_id_cuota_refinanciacion);
        formData.append('array_tipo_aportacion', jsonArray_tipo_aportacion);
        formData.append('array_id_concepto_aportacion', jsonArray_id_concepto_aportacion);
        formData.append('array_id_fecha_inicio', jsonArray_id_fecha_inicio);
        formData.append('array_id_cantidad_concepto_aportacion', jsonArray_id_cantidad_concepto_aportacion);
        formData.append('array_id_duracion_meses', jsonArray_id_duracion_meses);
        formData.append('array_input_nro_items', jsonArray_input_nro_items);
        formData.append('cuotas_descartadas', cuotas_descartadas);
        formData.append('total', total);
        //formData.append('email', email);
        //formData.append('token', token);

        if (jsonArray_tipo_aportacion != "[]") {

            $.ajax({
                data: formData,
                url: ruta,
                type: 'post',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $(".c-loader").fadeIn("fast");
                },
                success: function(data) {
                    
                    $("#modal-pago-id").html("<iframe style='width:100%; height:550px;' src=" + data + "></iframe>");

                },
                complete: function(data) {
                    $(".c-loader").fadeOut("fast");
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("La solicitud a fallado: " + textStatus);
                }
            });


        } else {
            mensaje_dialogo('rojo', 'Elija algun concepto de pago');
            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
        }

    });

    /*BOTON DE EMERGENCIA 2*/
    $(".btn-emergencia2").click(function() {
        var token = '';
        var email = '';

        $(".c-modal-pago").slideDown("slow");

        var id_colegiado = $(".btn-emergencia2").attr("id_colegiado");
        var nro_operacion = $("#nro_operacion").val();
        var id_refinanciacion_atrasada = $(".btn-emergencia2").attr("id_refinanciacion_atrasada");
        var fecha_nuevo_inicio_at = $(".btn-emergencia2").attr("fecha_nuevo_inicio_at");
        var fecha_actual_ultpago = $(".btn-emergencia2").attr("fecha_actual_ultpago");
        var monto_cuotas_pen_atra = $(".btn-emergencia2").attr("monto_cuotas_pen_atra");
        var total_a_pagar_culqi = $(".total-comprobante1").attr("total_a_pagar");
        total_a_pagar_culqi = total_a_pagar_culqi * 100;
        guardar_boucher_total_culqi(id_colegiado, nro_operacion, token, email, total_a_pagar_culqi, id_refinanciacion_atrasada, fecha_nuevo_inicio_at, fecha_actual_ultpago, monto_cuotas_pen_atra);

    });

    /*BOTON DE EMERGENCIA - REFINANCIAMIENTO*/

    $(".btn-emergencia").click(function() {

        var token = '';
        var email = '';
        $(".c-modal-pago").slideDown("slow");
        var diferencia = $(".btn-emergencia").attr("diferencia");
        var id_colegiado = $(".btn-emergencia").attr("id_colegiado");
        var array_meses = new Array();
        var array_years = new Array();
        var monto = $('#monto-ref-id').attr("monto");
        var monto_bruto = $('#monto-ref-id').attr("monto_bruto");
        var descuento1 = $('#descuento').val();
        descuento = descuento1 * 100;
        var nro_operacion = $("#nro_operacion").val();
        var fecha_contrato = $("#fecha_contrato").val();
        if (descuento == '') {
            descuento = 0;
        }
        var cuotas = $('#cuotas_id').val();
        if (cuotas == '') {
            cuotas = 0;
        }
        var mon_x_cuota = $('#mon_x_cuota').val();

        $("input:checkbox:checked").each(function() {
            var mes = $(this).attr("mes");
            var year = $(this).attr("year");
            array_meses.push(mes);
            array_years.push(year);
        });

        if (diferencia >= 13 && diferencia <= 48) {
            var monto_descuento = monto * 0.2;
            if (descuento1 >= monto_descuento) {
                guardar_refinanciacion_culqi(id_colegiado, token, email, nro_operacion, fecha_contrato, array_meses, array_years, monto, monto_bruto, descuento, cuotas, mon_x_cuota);

            } else {
                mensaje_dialogo('rojo', 'El adelanto debe ser como minimo de: S/.' + monto * 0.2);
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
            }
        } else if (diferencia >= 49 && diferencia <= 144) {
            var monto_descuento = monto * 0.1;
            if (descuento1 >= monto_descuento) {
                guardar_refinanciacion_culqi(id_colegiado, token, email, nro_operacion, fecha_contrato, array_meses, array_years, monto, monto_bruto, descuento, cuotas, mon_x_cuota);

            } else {
                mensaje_dialogo('rojo', 'El adelanto debe ser como minimo de: S/.' + monto * 0.1);
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
            }
        } else if (diferencia >= 145) {
            var monto_descuento = monto * 0.05;
            if (descuento1 >= monto_descuento) {
                guardar_refinanciacion_culqi(id_colegiado, token, email, nro_operacion, fecha_contrato, array_meses, array_years, monto, monto_bruto, descuento, cuotas, mon_x_cuota);

            } else {
                mensaje_dialogo('rojo', 'El adelanto debe ser como minimo de: S/.' + monto * 0.05);
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
            }
        }
    });

    $(".btn-cerrar-modal-pago").click(function() {
        $(".c-modal-pago").slideUp("fast");
        location.reload();
    });

    $(".cerrar-pago").click(function() {
        $(".c-modal-pago").slideUp("fast");
    });


    /*evento pago por trasferencia*/
    $(".btn-modal-img-boucher").click(function() {

        $(".c-modal-img-boucher").slideDown("slow");
    });

    $(".btn-salir-noti").click(function() {
        $(".c-modal-img-boucher").slideUp("slow");
    });


    $(".btn-guardar-pago-total-culqi").click(function() {

        //var nro_operacion = $("#nro_operacion").val();
        val_diferencia = 'total';
        var nuevo_total = $(".total-comprobante1").attr("total_a_pagar");
        nuevo_total = nuevo_total * 100;

        Culqi.settings({
            title: 'Colegio de Economistas',
            currency: 'PEN',
            description: 'Cajamarca',
            amount: nuevo_total
        });
        Culqi.open();
        e.preventDefault();
    });

    /*ocultar radio bottom al presentar atrazo*/

    val_diferencia = $("#c-form-crear-ref-id").attr("val_diferencia");

    if (val_diferencia > 12) {

        $(".c-tag-conceptos").attr("style", "display:none !important");
        $(".c-total-pago1").attr("class", "col-lg-12 col-md-12 col-sm-12 col-xs-12 c-total-pago c-total-pago1");

        $(".c-fecha-inicio").attr("style", "display:none !important");
        $(".c-fecha-fin").attr("style", "display:none !important");
        $(".c-lista-comprobante").attr("style", "display:none !important");

        $(".c-t-pagar").attr("style", "display:none !important");

        $(".c-link-otros").html('<a href="vista-pagos-cursos.php">Realizar Otros pagos->></a>');
    }


    /*PREGUNTAR POR NUEVAS NOTIFICACIONES*/
    var status_noti = $("#noti-id").attr("class");
    var id_colegiado_noti = $("#noti-id").attr("id_colegiado");

    if (status_noti == 'navbar-brand item-bar-hor mr-0 c-img-user noti') {
        load_notificaciones(id_colegiado_noti);
        $(".modal-noti").slideDown("slow");
    }

    /*EVENTO ACTIVAR POLITICA*/
    $(".btn-guardar-fecha").click(function() {
        var id_det_politica = $(this).attr("id_det_politica");
        var fecha_limite = $("#fecha_limite").val();

        actualizar_fecha_limite(id_det_politica, fecha_limite);
    });

    /*validacion para link en total de deuda*/
    var valor_atra = $(".mensaje-atrazo").attr("valor");
    if (valor_atra != 0) {
        $(".c-link-otros").html('<a href="vista-pagos-cursos.php">Realizar Otros pagos->></a>');
    }

    /*EVENTO PARA PARPADEAR DOCUMENTO DE REFINANCIACION*/




    Culqi.createToken();



});
var val_diferencia = 0;
var cont_file = 0;
var nuevo_total = 0;
var monto_adelanto = 0;
var cuotas_descartadas = 0;

function toggleSidebar() {
    document.getElementById("sidebar").classList.toggle('active');
}

function eventos_panel() {
    $(".li-item").click(function() {
        var id_item_cuota = $(this).attr("id");
        $("#d" + id_item_cuota).slideToggle("fast");
    });

    /** EVENTOS FOTOS*/
    $(".control-boucher").on("change", function() {
        var id_cuota = $(this).attr("id_cuota");
        //$("#id_imagen_perfil").html("<img src='../../client/img/user.png' alt='' />");
        var boucher = document.getElementById('boucher' + id_cuota).files;
        var navegador = window.URL || window.webkitURL;

        for (x = 0; x < boucher.length; x++) {
            var size = boucher[x].size;
            var type = boucher[x].type;
            var name = boucher[x].name;

            var objeto_url = navegador.createObjectURL(boucher[x]);

            if (type != "image/jpg" && type != "image/jpeg" && type != "image/png" && type != "image/JPG" && type != "image/JPEG" && type != "image/PNG") {
                $("#v" + id_cuota).html("<p class='bg-danger no-permit'>Formato de Archivo no permitido, solo esta permitido subir imagenes en formatos jpg, jpeg o png<p/>");
            } else {
                document.getElementById("v" + id_cuota).innerHTML = "<img src='" + objeto_url + "' alt='' />";
                //document.getElementById("modal-img-id"+id_cuota).innerHTML="<img src='"+objeto_url+"' alt='' />";
                $("#v" + id_cuota).attr("url_boucher", objeto_url);
                cont_file++;
            }

        }

    })

    $("#boucher-id").on("change", function() {

        //$("#id_imagen_perfil").html("<img src='../../client/img/user.png' alt='' />");
        var boucher = document.getElementById('boucher-id').files;
        var navegador = window.URL || window.webkitURL;

        for (x = 0; x < boucher.length; x++) {
            var size = boucher[x].size;
            var type = boucher[x].type;
            var name = boucher[x].name;

            var objeto_url = navegador.createObjectURL(boucher[x]);

            if (type != "image/jpg" && type != "image/jpeg" && type != "image/png" && type != "image/JPG" && type != "image/JPEG" && type != "image/PNG") {
                $("#img-boucher-id").html("<p class='bg-danger no-permit'>Formato de Archivo no permitido, solo esta permitido subir imagenes en formatos jpg, jpeg o png<p/>");
            } else {
                document.getElementById("img-boucher-id").innerHTML = "<img style='width: 100%; height: auto;' src='" + objeto_url + "' alt='' />";
                cont_file++;
            }

        }

    })


    $("#foto_perfil_col").on("change", function() {

        //$("#id_imagen_perfil").html("<img src='../../client/img/user.png' alt='' />");
        var boucher = document.getElementById('foto_perfil_col').files;
        var navegador = window.URL || window.webkitURL;

        for (x = 0; x < boucher.length; x++) {
            var size = boucher[x].size;
            var type = boucher[x].type;
            var name = boucher[x].name;

            var objeto_url = navegador.createObjectURL(boucher[x]);

            if (type != "image/jpg" && type != "image/jpeg" && type != "image/png" && type != "image/JPG" && type != "image/JPEG" && type != "image/PNG") {
                $("#vista-previa-foto-id").html("<p class='bg-danger no-permit'>Formato de Archivo no permitido, solo esta permitido subir imagenes en formatos jpg, jpeg o png<p/>");
            } else {
                document.getElementById("vista-previa-foto-id").innerHTML = "<img src='" + objeto_url + "' alt='' />";

                cont_file++;
            }

        }

    })

    /** EVENTO GUARDAR BOUCHER*/
    $(".btn-save-boucher").click(function() {
        var id_cuota = $(this).attr("id_cuota");
        var id_colegiado = $(this).attr("id_colegiado");
        var nro_operacion = $("#nro_operacion").val();

        if (cont_file > 0) {
            subir_foto(id_cuota, id_colegiado, nro_operacion);
        } else {
            mensaje_dialogo('rojo', 'Coloque una imagen');
            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
        }

    });

    /*$(".btn-guardar-pago").click(function() {
      
                var id_colegiado=$("#form-adel-pago").attr("id_colegiado");

                var mes_inicio=$("#m-inicio").val();
                mes_inicio=zfill(mes_inicio,2);
                
                var year_inicio=$("#y-inicio").val();
                var fecha_inicio=year_inicio+"-"+mes_inicio+"-01";
                fecha_inicio= new Date(fecha_inicio);

                var mes_fin=$("#m-fin").val();
                mes_fin=zfill(mes_fin,2);

                var year_fin=$("#y-fin").val();
                var fecha_fin=year_fin+"-"+mes_fin+"-01";
                fecha_fin= new Date(fecha_fin);
                
                var primerDia = new Date(fecha_inicio.getFullYear(), fecha_inicio.getMonth(), 1);      
                //var ultimoDia = new Date(fecha_fin.getFullYear(), fecha_fin.getMonth() + 1, 0);
                
                fecha_inicio = year_inicio+"-"+mes_inicio+"-"+zfill(primerDia.getDate(),2);
                
                var nro_operacion=$("#nro_operacion").val();
            
                var monto = $("#total_pag_reg_id").attr('valor');

                var array_ids= new Array();
           
                $(".imput-cuotas-pago").each(function(){
                  var id_valor=$(this).attr("id_valor");
                                
                  if($(this).prop('checked')){
                    array_ids.push(id_valor);
                     
                  }
                  
                });
            
                guardar_boucher(id_colegiado,fecha_inicio,year_fin,mes_fin,monto,nro_operacion,array_ids);
              
            });*/

    $(".imput-cuotas-pago").click(function() {
        var cantidad_cuota_actual = $('.cantidad-cuota-actual').val();

        var m_inicio = $("#m-inicio").val();
        var y_inicio = $("#y-inicio").val();
        var m_fin = $("#m-fin").val();
        var y_fin = $("#y-fin").val();
        var meses = calculo_monto(m_inicio, y_inicio, m_fin, y_fin);
        var cant_meses = meses / 12;
        var descuento = Math.trunc(cant_meses) * (cantidad_cuota_actual * 2);
        var monto_final = (meses * cantidad_cuota_actual) - descuento;

        var total_cuotas = 0;
        var total_pago_regular = 0;

        if (monto_final <= 0) {
            monto_final = 0;
        }

        total_pago_regular = monto_final;

        $(".imput-cuotas-pago").each(function() {
            var cuota_ref = $(this).attr("value");

            if ($(this).prop('checked')) {
                monto_final = monto_final + parseFloat(cuota_ref);
                total_cuotas = total_cuotas + parseFloat(cuota_ref);
            }
        });

        document.getElementById("total_cuotas_id").innerHTML = 'S/. ' + total_cuotas.toFixed(2);
        $("#total_cuotas_id").attr("valor", total_cuotas);

        document.getElementById("total_pag_reg_id").innerHTML = 'S/. ' + total_pago_regular.toFixed(2);
        $("#total_pag_reg_id").attr("valor", total_pago_regular);

        document.getElementById("monto-total-id").innerHTML = 'S/. ' + monto_final.toFixed(2);
        $("#monto-total-id").attr("valor", monto_final);
    });

    $('#m-fin').change(function() {
        var cantidad_cuota_actual = $('.cantidad-cuota-actual').val();

        var m_inicio = $("#m-inicio").val();
        var y_inicio = $("#y-inicio").val();
        var m_fin = $("#m-fin").val();
        var y_fin = $("#y-fin").val();
        var meses = calculo_monto(m_inicio, y_inicio, m_fin, y_fin);
        var cant_meses = meses / 12;
        var descuento = Math.trunc(cant_meses) * (cantidad_cuota_actual * 2);
        var monto_final = (meses * cantidad_cuota_actual) - descuento;

        var total_cuotas = 0;
        var total_pago_regular = 0;

        if (monto_final <= 0) {
            monto_final = 0;
        }

        total_pago_regular = monto_final;

        $(".imput-cuotas-pago").each(function() {
            var cuota_ref = $(this).attr("value");

            if ($(this).prop('checked')) {
                monto_final = monto_final + parseFloat(cuota_ref);
                total_cuotas = total_cuotas + parseFloat(cuota_ref);
            }
        });

        document.getElementById("total_cuotas_id").innerHTML = 'S/. ' + total_cuotas.toFixed(2);
        $("#total_cuotas_id").attr("valor", total_cuotas);

        document.getElementById("total_pag_reg_id").innerHTML = 'S/. ' + total_pago_regular.toFixed(2);
        $("#total_pag_reg_id").attr("valor", total_pago_regular);

        document.getElementById("monto-total-id").innerHTML = 'S/. ' + monto_final.toFixed(2);
        $("#monto-total-id").attr("valor", monto_final);
    });
    $('#y-fin').change(function() {
        var cantidad_cuota_actual = $('.cantidad-cuota-actual').val();

        var m_inicio = $("#m-inicio").val();
        var y_inicio = $("#y-inicio").val();
        var m_fin = $("#m-fin").val();
        var y_fin = $("#y-fin").val();
        var meses = calculo_monto(m_inicio, y_inicio, m_fin, y_fin);
        var cant_meses = meses / 12;
        var descuento = Math.trunc(cant_meses) * (cantidad_cuota_actual * 2);
        var monto_final = (meses * cantidad_cuota_actual) - descuento;

        var total_cuotas = 0;
        var total_pago_regular = 0;

        if (monto_final <= 0) {
            monto_final = 0;
        }

        total_pago_regular = monto_final;

        $(".imput-cuotas-pago").each(function() {
            var cuota_ref = $(this).attr("value");

            if ($(this).prop('checked')) {
                monto_final = monto_final + parseFloat(cuota_ref);
                total_cuotas = total_cuotas + parseFloat(cuota_ref);
            }
        });

        document.getElementById("total_cuotas_id").innerHTML = 'S/. ' + total_cuotas.toFixed(2);
        $("#total_cuotas_id").attr("valor", total_cuotas);

        document.getElementById("total_pag_reg_id").innerHTML = 'S/. ' + total_pago_regular.toFixed(2);
        $("#total_pag_reg_id").attr("valor", total_pago_regular);

        document.getElementById("monto-total-id").innerHTML = 'S/. ' + monto_final.toFixed(2);
        $("#monto-total-id").attr("valor", monto_final);
    });

    $(".ver-comprobante").click(function() {
        var id_aportacion = $(this).attr("id");
        var array_aportacion = id_aportacion.split("v");
        var url_boucher = $(this).attr("url_boucher");

        $("#vmod" + array_aportacion[1]).fadeIn("fast");

    });

    $(".cerrar-mod-img").click(function() {
        var id_aportacion = $(this).attr("id");
        var array_aportacion = id_aportacion.split("close");
        $("#vmod" + array_aportacion[1]).fadeOut("fast");

    });

    /** GUARDAR DATOS COLEGIADO*/
    $(".btn-save-config").click(function() {
        var id_colegiado = $(".c-form-config").attr("id");
        var nombre_colegiado = $("#nombre_colegiado").val();
        var apellido_paterno = $("#apellido_paterno").val();
        var apellido_materno = $("#apellido_materno").val();
        var dni_colegiado = $("#dni_colegiado").val();
        var sexo_colegiado = $("#sexo_colegiado").val();
        var fecha_nacimiento = $("#datepicker").val();

        var telefono = $("#telefono_colegiado").val();
        var direccion = $("#direccion_colegiado").val();
        var correo = $("#correo_colegiado").val();
        var usuario = $("#nickname_colegiado").val();
        var clave = $("#clave_colegiado").val();
        actualizar_colegiado(id_colegiado, nombre_colegiado, apellido_paterno, apellido_materno, dni_colegiado, telefono, sexo_colegiado, fecha_nacimiento, direccion, correo, usuario, clave);
    });

    /** SELECCIONAR AÑO*/
    $("#id_year").change(function() {
        var year = $(this).val();
        var id_colegiado = $(this).attr('id_colegiado');
        load_cuotas(year, id_colegiado);
    });

    /*-------------------------eventos notificaciones----------------*/
    $('.c-img-user').click(function() {
        var id_colegiado = $(this).attr("id_colegiado");
        load_notificaciones(id_colegiado);
        $(".modal-noti").slideDown("slow");

    });

    $('.open-noti').click(function() {
        var id_colegiado = $(this).attr("id_colegiado");
        load_notificaciones(id_colegiado);
        $(".modal-noti").slideDown("slow");

    });

}

function calculo_monto(m_inicio, y_inicio, m_fin, y_fin) {

    m_inicio = parseInt(m_inicio);
    y_inicio = parseInt(y_inicio);

    m_fin = parseInt(m_fin);
    y_fin = parseInt(y_fin);

    resultado = 0;

    if (y_inicio == y_fin) {
        resultado = (m_fin - m_inicio) + 1;
    } else {
        dif_year = y_fin - y_inicio;
        if (dif_year == 1) {
            resultado = ((12 - m_inicio) + 1) + m_fin;
        } else {
            resultado = ((dif_year - 1) * 12) + (((12 - m_inicio) + 1) + m_fin);
        }
    }

    return resultado;
}

function zfill(number, width) {
    var numberOutput = Math.abs(number); /* Valor absoluto del número */
    var length = number.toString().length; /* Largo del número */
    var zero = "0"; /* String de cero */

    if (width <= length) {
        if (number < 0) {
            return ("-" + numberOutput.toString());
        } else {
            return numberOutput.toString();
        }
    } else {
        if (number < 0) {
            return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
        } else {
            return ((zero.repeat(width - length)) + numberOutput.toString());
        }
    }
}

function mensaje_dialogo(color, contenido) {
    var resultado = '';
    resultado = '<p class="mensaje-' + color + ' col-lg-6 col-md-8 col-sm-10">' + contenido + '</p>';
    $(".c-mensaje").html(resultado);
    $(".c-mensaje").fadeIn("slow");
}

function login(usuario, clave) {
    if (usuario != '' && clave != '') {

        $.ajax({
            data: { "u": usuario, "c": clave },
            url: "../../server/session.php",
            type: 'post',
            beforeSend: function() {
                $(".c-loader").fadeIn("fast");
            },
            success: function(data) {
                if (data == 1) {
                    window.location.href = "../../index.php";
                } else if (data == 0) {
                    mensaje_dialogo('rojo', 'Usuario o Clave incorrectos');
                } else if (data == 3) {
                    mensaje_dialogo('amarillo', 'Usuario Desasociado');
                }
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
            },
            complete: function(data) {
                $(".c-loader").fadeOut("fast");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });

    } else {
        mensaje_dialogo('rojo', 'Coloque un usuario y clave validos!!!');
        setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
    }
}

function recuperar_clave(correo_recu) {
    if (correo_recu != '') {

        $.ajax({
            data: { "correo_recu": correo_recu },
            url: "../../server/recuperar_clave.php",
            type: 'post',
            beforeSend: function() {
                $(".c-loader").fadeIn("fast");
            },
            success: function(data) {
                if (data == 1) {
                    $(".c-form-recu").fadeOut("fast");
                    mensaje_dialogo('verde', 'Contraseña enviada exitosamente, revise su correo electrónico!');
                } else if (data == 0) {
                    mensaje_dialogo('rojo', 'Correo electrónico no registrado');
                }
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 3000);
            },
            complete: function(data) {
                $(".c-loader").fadeOut("fast");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });

    } else {
        mensaje_dialogo('rojo', 'Coloque un correo electrónico valido!');
        setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
    }
}

function actualizar_colegiado(id_colegiado, nombre_colegiado, apellido_paterno, apellido_materno, dni_colegiado, telefono, sexo_colegiado, fecha_nacimiento, direccion, correo, usuario, clave) {
    mensaje_final = 'Datos registrados correctamente';

    contador = 0;
    if (usuario == '') {
        mensaje_final = 'Coloque un nombre de Usuario';
        contador++;
    } else if (clave == '') {
        mensaje_final = 'Coloque una Contraseña';
        contador++;
    }
    if (contador == 0) {

        $.ajax({
            data: {
                "id_colegiado": id_colegiado,
                "nombre_colegiado": nombre_colegiado,
                "apellido_paterno": apellido_paterno,
                "apellido_materno": apellido_materno,
                "dni_colegiado": dni_colegiado,
                "sexo_colegiado": sexo_colegiado,
                "fecha_nacimiento": fecha_nacimiento,
                "telefono": telefono,
                "direccion": direccion,
                "correo": correo,
                "usuario": usuario,
                "clave": clave
            },
            url: "../../server/guardar-colegiado.php",
            type: 'post',
            beforeSend: function() {
                $(".c-loader").fadeIn("fast");
            },
            success: function(data) {

                if (data != 0) {
                    $(".c-form-recu").fadeOut("fast");
                    if (cont_file > 0) {
                        subir_foto_perfil(id_colegiado);
                        //window.location.href='vista-configuracion.php';
                    }
                    mensaje_dialogo('verde', mensaje_final);
                } else if (data == 0) {
                    mensaje_dialogo('rojo', 'Error al guardar datos');
                }
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 3000);
            },
            complete: function(data) {
                $(".c-loader").fadeOut("fast");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });

    } else {
        mensaje_dialogo('rojo', mensaje_final);
        setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
    }
}

function subir_foto(id_cuota, id_colegiado, nro_operacion) {

    var formData = new FormData($("#form" + id_cuota)[0]);
    var ruta = "../../server/subir-foto.php";
    formData.append('id_cuota', id_cuota);
    formData.append('id_colegiado', id_colegiado);
    formData.append('nro_operacion', nro_operacion);

    $.ajax({
        data: formData,
        url: ruta,
        type: 'post',
        contentType: false,
        processData: false,
        beforeSend: function() {
            $(".c-loader").fadeIn("fast");
        },
        success: function(data) {

            if (data != 0) {
                mensaje_dialogo('verde', 'Boucher Guardado Correctamente!');
                var array_resultado = data.split('$');
                document.getElementById('form' + id_cuota).innerHTML = array_resultado[0];
                document.getElementById('modal-img-id' + id_cuota).innerHTML = array_resultado[1];
                btn_ver = "<button class='btn btn-success pl-3 pr-3 ver-comprobante' url_boucher='" + array_resultado[1] + "' id='v" + id_cuota + "'>Ver</button>";

                document.getElementById('c-btn-cuota-id' + id_cuota).innerHTML = btn_ver;
                $("#vmod" + id_cuota).fadeOut("fast");
            } else {
                mensaje_dialogo('rojo', 'Error al guardar Boucher');
            }

            eventos_panel();

            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
        },
        complete: function(data) {
            $(".c-loader").fadeOut("fast");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });


}

function subir_foto_perfil(id_colegiado) {

    var formData = new FormData($("#" + id_colegiado)[0]);
    var ruta = "../../server/subir-foto-perfil.php";
    formData.append('id_colegiado', id_colegiado);

    $.ajax({
        data: formData,
        url: ruta,
        type: 'post',
        contentType: false,
        processData: false,
        beforeSend: function() {
            $(".c-loader").fadeIn("fast");
        },
        success: function(data) {
            window.location.href = 'vista-configuracion.php';
        },
        complete: function(data) {
            $(".c-loader").fadeOut("fast");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });

}

function load_cuotas(year, id_colegiado) {
    $.ajax({
        data: {
            "year": year,
            "id_colegiado": id_colegiado
        },
        url: "../../client/views/ajax.php",
        type: 'post',
        beforeSend: function() {
            $(".c-loader").fadeIn("fast");
        },
        success: function(data) {
            document.getElementById('c-cuotas-res-id').innerHTML = data;
            eventos_panel();
            //setTimeout(function(){ $(".c-mensaje").fadeOut("slow"); }, 3000);
        },
        complete: function(data) {
            $(".c-loader").fadeOut("fast");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });
}

function load_notificaciones(id_colegiado) {

    $.ajax({
        data: {
            "id_colegiado": id_colegiado,
            "load_notificaciones": 'load_notificaciones'
        },
        url: "../../client/views/ajax.php",
        type: 'post',
        beforeSend: function() {
            $(".c-loader").fadeIn("fast");
        },
        success: function(data) {

            document.getElementById('c-modal-noti-id').innerHTML = data;

            $(".btn-salir-noti").click(function() {
                var id_colegiado = $(this).attr("id_colegiado");
                actualizar_estado_noti(id_colegiado);
            });
            //setTimeout(function(){ $(".c-mensaje").fadeOut("slow"); }, 3000);
        },
        complete: function(data) {
            $(".c-loader").fadeOut("fast");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });
}

function guardar_boucher1(id_colegiado, fecha_inicio, year_fin, mes_fin, monto, nro_operacion, array_ids) {
    var formData = new FormData($("#form-adel-pago")[0]);
    var ruta = "../../server/pago-culqi.php";
    var jsonIds = JSON.stringify(array_ids);

    formData.append('id_colegiado', id_colegiado);
    formData.append('fecha_inicio', fecha_inicio);
    formData.append('year_fin', year_fin);
    formData.append('mes_fin', mes_fin);
    formData.append('monto', monto);
    formData.append('nro_operacion', nro_operacion);
    formData.append('array_ids', jsonIds);

    if (cont_file != 0) {

        if (nro_operacion != '') {
            $.ajax({
                data: formData,
                url: ruta,
                type: 'post',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $(".c-loader").fadeIn("fast");
                },
                success: function(data) {

                    if (data == 1) {
                        mensaje_dialogo('verde', 'Pago Registrado');
                        location.reload();
                    } else {
                        mensaje_dialogo('rojo', 'Error al Guardar!');
                    }
                    setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
                },
                complete: function(data) {
                    $(".c-loader").fadeOut("fast");
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("La solicitud a fallado: " + textStatus);
                }
            });
        } else {
            mensaje_dialogo('rojo', 'Coloque el Nro de Operacion o Comprobante');
            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
        }


    } else {
        mensaje_dialogo('rojo', 'Agregue la imagen del Comprobante');
        setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
    }

}

function guardar_boucher_total(id_colegiado, nro_operacion, id_refinanciacion_atrasada, fecha_nuevo_inicio_at, fecha_actual_ultpago, monto_cuotas_pen_atra, total_a_pagar) {
    var formData = new FormData($("#form-adel-pago")[0]);
    var ruta = "../../server/subir-boucher-adelanto-total.php";


    formData.append('id_colegiado', id_colegiado);
    formData.append('nro_operacion', nro_operacion);
    formData.append('id_refinanciacion_atrasada', id_refinanciacion_atrasada);
    formData.append('fecha_nuevo_inicio_at', fecha_nuevo_inicio_at);
    formData.append('fecha_actual_ultpago', fecha_actual_ultpago);
    formData.append('monto_cuotas_pen_atra', monto_cuotas_pen_atra);
    formData.append('total_a_pagar', total_a_pagar);
    if (cont_file != 0) {

        if (nro_operacion != '') {
            $.ajax({
                data: formData,
                url: ruta,
                type: 'post',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $(".c-loader").fadeIn("fast");
                },
                success: function(data) {

                    if (data == 1) {
                        mensaje_dialogo('verde', 'Pago Registrado');
                        setTimeout(function() { location.reload(); }, 2000);
                    } else {
                        mensaje_dialogo('rojo', 'Error al Guardar!');
                    }
                    setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
                },
                complete: function(data) {
                    $(".c-loader").fadeOut("fast");
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log("La solicitud a fallado: " + textStatus);
                }
            });
        } else {
            mensaje_dialogo('rojo', 'Coloque el Nro de Operacion o Comprobante');
            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
        }


    } else {
        mensaje_dialogo('rojo', 'Agregue la imagen del Comprobante');
        setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
    }

}


function actualizar_estado_noti(id_colegiado) {

    $.ajax({
        data: {
            "id_colegiado": id_colegiado,
            "actualizar_estados": 'actualizar_estados'
        },
        url: "../../client/views/ajax.php",
        type: 'post',
        beforeSend: function() {
            $(".c-loader").fadeIn("fast");
        },
        success: function(data) {

            $(".modal-noti").slideUp("slow");
            location.reload();
            //setTimeout(function(){ $(".c-mensaje").fadeOut("slow"); }, 3000);
        },
        complete: function(data) {
            $(".c-loader").fadeOut("fast");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });
}

function load_mensajeria(id_colegiado) {
    $.ajax({
        data: {
            "id_colegiado": id_colegiado,
            "load_mensajeria": 'load_mensajeria'
        },
        url: "../../client/views/ajax.php",
        type: 'post',
        beforeSend: function() {
            $(".c-loader").fadeIn("fast");
        },
        success: function(data) {
            document.getElementById('c-modal-noti-id').innerHTML = data;

            $(".btn-salir-buzon").click(function() {
                $(".modal-noti").slideUp();
            });

            $(".btn-nueva-mensaje").click(function() {
                var id_colegiado = $(this).attr("id_colegiado");
                var id_detalle_inbox = $(this).attr("id_detalle_inbox");
                load_nuevo_mensaje(id_colegiado, id_detalle_inbox);
            });

            $(".select-tag-buzon").change(function() {
                var id_tag = $(this).val();
                var id_colegiado = $(".tag-buzon").attr("id_colegiado");
                load_lista_mensaje(id_tag, id_colegiado);
            });

            $(".item-li-buzon").click(function() {
                var asunto_detalle_inbox = $(this).attr("asunto_detalle_inbox");
                var id_detalle_inbox = $(this).attr("id_detalle_inbox");
                var id_tag = $(this).attr("id_tag");
                $(".item-li-buzon-active").attr("class", "item-li-buzon");
                $(this).attr("class", "item-li-buzon-active");
                load_detalle_mensaje(asunto_detalle_inbox, id_tag, id_detalle_inbox);
            });
            //setTimeout(function(){ $(".c-mensaje").fadeOut("slow"); }, 3000);
        },
        complete: function(data) {
            $(".c-loader").fadeOut("fast");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });
}


function load_nuevo_mensaje(id_colegiado, id_detalle_inbox) {
    $.ajax({
        data: {
            "id_colegiado": id_colegiado,
            "id_detalle_inbox": id_detalle_inbox,
            "load_form_mensaje": 'load_form_mensaje'
        },
        url: "../../client/views/ajax.php",
        type: 'post',
        beforeSend: function() {
            $(".c-loader").fadeIn("fast");
        },
        success: function(data) {

            document.getElementById("c-form-mensaje-id").innerHTML = data;
            //tinymce.init({selector:'.textarea-email'});
            $('.buscar-correo').select2();


            $('.buscar-correo').change(function() {
                var datos_usuario = $(this).val();
                var res = datos_usuario.split("$");
                var id_usuario = res[0];
                var nombre_compleo = res[1];

                $('.c-lista-destino').append('<div class="correo-destino d-flex" id_usuario="' + id_usuario + '" id="correo' + id_usuario + '" "><p class="mb-0">' + nombre_compleo + '</p><a class="del-correo" id_usuario="' + id_usuario + '">X</a></div>');

                $(".del-correo").click(function() {
                    var id_usuario = $(this).attr("id_usuario");
                    $("#correo" + id_usuario).remove();
                });


            });


            $(".btn-enviar-mensaje").click(function() {
                var id_colegiado = $(this).attr("id_colegiado");
                var id_detalle_inbox = $(this).attr("id_detalle_inbox");
                var asunto_detalle_inbox = $("#asunto_detalle_inbox").val();
                var descripcion_detalle_inbox = $(".textarea-email").val();
                var array_correos = new Array();
                $(".correo-destino").each(function() {
                    var id_usuario = $(this).attr("id_usuario");
                    array_correos.push(id_usuario);
                });

                if (array_correos != '') {
                    if (asunto_detalle_inbox != '') {
                        if (descripcion_detalle_inbox != '') {
                            enviar_mensaje_a_colegiados(id_colegiado, id_detalle_inbox, asunto_detalle_inbox, descripcion_detalle_inbox, array_correos);
                        } else {
                            mensaje_dialogo('rojo', 'Coloque el contenido del mensaje que desea enviar');
                            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
                        }
                    } else {
                        mensaje_dialogo('rojo', 'Coloque el asunto del mensaje que desea enviar');
                        setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
                    }
                } else {
                    mensaje_dialogo('rojo', 'Elija destinatario');
                    setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
                }


            });


            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
        },
        complete: function(data) {
            $(".c-loader").fadeOut("fast");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });
}

function enviar_mensaje_a_colegiados(id_colegiado, id_detalle_inbox, asunto_detalle_inbox, descripcion_detalle_inbox, array_correos) {
    var jsonCorreos = JSON.stringify(array_correos);

    $.ajax({
        data: {
            "id_colegiado": id_colegiado,
            "asunto_detalle_inbox": asunto_detalle_inbox,
            "descripcion_detalle_inbox": descripcion_detalle_inbox,
            "jsonCorreos": jsonCorreos,
            "enviar_mensaje": 'enviar_mensaje'
        },
        url: "../../client/views/ajax.php",
        type: 'post',
        beforeSend: function() {
            $(".c-loader").fadeIn("fast");
        },
        success: function(data) {

            if (data == 1) {
                mensaje_dialogo("verde", "Mensaje enviado Exitosamente");
                location.reload();
            } else {
                mensaje_dialogo("rojo", "ERROR al enviar mensaje");
            }

            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
        },
        complete: function(data) {
            $(".c-loader").fadeOut("fast");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });
}

function load_lista_mensaje(id_tag, id_colegiado) {
    $.ajax({
        data: {
            "id_tag": id_tag,
            "id_colegiado": id_colegiado,
            "load_bandeja": 'load_bandeja'
        },
        url: "../../client/views/ajax.php",
        type: 'post',
        beforeSend: function() {
            $(".c-loader").fadeIn("fast");
        },
        success: function(data) {
            document.getElementById('list-buzon-id').innerHTML = data;

            $(".item-li-buzon").click(function() {
                var asunto_detalle_inbox = $(this).attr("asunto_detalle_inbox");
                var id_detalle_inbox = $(this).attr("id_detalle_inbox");
                var id_tag = $(this).attr("id_tag");
                $(".item-li-buzon-active").attr("class", "item-li-buzon");
                $(this).attr("class", "item-li-buzon-active");
                load_detalle_mensaje(asunto_detalle_inbox, id_tag, id_detalle_inbox);
            });

            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
        },
        complete: function(data) {
            $(".c-loader").fadeOut("fast");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });
}

function load_detalle_mensaje(asunto_detalle_inbox, id_tag, id_detalle_inbox) {
    $.ajax({
        data: {
            "asunto_detalle_inbox": asunto_detalle_inbox,
            "id_detalle_inbox": id_detalle_inbox,
            "id_tag": id_tag,
            "load_detalle_mensaje": 'load_detalle_mensaje'
        },
        url: "../../client/views/ajax.php",
        type: 'post',
        beforeSend: function() {
            $(".c-loader").fadeIn("fast");
        },
        success: function(data) {

            document.getElementById('c-form-mensaje-id').innerHTML = data;


            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
        },
        complete: function(data) {
            $(".c-loader").fadeOut("fast");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });
}

function recalcular_monto() {
    var monto_total = 0;
    var cant_cuotas_pen_ref = 0;
    var suma_nro_items = 0;
    var meses_descuento = 0;
    var input_nro_items = 0;
    var num_c_pend = 0;
    var cantidad_items = 0;
    var calculo=0;
    var val_resta=0;
    var descuento_pol=$("#descuento-total").attr("descuento");
    $(".class-item-pagar").each(function() {
        cantidad_items++;
    });

    $(".class-item-pagar").each(function() {
        input_nro_items = $(this).children("td").children(".input-nro-items").val();
        num_c_pend = $(this).children("td").children(".input-nro-items").attr("num_c_pend");
        var id_duracion_meses = $(this).attr("duracion_meses");
        if (input_nro_items == '' || input_nro_items == 0) {
            input_nro_items = 0;
            num_c_pend = 0;
        }
        if (input_nro_items == 1) {
            num_c_pend = 1;
        }

        if (input_nro_items <= num_c_pend) {
            //num_c_pend = input_nro_items;
        } else {
            //num_c_pend = $(this).children("td").children(".input-nro-items").attr("num_c_pend");
        }

        if (input_nro_items > 0) {
            suma_nro_items = parseInt(suma_nro_items) + parseInt(input_nro_items);
            /* alert(suma_nro_items);*/
            /*CANTIDAD DE MESES DESCARTADOS*/
            /*alert(cantidad_items + ">" + 1 + "&&" + suma_nro_items + ">=" + 12);*/

        }


    });

    if (suma_nro_items >= 12) {

        nro_recorrido = suma_nro_items / 12;
        /*if (nro_recorrido > 0 && nro_recorrido < 1) {
            nro_recorrido = 1;
        }*/
        for (var i = 0; i < parseInt(nro_recorrido); i++) {

            meses_descuento += 2;

        }

        /*input_nro_items = input_nro_items - 2;*/
    }


    


    $(".class-item-pagar").each(function() {
        var input_nro_items = $(this).children("td").children(".input-nro-items").val();
        
        var cont_c_ref = $("#cont_c_ref").attr("valor");
        var tipo_aportacion = $(this).attr("tipo_aportacion");
        var id_cuota_refinanciacion = $(this).attr("id_cuota_refinanciacion");
        if (!id_cuota_refinanciacion) {
            id_cuota_refinanciacion = "0$0";
        }
        var id_cantidad_concepto_aportacion = $(this).children("td").children(".id_cantidad_concepto_aportacion").attr("cantidad_concepto_aportacion");
        var id_duracion_meses = $(this).attr("duracion_meses");

        var status_nro_items = $(this).children("td").children(".input-nro-items").prop("disabled");
        var fecha_inicio = $(this).children("td").children(".id_fecha_inicio").attr("fecha_inicio");


        var monto_cuota = id_cuota_refinanciacion.split("$");

        if (input_nro_items == '' || input_nro_items == 0) {
            input_nro_items = 0;
        }
        
        if (input_nro_items > 0) {
            //alert(input_nro_items);
            /*CANTIDAD CUOTAS REFINANCIADAS PUESTAS EN FORMULARIO DE PAGOS
            if (monto_cuota[1] != 0) {
                cant_cuotas_pen_ref++;
            }*/
            //alert(cant_cuotas_pen_ref + "<=" + cont_c_ref);
            /*if (cant_cuotas_pen_ref <= cont_c_ref && monto_cuota[1] == 0) {
                monto_cuota[1] = 0;
                alert(monto_cuota[1]);
                var item_fecha = fecha_inicio.split("/");
                var mes_item_fecha = item_fecha[1];
                var year_item_fecha = item_fecha[2];

                mes_item_fecha = parseInt(mes_item_fecha) + 1
                if (mes_item_fecha > 12) {
                    mes_item_fecha = 1;
                    year_item_fecha = parseInt(year_item_fecha) + 1;
                }
                alert(fecha_inicio);
                if (year_item_fecha > 2019) {
                    id_cantidad_concepto_aportacion = 15;
                }

            }*/
            //year_inicio = fecha_inicio.split("/");

            /*RECORRER CANTIDAD DE ITEMS
            if (fecha_inicio != "-" && status_nro_items == false && monto_cuota[1] == 0) {
                var recorre = 0;
                while (recorre < input_nro_items) {

                    
                    //alert(input_nro_items);
                    //alert(cont_c_ref);   
                    //alert(year_item_fecha);

                    var item_fecha = fecha_inicio.split("/");
                    var mes_item_fecha = item_fecha[1];
                    var year_item_fecha = item_fecha[2];

                    mes_item_fecha = parseInt(mes_item_fecha) + 1
                    if (mes_item_fecha > 12) {
                        mes_item_fecha = 1;
                        year_item_fecha = parseInt(year_item_fecha) + 1;
                    }

                    if (year_item_fecha > 2019) {
                        id_cantidad_concepto_aportacion = 15;
                    }

                    recorre++;
                }
            }*/


            var monto = 0;
            var monto1 = 0;
            var monto2 = 0;
            //alert(id_cantidad_concepto_aportacion);

            cuotas_descartadas = (parseInt(suma_nro_items / 12)) * 2;
            //alert(id_duracion_meses);
            
            if (id_duracion_meses == 1) {
                /*if (suma_nro_items >= 12) {

                    monto1 = parseInt(id_cantidad_concepto_aportacion) * (parseInt(input_nro_items) - 2);
                    monto2 = parseInt(monto_cuota[1]) * parseInt(num_c_pend);
                    monto = parseInt(monto1) + parseInt(monto2);
                } else {*/
                
                if (meses_descuento > 0) {
                    // alert(input_nro_items + "=" + input_nro_items + "-" + meses_descuento);

                    if (input_nro_items >= 12 && cantidad_items==1) {
                        calculo = input_nro_items / 12;
                        
                        if (calculo > 0 && calculo < 1) {
                            calculo = 1;
                        }
                        
                        val_resta = parseInt(calculo) * 2;
                       
                        input_nro_items = input_nro_items - val_resta;
                        meses_descuento = meses_descuento - val_resta;
                        
                        
                    }
                    

                    
                    if (input_nro_items == 1 || cantidad_items==2) {
                        input_nro_items = input_nro_items - 1;
                        meses_descuento--;
                    }

                }
                //alert(val_resta);
                //alert(parseInt(id_cantidad_concepto_aportacion) + "*" + parseInt(input_nro_items));
                monto1 = parseInt(id_cantidad_concepto_aportacion) * parseInt(input_nro_items);

                //alert(parseInt(monto_cuota[1]) + "*" + parseInt(num_c_pend));
                //alert(monto1);
                monto2 = parseFloat(monto_cuota[1]) * parseInt(num_c_pend);
                
                monto = parseInt(monto1) + parseFloat(monto2);
                //alert(parseInt(monto1) + "+" + parseInt(monto2));
                //alert(parseInt(monto_total) + "+" + parseInt(monto));
                /*}*/
            } else {
                monto = parseInt(id_cantidad_concepto_aportacion) * parseInt(input_nro_items);
            }

            monto_total = parseFloat(monto_total) + parseFloat(monto);
        }
    });

    if (input_nro_items == '' || input_nro_items == 0) {
        return 0;
    } else {
        return monto_total;
    }


}

function guardar_refinanciacion(id_colegiado, nro_operacion, fecha_contrato, array_meses, array_years, monto, monto_bruto, descuento, cuotas, mon_x_cuota) {
    var formData = new FormData($("#form-adel-pago")[0]);
    var ruta = "../../server/nueva-refinanciacion.php";

    var jsonMeses = JSON.stringify(array_meses);
    var jsonYears = JSON.stringify(array_years);

    formData.append('json_meses', jsonMeses);
    formData.append('json_years', jsonYears);
    formData.append('id_colegiado', id_colegiado);
    formData.append('monto', monto);
    formData.append('descuento', descuento);
    formData.append('cuotas', cuotas);
    formData.append('mon_x_cuota', mon_x_cuota);
    formData.append('nro_operacion', nro_operacion);
    formData.append('fecha_contrato', fecha_contrato);
    formData.append('monto_bruto', monto_bruto);

    if (descuento != 0) {
        if (fecha_contrato != '') {
            if (cuotas > 0) {

                if (nro_operacion != '') {

                    if (cont_file != 0) {

                        $.ajax({
                            data: formData,
                            url: ruta,
                            type: 'post',
                            contentType: false,
                            processData: false,
                            beforeSend: function() {
                                $(".c-loader").fadeIn("fast");
                            },
                            success: function(data) {
                                //alert(data);
                                if (data > 0) {

                                    mensaje_dialogo('verde', 'Refinanciacion guardada exitosamente!');
                                    setTimeout(function() { window.location.href = "../../client/views/vista-descargables.php?id_ref='ref'"; }, 500);
                                } else {
                                    mensaje_dialogo('rojo', 'Error Inesperado!');
                                }
                                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
                            },
                            complete: function(data) {
                                $(".c-loader").fadeOut("fast");
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log("La solicitud a fallado: " + textStatus);
                            }
                        });
                    } else {
                        mensaje_dialogo('rojo', 'Seleccione un comprobante');
                        setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
                    }
                } else {
                    mensaje_dialogo('rojo', 'Coloque el numero de operacion');
                    setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
                }
            } else {
                mensaje_dialogo('rojo', 'El numero de cuotas no puede ser 0!');
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
            }
        } else {
            mensaje_dialogo('rojo', 'Coloque la fecha de inicio del contrato');
            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
        }
    } else {
        mensaje_dialogo('rojo', 'No se pueden registrar montos en 0!');
        setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
    }


}

function calculo_refinanciado(cantidad, posicion, descuento, cuotas) {
    if ($('#mes-atra-ref' + posicion).prop('checked')) {
        posicion_sig = parseInt(posicion) + 1;

        if (posicion_sig < cantidad) {
            $('#mes-atra-ref' + posicion_sig).attr('disabled', false);
        }
        posicion_sig++;
        for (var i = posicion_sig; i < cantidad; i++) {
            $('#mes-atra-ref' + i).attr('disabled', true);
            $('#mes-atra-ref' + i).prop('checked', false);
        }
    } else {

        posicion = parseInt(posicion);
        if (posicion < cantidad) {
            posicion_sig = posicion + 1;

            for (var i = posicion_sig; i <= cantidad; i++) {

                $('#mes-atra-ref' + i).attr('disabled', true);
                $('#mes-atra-ref' + i).prop('checked', false);
            }
        }
    }

    var monto = 0;

    /*$("input:checkbox:checked").each(function(){
      monto = monto +10;
      
      document.getElementById('monto-ref-id').innerHTML='S/. '+monto;
      $('#monto-ref-id').attr('monto',monto);
    });*/

    $("input:checkbox").each(function() {
        var cantidad_cuota_actual = $('.cantidad-cuota-actual').val();
        if ($(this).prop('checked')) {
            monto = monto + parseInt(cantidad_cuota_actual);
        }

    });
    monto = monto - descuento;

    if (monto <= 0) {
        monto = 0;
    }

    return monto;
}

function guardar_boucher(id_colegiado, nro_operacion, array_id_cuota_refinanciacion, array_tipo_aportacion, array_id_concepto_aportacion, array_id_fecha_inicio, array_id_cantidad_concepto_aportacion, array_id_duracion_meses, array_input_nro_items, total_a_pagar, cuotas_descartadas) {

    var formData = new FormData($("#form-adel-pago")[0]);
    var ruta = "../../server/subir-boucher-adelanto.php";


    var jsonArray_id_cuota_refinanciacion = JSON.stringify(array_id_cuota_refinanciacion);
    var jsonArray_tipo_aportacion = JSON.stringify(array_tipo_aportacion);
    var jsonArray_id_concepto_aportacion = JSON.stringify(array_id_concepto_aportacion);
    var jsonArray_id_fecha_inicio = JSON.stringify(array_id_fecha_inicio);
    var jsonArray_id_cantidad_concepto_aportacion = JSON.stringify(array_id_cantidad_concepto_aportacion);
    var jsonArray_id_duracion_meses = JSON.stringify(array_id_duracion_meses);
    var jsonArray_input_nro_items = JSON.stringify(array_input_nro_items);

    formData.append('id_colegiado', id_colegiado);
    formData.append('nro_operacion', nro_operacion);

    formData.append('array_id_cuota_refinanciacion', jsonArray_id_cuota_refinanciacion);
    formData.append('array_tipo_aportacion', jsonArray_tipo_aportacion);
    formData.append('array_id_concepto_aportacion', jsonArray_id_concepto_aportacion);
    formData.append('array_id_fecha_inicio', jsonArray_id_fecha_inicio);
    formData.append('array_id_cantidad_concepto_aportacion', jsonArray_id_cantidad_concepto_aportacion);
    formData.append('array_id_duracion_meses', jsonArray_id_duracion_meses);
    formData.append('array_input_nro_items', jsonArray_input_nro_items);
    formData.append('total_a_pagar', total_a_pagar);
    formData.append('cuotas_descartadas', cuotas_descartadas);
    if (jsonArray_tipo_aportacion != "[]") {

        if (cont_file != 0) {

            if (nro_operacion != '') {
                if(total_a_pagar!=0){
                    $.ajax({
                        data: formData,
                        url: ruta,
                        type: 'post',
                        contentType: false,
                        processData: false,
                        beforeSend: function() {
                            $(".c-loader").fadeIn("fast");
                        },
                        success: function(data) {

                            if (data == 1) {
                                mensaje_dialogo('verde', 'Pago Registrado');
                                setTimeout(function() { window.location.reload(); }, 500);
                            } else {
                                mensaje_dialogo('rojo', 'Error al Guardar!');
                            }
                            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
                        },
                        complete: function(data) {
                            $(".c-loader").fadeOut("fast");
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log("La solicitud a fallado: " + textStatus);
                        }
                    });
                }else{
                    mensaje_dialogo('rojo', 'No puede realizar un pago con monto 0');
                    setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
                }
                
            } else {
                mensaje_dialogo('rojo', 'Coloque el Nro de Operacion o Comprobante');
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
            }


        } else {
            mensaje_dialogo('rojo', 'Agregue la imagen del Comprobante');
            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
        }

    } else {
        mensaje_dialogo('rojo', 'Elija algun concepto de pago');
        setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
    }




}


function culqi() {
    //alert(id_colegiado+"---"+nro_operacion+"---"+token+"---"+email+"---"+total_a_pagar_culqi+"---"+id_refinanciacion_atrasada+"---"+fecha_nuevo_inicio_at+"---"+fecha_actual_ultpago+"---"+monto_cuotas_pen_atra);
    if (Culqi.token) {

        if (val_diferencia <= 12 || val_diferencia == undefined) {
            var token = Culqi.token.id;
            var email = Culqi.token.email;

            var id_colegiado = $(".btn-guardar-pago").attr("id_colegiado");
            var nro_operacion = $("#nro_operacion").val();
            var total = $(".total-comprobante").attr("total_a_pagar");
            total = total * 100;
            var array_id_cuota_refinanciacion = new Array();
            var array_tipo_aportacion = new Array();
            var array_id_concepto_aportacion = new Array();
            var array_id_fecha_inicio = new Array();
            var array_id_cantidad_concepto_aportacion = new Array();
            var array_id_duracion_meses = new Array();
            var array_input_nro_items = new Array();



            $(".class-item-pagar").each(function() {

                var id_cuota_refinanciacion = $(this).attr("id_cuota_refinanciacion");
                var tipo_aportacion = $(this).attr("tipo_aportacion");
                var id_concepto_aportacion = $(this).attr("id_concepto_aportacion");
                var id_fecha_inicio = $(this).children("td").children(".id_fecha_inicio").attr("fecha_inicio");
                var id_cantidad_concepto_aportacion = $(this).children("td").children(".id_cantidad_concepto_aportacion").attr("cantidad_concepto_aportacion");
                var id_duracion_meses = $(this).attr("duracion_meses");
                var input_nro_items = $(this).children("td").children(".input-nro-items").val();

                //alert(tipo_aportacion+"---"+id_concepto_aportacion+"---"+id_fecha_inicio+"---"+id_cantidad_concepto_aportacion+"---"+id_cuota_refinanciacion+"---"+id_duracion_meses+"---"+input_nro_items);

                array_id_cuota_refinanciacion.push(id_cuota_refinanciacion);
                array_tipo_aportacion.push(tipo_aportacion);
                array_id_concepto_aportacion.push(id_concepto_aportacion);
                array_id_fecha_inicio.push(id_fecha_inicio);
                array_id_cantidad_concepto_aportacion.push(id_cantidad_concepto_aportacion);
                array_id_duracion_meses.push(id_duracion_meses);
                array_input_nro_items.push(input_nro_items);

            });


            var formData = new FormData($("#form-adel-pago")[0]);
            var ruta = "../../server/pago-culqi.php";

            var jsonArray_id_cuota_refinanciacion = JSON.stringify(array_id_cuota_refinanciacion);
            var jsonArray_tipo_aportacion = JSON.stringify(array_tipo_aportacion);
            var jsonArray_id_concepto_aportacion = JSON.stringify(array_id_concepto_aportacion);
            var jsonArray_id_fecha_inicio = JSON.stringify(array_id_fecha_inicio);
            var jsonArray_id_cantidad_concepto_aportacion = JSON.stringify(array_id_cantidad_concepto_aportacion);
            var jsonArray_id_duracion_meses = JSON.stringify(array_id_duracion_meses);
            var jsonArray_input_nro_items = JSON.stringify(array_input_nro_items);

            formData.append('id_colegiado', id_colegiado);
            formData.append('nro_operacion', nro_operacion);

            formData.append('array_id_cuota_refinanciacion', jsonArray_id_cuota_refinanciacion);
            formData.append('array_tipo_aportacion', jsonArray_tipo_aportacion);
            formData.append('array_id_concepto_aportacion', jsonArray_id_concepto_aportacion);
            formData.append('array_id_fecha_inicio', jsonArray_id_fecha_inicio);
            formData.append('array_id_cantidad_concepto_aportacion', jsonArray_id_cantidad_concepto_aportacion);
            formData.append('array_id_duracion_meses', jsonArray_id_duracion_meses);
            formData.append('array_input_nro_items', jsonArray_input_nro_items);
            formData.append('cuotas_descartadas', cuotas_descartadas);
            formData.append('total', total);
            formData.append('email', email);
            formData.append('token', token);

            if (jsonArray_tipo_aportacion != "[]") {


                $.ajax({
                    data: formData,
                    url: ruta,
                    type: 'post',
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $(".c-loader").fadeIn("fast");
                    },
                    success: function(data) {

                        if (data == 1) {
                            mensaje_dialogo('verde', 'Pago Registrado');
                            setTimeout(function() { location.reload(); }, 500);
                            //load_vista_pagos_col('pagos',id_colegiado);

                        } else {
                            mensaje_dialogo('rojo', 'Error, Verifique los Datos de su Tarjeta de Credito o Debito');
                        }
                        setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
                    },
                    complete: function(data) {
                        $(".c-loader").fadeOut("fast");
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log("La solicitud a fallado: " + textStatus);
                    }
                });


            } else {
                mensaje_dialogo('rojo', 'Elija algun concepto de pago');
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
            }


        } else if (val_diferencia == 'total') {

            var token = Culqi.token.id;
            var email = Culqi.token.email;

            var id_colegiado = $(".btn-guardar-pago-total-culqi").attr("id_colegiado");
            var nro_operacion = $("#nro_operacion").val();
            var id_refinanciacion_atrasada = $(".btn-guardar-pago-total-culqi").attr("id_refinanciacion_atrasada");
            var fecha_nuevo_inicio_at = $(".btn-guardar-pago-total-culqi").attr("fecha_nuevo_inicio_at");
            var fecha_actual_ultpago = $(".btn-guardar-pago-total-culqi").attr("fecha_actual_ultpago");
            var monto_cuotas_pen_atra = $(".btn-guardar-pago-total-culqi").attr("monto_cuotas_pen_atra");
            var total_a_pagar_culqi = $(".total-comprobante1").attr("total_a_pagar");
            total_a_pagar_culqi = total_a_pagar_culqi * 100;
            
            guardar_boucher_total_culqi(id_colegiado, nro_operacion, token, email, total_a_pagar_culqi, id_refinanciacion_atrasada, fecha_nuevo_inicio_at, fecha_actual_ultpago, monto_cuotas_pen_atra);

        } else {
            var token = Culqi.token.id;
            var email = Culqi.token.email;

            var diferencia = $(".btn-culqi-ref").attr("diferencia");
            var id_colegiado = $(".btn-culqi-ref").attr("id_colegiado");
            var array_meses = new Array();
            var array_years = new Array();
            var monto = $('#monto-ref-id').attr("monto");
            var monto_bruto = $('#monto-ref-id').attr("monto_bruto");
            var descuento1 = $('#descuento').val();
            descuento = descuento1 * 100;
            var nro_operacion = $("#nro_operacion").val();
            var fecha_contrato = $("#fecha_contrato").val();
            if (descuento == '') {
                descuento = 0;
            }
            var cuotas = $('#cuotas_id').val();
            if (cuotas == '') {
                cuotas = 0;
            }
            var mon_x_cuota = $('#mon_x_cuota').val();

            $("input:checkbox:checked").each(function() {
                var mes = $(this).attr("mes");
                var year = $(this).attr("year");
                array_meses.push(mes);
                array_years.push(year);
            });

            if (diferencia >= 13 && diferencia <= 48) {
                var monto_descuento = monto * 0.2;
                if (descuento1 >= monto_descuento) {
                    guardar_refinanciacion_culqi(id_colegiado, token, email, nro_operacion, fecha_contrato, array_meses, array_years, monto, monto_bruto, descuento, cuotas, mon_x_cuota);

                } else {
                    mensaje_dialogo('rojo', 'El adelanto debe ser como minimo de: S/.' + monto * 0.2);
                    setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
                }
            } else if (diferencia >= 49 && diferencia <= 144) {
                var monto_descuento = monto * 0.1;
                if (descuento1 >= monto_descuento) {
                    guardar_refinanciacion_culqi(id_colegiado, token, email, nro_operacion, fecha_contrato, array_meses, array_years, monto, monto_bruto, descuento, cuotas, mon_x_cuota);

                } else {
                    mensaje_dialogo('rojo', 'El adelanto debe ser como minimo de: S/.' + monto * 0.1);
                    setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
                }
            } else if (diferencia >= 145) {
                var monto_descuento = monto * 0.05;
                if (descuento1 >= monto_descuento) {
                    guardar_refinanciacion_culqi(id_colegiado, token, email, nro_operacion, fecha_contrato, array_meses, array_years, monto, monto_bruto, descuento, cuotas, mon_x_cuota);

                } else {
                    mensaje_dialogo('rojo', 'El adelanto debe ser como minimo de: S/.' + monto * 0.05);
                    setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
                }
            }
        }

    } else {
        HTMLFormControlsCollection.log(Culqi.error);
        alert(Culqi.error.mensaje);
    }
}

function guardar_refinanciacion_culqi(id_colegiado, token, email, nro_operacion, fecha_contrato, array_meses, array_years, monto, monto_bruto, descuento, cuotas, mon_x_cuota) {
    var formData = new FormData($("#form-adel-pago")[0]);
    var ruta = "../../server/nueva-refinanciacion-culqi.php";

    var jsonMeses = JSON.stringify(array_meses);
    var jsonYears = JSON.stringify(array_years);

    formData.append('json_meses', jsonMeses);
    formData.append('json_years', jsonYears);
    formData.append('id_colegiado', id_colegiado);
    formData.append('monto', monto);
    formData.append('descuento', descuento);
    formData.append('cuotas', cuotas);
    formData.append('mon_x_cuota', mon_x_cuota);
    formData.append('nro_operacion', nro_operacion);
    formData.append('fecha_contrato', fecha_contrato);
    formData.append('monto_bruto', monto_bruto);
    formData.append('token', token);
    formData.append('email', email);

    if (descuento != 0) {
        if (fecha_contrato != '') {
            if (cuotas > 0) {

                $.ajax({
                    data: formData,
                    url: ruta,
                    type: 'post',
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $(".c-loader").fadeIn("fast");
                    },
                    success: function(data) {
                        //alert(data);
                        $("#modal-pago-id").html("<iframe style='width:100%; height:550px;' src=" + data + "></iframe>");
                    },
                    complete: function(data) {
                        $(".c-loader").fadeOut("fast");
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log("La solicitud a fallado: " + textStatus);
                    }
                });

            } else {
                mensaje_dialogo('rojo', 'El numero de cuotas no puede ser 0!');
                setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
            }
        } else {
            mensaje_dialogo('rojo', 'Coloque la fecha de inicio del contrato');
            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
        }
    } else {
        mensaje_dialogo('rojo', 'No se pueden registrar montos en 0!');
        setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
    }


}

function guardar_boucher_total_culqi(id_colegiado, nro_operacion, token, email, total_a_pagar_culqi, id_refinanciacion_atrasada, fecha_nuevo_inicio_at, fecha_actual_ultpago, monto_cuotas_pen_atra) {
    var formData = new FormData($("#form-adel-pago")[0]);
    var ruta = "../../server/subir-boucher-adelanto-total-culqi.php";


    formData.append('id_colegiado', id_colegiado);
    formData.append('nro_operacion', nro_operacion);
    formData.append('id_refinanciacion_atrasada', id_refinanciacion_atrasada);
    formData.append('fecha_nuevo_inicio_at', fecha_nuevo_inicio_at);
    formData.append('fecha_actual_ultpago', fecha_actual_ultpago);
    formData.append('monto_cuotas_pen_atra', monto_cuotas_pen_atra);
    formData.append('token', token);
    formData.append('email', email);
    formData.append('total_a_pagar_culqi', total_a_pagar_culqi);

    $.ajax({
        data: formData,
        url: ruta,
        type: 'post',
        contentType: false,
        processData: false,
        beforeSend: function() {
            $(".c-loader").fadeIn("fast");
        },
        success: function(data) {
            //alert(data);
            $("#modal-pago-id").html("<iframe style='width:100%; height:550px;' src=" + data + "></iframe>");
        },
        complete: function(data) {
            $(".c-loader").fadeOut("fast");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });

}

function actualizar_fecha_limite(id_det_politica, fecha_limite) {

    $.ajax({
        data: {
            "id_det_politica": id_det_politica,
            "fecha_limite": fecha_limite
        },
        url: "../../client/views/ajax.php",
        type: 'post',
        beforeSend: function() {
            $(".c-loader").fadeIn("fast");
        },
        success: function(data) {

            if (data > 0) {
                mensaje_dialogo('verde', 'Politica Activada correctamente');
                setTimeout(function() { window.location.href = "../../client/views/vista-descargables.php?id_ref='ref'"; }, 500);
            } else {
                mensaje_dialogo('rojo', 'Fecha Invalida');
            }

            setTimeout(function() { $(".c-mensaje").fadeOut("slow"); }, 2000);
        },
        complete: function(data) {
            $(".c-loader").fadeOut("fast");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });
}