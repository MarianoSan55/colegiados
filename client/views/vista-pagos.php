<?php
session_start();
date_default_timezone_set("America/Lima");
?>
<!DOCTYPE html>
<html lang="es">
<?php include_once "header.php";?>
<?php include_once "config.php";?>
<?php include_once "../../server/phpmailer/phpmailer/PHPMailerAutoload.php";?>
<body>
    
    <?php
    if(isset($_SESSION['id_colegiado'])){
        $id_colegiado=$_SESSION['id_colegiado'];
        ?> 
        <div class="c-cuerpo-panel">       
        <div class="c-nav-usuario d-flex">  
            <div class="col-12 c-barra-horizontal pl-0 pr-0">
                <div class=" d-flex col-12 barra-horizontal pl-0 pr-0">
                    <div class="d-flex bar-h-bloque1">
                        <?php
                            include_once "../../server/conex.php";
                            //$id_colegiado=$_SESSION['id_colegiado'];
                            $query="SELECT * FROM `colegiado` WHERE `colegiado`.`id_colegiado`='$id_colegiado'";
                            $resultado =$conexion->query($query);
                            $num=mysqli_num_rows($resultado);
                            $row=$resultado->fetch_assoc();
                            $foto_perfil=$row['foto_colegiado'];
                            $estado_colegiado=$row['estado_colegiado'];
                            $fecha_suscripcion=$row['fecha_suscripcion'];
                            if($foto_perfil=='user.jpg'){
                                $foto_perfil='client/img/user.png';
                            }else{
                                $foto_perfil='client/img/users/'.$_SESSION['id_colegiado'].'/'.$foto_perfil; 
                            }
                            
                            /*VALIDADCION DE EXIXTENCIA DE NOTIFICACIONES*/
                            $style_noti='';
                            $class_noti='';
                            $query1="SELECT * FROM `anuncio`";
                            $resultado1 =$conexion->query($query1);
                            
                            
                            while ($row1=$resultado1->fetch_assoc()) {
                                $id_anuncio=$row1['id_anuncio'];
                                
                                $query="SELECT * FROM `estado_anuncio` WHERE `estado_anuncio`.`colegiado_id_colegiado`='$id_colegiado' AND `estado_anuncio`.`anuncio_id_anuncio`='$id_anuncio'";
                                $resultado =$conexion->query($query);
                                $num=mysqli_num_rows($resultado);
                                
                                if($num==0){
                                    $style_noti='style="display:block;"';
                                    $class_noti='noti';
                                }
                                

                            }
                        ?>
                        
                        <div class="d-flex item-bar-hor">
                            <?php 
                            $cant_deuda=0;
                            $query1="SELECT * FROM `aportacion` WHERE `aportacion`.`colegiado_id_colegiado`='$id_colegiado' AND `aportacion`.`colegiado_id_colegiado` IS NOT NULL AND `aportacion`.`estado_aportacion`='1' ORDER BY `aportacion`.`fecha_fin` DESC LIMIT 1 ";
                            $resultado1 =$conexion->query($query1);
                            $num=mysqli_num_rows($resultado1);
                            $row1=$resultado1->fetch_assoc();

                            $fecha_actual=date("Y-m-d");

                            if($row1>0){                        
                                $fecha_fin=$row1['fecha_fin'];
                            
                            }else{
                                $fecha_fin=$fecha_suscripcion;
                            }


                            $estado_deuda="";            

                            /*añadir un mes */
                            $year_fin = date("Y",strtotime($fecha_fin));
                            $mes_fin = date("m",strtotime($fecha_fin));

                            $nuevo_mes_fin=$mes_fin+1;
                            $nuevo_year_fin=$year_fin;
                            if($nuevo_mes_fin>12){
                                $nuevo_mes_fin=1;
                                $nuevo_year_fin=$year_fin+1;
                            }
                            
                            $nuevo_dia_fin= date("d",(mktime(0,0,0,$nuevo_mes_fin+1,1,$nuevo_year_fin)-1));
                            $nueva_fecha_fin=$nuevo_year_fin."-".zero_fill($nuevo_mes_fin,2)."-".$nuevo_dia_fin;

                            
                            if($estado_colegiado==3){
                                echo '<p class="item-estado-col-desa" title="Desasociado"></p><p class="c-estado-col  mb-0 pr-2" style="font-weight:bold;" for="">Desasociado</p>';
                            }else{
                            if($fecha_actual<=$nueva_fecha_fin){
                                    echo '<p class="item-estado-col-ok" title="Habilitado"></p><p class="c-estado-col mb-0 pr-2" style="font-weight:bold;" for="">Habilitado</p>';
                                }else{
                                    $cant_deuda++;
                                    echo '<p class="item-estado-col-des" title="Inhabilitado"></p><p class="c-estado-col mb-0 pr-2" style="font-weight:bold;" for="">Inhabilitado</p>';
                                } 
                            }

                            ?>
                        </div>
                        <div class="d-flex item-bar-hor">
                            <?php
                            /** TIPO DE PAGO */
                            $query2="SELECT * FROM `aportacion` WHERE `aportacion`.`colegiado_id_colegiado`='$id_colegiado' ORDER BY `aportacion`.`id_aportacion` DESC LIMIT 1 ";
                            $resultado2 =$conexion->query($query2);
                            //$num=mysqli_num_rows($resultado1);
                            $row2=$resultado2->fetch_assoc();
                            $tipo_pago=$row2['concepto_aportacion_id_concepto_aportacion'];
                            $t_pago='';
                            if($tipo_pago==1){
                                $t_pago='Regular';
                            }else if($tipo_pago==2){
                                $t_pago='Refinanciado';
                            }else if($tipo_pago==3){
                                $t_pago='Refinanciado';
                            }else if($tipo_pago==4){
                                $t_pago='Regular';
                            }else{
                                $t_pago='Regular';
                            }
                            ?>
                            <p class="c-estado-col ml-0 mb-0" style="font-weight:bold;" for=""><?php echo $t_pago;?></p>
                        </div> 
                    </div>
                    <ul class="encabezado pl-0">
                    
                                <li class="nav-item item-encabezado">
                                    <a class="nav-link c-cerrar" title="Cerrar Sesión" href="../../server/cerrar.php"><img src="<?php echo URL;?>client/img/logout.png" alt=""></a>
                                </li>
                                <li class="nav-item item-encabezado open-noti" id_colegiado="<?php echo $id_colegiado; ?>">
                                    <?php
                                    $query2="SELECT * FROM `anuncio`";
                                    $resultado2 =$conexion->query($query2);
                                    $existe_nuevo=0;
                                    while ($row2=$resultado2->fetch_assoc()) {
                                        $id_anuncio=$row2['id_anuncio'];
                                        
                                        $query="SELECT * FROM `estado_anuncio` WHERE `estado_anuncio`.`colegiado_id_colegiado`='$id_colegiado' AND `estado_anuncio`.`anuncio_id_anuncio`='$id_anuncio'";
                                        $resultado =$conexion->query($query);
                                        $num=mysqli_num_rows($resultado);
                                        
                                        if($num==0){
                                            $existe_nuevo++;
                                        }       
                                    }
                                    ?>   
                                    <a class="nav-link c-mensajes <?php if($existe_nuevo!=0){echo 'mensajes-nuevos ';}?>" id_colegiado="<?php echo $id_colegiado; ?>" title="Buzon de Mensajes" ><img src="<?php echo URL;?>client/img/ico-btn-noti.png" alt=""></a>
                                </li>
                                <li class="nav-item item-encabezado">
                                    <?php
                                    $query2="SELECT * FROM `inbox_colegiado` AS inusu 
                                            INNER JOIN `detalle_inbox` AS din 
                                            ON inusu.`id_inbox_colegiado`=din.`inbox_colegiado_id_inbox_colegiado` 
                                            WHERE inusu.`colegiado_id_colegiado` = '$id_colegiado' AND din.`origen`='usuario'";
                                    $resultado2 =$conexion->query($query2);
                                    $existe_nuevo=0;
                                    while ($row2=$resultado2->fetch_assoc()) {
                                        $fecha_leido_detalle_inbox=$row2['fecha_leido_detalle_inbox'];
                                        if($fecha_leido_detalle_inbox==NULL || $fecha_leido_detalle_inbox==''){
                                            $existe_nuevo++;
                                        }          
                                    }
                                    ?>   
                                    <a class="nav-link c-mensajes <?php if($existe_nuevo!=0){echo 'mensajes-nuevos ';}?>" id_colegiado="<?php echo $id_colegiado; ?>" title="Buzon de Mensajes" ><img src="<?php echo URL;?>client/img/ico-mensajes.png" alt=""></a>
                                </li>

                                
                    </ul>
                </div>
                
            </div> 
            
            <div id="sidebar" class="active">
                <div class="c-nombre-col mt-4">
                    <div class="navbar-brand item-bar-hor mr-0 c-img-user <?php echo $class_noti; ?>" id="noti-id" id_colegiado="<?php echo $id_colegiado; ?>">
                        <div <?php echo $style_noti; ?> class="c-ico-noti">                            
                        <!-- <img src="../../client/img/ico-noti.png" alt="" />-->
                        </div>
                        <img src="<?php echo URL.$foto_perfil;?>" alt="" />
                    </div>
                    <div class="c-nombres justify-content-center mt-2 item-bar-hor">
                        <h4 class="nombre-usuario  form-inline mb-0 text-center col-12"><?php echo $_SESSION['nombre_colegiado']." ".$_SESSION['apellido_paterno']." ".$_SESSION['apellido_materno'];?></h4>
                        <h4 class="nombre-usuario form-inline mb-0 text-center col-12"></h4>
                    </div>
                </div>

                <div class="toggle-btn" onclick="toggleSidebar()">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                
                <ul class="c-list-menu-v">
                    
                    <a href="../../client/views/vista-pagos.php"><li>Pagos</li></a>
                    <a href="../../client/views/vista-cuotas.php"><li>Cuotas</li></a>
                    <a href="../../client/views/vista-refinanciados.php"><li>Refinanciacion</li></a>
                    <a href="../../client/views/vista-descargables.php"><li>Descargables</li></a>
                    <?php
                    $query_exo="SELECT * FROM `detalle_politica` WHERE `detalle_politica`.`colegiado_id_colegiado`='$id_colegiado' AND `detalle_politica`.`politica_id_politica`='1'";
                    $resultado_exo =$conexion->query($query_exo);
                    $num_exo=mysqli_num_rows($resultado_exo);

                    if($num_exo>0){
                        ?>
                        <a href="../../client/views/vista-politica.php"><li class="item-politica">Modalidad de Pago</li></a>
                        <?php
                    }
                    ?>
                    
                    <a href="../../client/views/vista-configuracion.php"><li>Configuracion</li></a>
                </ul>
            </div>

        <div class="col-12 ">
            
            <!--<ul class="nav menu form-inline col-12" id="myTab" role="tablist">
                <li class="nav-item col-3 item-menu-active text-center">
                    <a class="nav-link justify-content-center d-flex" id="home-tab" data-toggle="tab" href="../../client/views/vista-pagos.php" role="tab" aria-controls="home" aria-selected="true">
                        <img class="" src="<?php echo URL;?>client/img/give-money.png" alt="">
                        <p class="  d-none d-lg-block">Registro Pagos</p>
                    </a>
                </li>
                <li class="nav-item col-3 item-menu text-center">
                    <a class="nav-link justify-content-center d-flex" id="profile-tab" data-toggle="tab" href="../../client/views/vista-cuotas.php" role="tab" aria-controls="profile" aria-selected="false">
                        <img class="" src="<?php echo URL;?>client/img/edit-document.png" alt="">
                        <p class=" d-none d-lg-block">Cuota Regular</p>
                    </a>
                </li>
                <li class="nav-item col-2 item-menu text-center">
                    <a class="nav-link justify-content-center d-flex" id="profile-tab" data-toggle="tab" href="../../client/views/vista-refinanciados.php" role="tab" aria-controls="profile" aria-selected="false">
                        <img class="" src="<?php echo URL;?>client/img/agreement.png" alt="">
                        <p class=" d-none d-lg-block">Refinanciada</p>
                    </a>
                </li>
                <li class="nav-item col-2 item-menu text-center">
                    <a class="nav-link justify-content-center d-flex" id="profile-tab" data-toggle="tab" href="../../client/views/vista-descargables.php" role="tab" aria-controls="profile" aria-selected="false">
                        <img class="" src="<?php echo URL;?>client/img/icon-desc.png" alt="">
                        <p class=" d-none d-lg-block">Descargables</p>
                    </a>
                </li>
                <li class="nav-item col-2 item-menu text-center">
                    <a class="nav-link justify-content-center d-flex" id="profile-tab" data-toggle="tab" href="../../client/views/vista-configuracion.php" role="tab" aria-controls="profile" aria-selected="false">
                        <img class="" src="<?php echo URL;?>client/img/settings-gears.png" alt="">
                        <p class=" d-none d-lg-block">Configuración</p>
                    </a>
                </li>
            </ul>-->
            <div class="modal-noti">
                <div class="c-modal-noti col-lg-8 col-md-10 col-sm-12 col-xs-12 " id="c-modal-noti-id">
                    <p class="cerrar-mpago">X Cerrar< /p>
                    <div>
                        
                    </div>
                    <div>
                        
                    </div>
                </div>
            </div>
            
            <div class="" id="c-vista-col-id">
                
                <div class="tab-content row col-12 align-items-center" id="c-cuotas-res-id">
                    <?php include_once "pagos.php"; ?> 
                </div>
                 <!--MENSAJE EN ZONA DE PAGOS-->
                 <div class="c-mensaje-tarjetas c-cuotas container align-self-end col-lg-9 col-md-12 col-sm-12 col-xs-12 pt-5 pr-5 pl-0">
                    <div>
                        <h2>¿Cómo pagar refinanciamiento?</h2>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/MYSuJo_NYKM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div>
                        <h2>Pago de cuota regular</h2>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/vGLDVzBhp6w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div>
                        <h2>Pago de otros conceptos</h2>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/yR9lTa6hQ-s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div>
                        <h2>Vista general de panel de usuario</h2>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/5dAvG4p7h1w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    
                    <div><h4>PUEDES PAGAR CON ESTAS TARJETAS</h4></div>
                    <div class="">
                        <img class="img-tarjetas" src="../img/tarjetas.png" alt="">
                    </div>                    
                    <div class="mensje-tarjetas">
                        <h6>DATOS IMPORTANTES:</h6>
                        <ul>1. Asegúrate de que tu tarjeta esta habilitada para realizar pagos en linea.</ul>
                        <ul>2. Si tu pago fue rechazado podría deberse a:
                            <li>- Saldo insuficiente.</li>
                            <li>- Tu entidad financiera no permite pagos en linea.</li>
                        </ul>
                        <ul>3. Recuerda, si tu pago fue rechazado puedes intentar pagar con otra tarjeta.</ul>
                        <ul>4. Los pagos realizados por transferencia o deposito tienen un plazo de revisión de 24 a 48 horas, después de aprobado el pago tendrás acceso a realizar nuevos pagos de cuotas o generación de refinanciamiento.</ul>
                    </div>
                 </div>
            </div> 
        </div>
     
            
            </div>
            
            
            
            
        </div>      
        <?php
    }else{
        ?>
        <div class="c-cuerpo">
            <?php include_once "login.php";?> 
        </div>
        <?php
    }
    ?>
    <div class="c-loader">
        <div class="loader"><img src="<?php echo URL;?>client/img/loader.gif" alt=""></div>
    </div>
    <div class="c-modal-adelanto justify-content-center">
        <div class="form-modal-adelanto">
            <div class="form-group row justify-content-center">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="adelanto" id="todos" value="3" checked>
                            <label class="label-filtro" for="todos">
                                3 Meses
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="adelanto" id="habilitados" value="6">
                            <label class="label-filtro" for="habilitados">
                                6 Meses
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="adelanto" id="inhabilitados" value="12">
                            <label class="label-filtro" for="inhabilitados">
                                1 Año
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="adelanto" id="deudores" value="24">
                            <label class="label-filtro" for="deudores">
                                2 Años
                            </label>
                        </div>
            </div>
            <div class="form-group">
                <label for="">Adjuntar Boucher de Pago:</label>
                <input type="file" value=""/>
            </div>
            <div class="form-group">
                <button type="button" class="btn-pagar-adelanto btn btn-success">Pagar</button>
                <button type="button" class="btn-cancelar-pago btn btn-danger">Cancelar</button>
            </div>
        </div>       
    </div>
    
    <?php
    include_once "footer.php";
    ?>
</body>
</html>
<?php
function zero_fill ($valor, $long = 0)
{
    return str_pad($valor, $long, '0', STR_PAD_LEFT);
}
?>


