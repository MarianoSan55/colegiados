<?php 

include_once "../../server/conex.php";

?>
<?php include_once "config.php";?>
<div class="c-cuotas container align-self-end col-lg-9 col-md-12 col-sm-12 col-xs-12 pt-5 pr-0 pl-5">   
    <h3 class="titulo-vista1 mb-0">Modalidad de Pago</h3> 
    <?php

    $query="SELECT * FROM `detalle_politica` AS dpol 
            WHERE dpol.`colegiado_id_colegiado` = '$id_colegiado' AND dpol.`politica_id_politica` = 1 ";
    $resultado =$conexion->query($query);
    $num=mysqli_num_rows($resultado);
    $row=$resultado->fetch_assoc();

    $id_det_politica=0;
    $fecha_limite='';
    $id_det_politica=$row['id_detalle_politica'];
    $fecha_limite=$row['fecha_limite'];
    ?>
    <div class="c-form-politica">
        <div class="group-form">
            <p>Estimado colegiado, a usted se le a dado la posibilidad de habilitar el beneficio 
                de continuar realizando sus pagos de manera normal, para habilitar esta alternativa,
                debe seleccionar una fecha limite hasta la cual usted debera ponerse al corriente en sus pagos.</p>
            <p>IMPORTANTE: no puede seleccionar una fecha posterior al 31 de julio de 2021 y ademas si luego de
            activar esta alternativa usted presenta un atrazo de 2 meses en sus pagos, este beneficio será anulado automaticamente.</p>
        </div>
        <div class="form-group text-center ">
            <label for="">Fecha Limite: </label>
            <?php 
            $status_btn='';
            $class_fecha='';
            if($fecha_limite!=''){
                $class_fecha='alert alert-success';
                $status_btn='disabled';
            }
            ?>
            <input type="date" <?php echo $status_btn; ?> class="<?php echo $class_fecha;?>" name="" id="fecha_limite" value="<?php echo $fecha_limite; ?>">
        </div>
        
        <div class="form-group text-center ">
            <input <?php echo $status_btn; ?> type="button" class="btn btn-success btn-guardar-fecha" id_det_politica="<?php echo $id_det_politica; ?>" value="Activar">
        </div>
    </div>
    
</div>