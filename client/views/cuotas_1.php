<?php 

include_once "../../server/conex.php";
if(isset($_POST['year'])){
    $year_now=$_POST['year'];
    $id_colegiado=$_POST['id_colegiado'];
}else{
    $year_now=$year_filtro;
}

?>
<?php include_once "config.php";?>
<?php
    
    $query="SELECT * FROM `aportacion` AS ap 
                    INNER JOIN `concepto_aportacion` AS cap 
                    ON ap.`concepto_aportacion_id_concepto_aportacion` = cap.`id_concepto_aportacion` 
                    WHERE ap.`colegiado_id_colegiado`='$id_colegiado'
                    AND cap.`id_concepto_aportacion`!=2
                    AND cap.`id_concepto_aportacion`!=3
                    AND (ap.`year` = '$year_now' OR YEAR(ap.`fecha_aportacion`)='$year_now')
                    ORDER BY ap.`id_aportacion` DESC";
    $resultado =$conexion->query($query);
    $num=mysqli_num_rows($resultado);

    if($num>0){
        ?>
            <table class="table-cuotas container">
                <thead>
                    <tr class="text-center">
                        <th scope="col">Mes Cuota</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Inicio</th>
                        <th scope="col">Vencimiento</th>
                        <th scope="col">Fecha Aportación</th>
                        <th scope="col">Comprobante</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    while ($row=$resultado->fetch_assoc()) {
                        $id_aportacion=$row['id_aportacion'];
                        $fecha_aportacion=$row['fecha_aportacion'];
                        if($fecha_aportacion==NULL){
                            $fecha_aportacion="-";
                        }
                        $fecha_inicio=$row['fecha_inicio'];
                        $fecha_fin=$row['fecha_fin'];
                        $url_boucher=$row['comprobante_id_comprobante'];

                                                        if($url_boucher!=NULL){
                                                            $query_comp="SELECT * FROM `comprobante` WHERE `comprobante`.`id_comprobante` = '$url_boucher'";
                                                            $resultado_comp =$conexion->query($query_comp);
                                                            //$num_comp=mysqli_num_rows($resultado_comp);
                                                            while ($row_comp=$resultado_comp->fetch_assoc()) {
                                                                $url_boucher=$row_comp['url_comprobante'];
                                                            }
                                                        }  
                        
                        $id_estado=$row['estado_aportacion'];
                        $id_tipo_aportacion=$row['id_concepto_aportacion'];
                        $nombre_tipo_aportacion=$row['nombre_concepto_aportacion'];
                        $id_aportacion_fk=$row['aportacion_id_aportacion'];
                        $metodo_pago=$row['metodo_pago_id_metodo_pago'];
                        $mes='';
                        $mes = date('m',strtotime($fecha_inicio));
                        if($id_tipo_aportacion==1){
                            
            
                            $mes = reemplazo_meses($mes);
                            $mes = $mes."-".$year_now;
                        }else if($id_tipo_aportacion==2){
                            $mes = 'REF'.$id_aportacion;
                        }else if($id_tipo_aportacion==3){
                            $mes = 'CUOTA';
                        }else if($id_tipo_aportacion==4 || $id_tipo_aportacion==1 || $id_tipo_aportacion==16){
                            $mes_i = reemplazo_meses($mes);
                            $year_i = date('Y',strtotime($fecha_inicio));
                            $mes_f = date('m',strtotime($fecha_fin));
                            $mes_f = reemplazo_meses($mes_f);
                            $year_f = date('Y',strtotime($fecha_fin));
                            $mes = '('.$mes_i.'-'.$year_i.') - ('.$mes_f.'-'.$year_f.')';
                        }else if($id_tipo_aportacion>4){
                            $mes=$nombre_tipo_aportacion;
                        }
            
                        /** ESTILO ESTADO APORTACION*/
                        $etiqueta='';
                        if($id_estado==1 && $id_aportacion_fk==NULL){
                            $estilo='item-cuota-check';
                            $etiqueta='<div class="item-cuota-check" title="Pagada"></div>';
                        }else if($id_estado==2 && $id_aportacion_fk==NULL){
                            $estilo='item-cuota-atrazada';
                            $etiqueta='<div class="item-cuota-atrazada" title="Atrazada"></div>';
                        }else if($id_estado==3 && $id_aportacion_fk==NULL){
                            $estilo='item-cuota';
                            $etiqueta='<div class="item-cuota" title="Pendiente"></div>';
                        }else if($id_estado==2 && $id_aportacion_fk!=NULL){
                            $estilo='item-cuota-ref';
                            $etiqueta='<div class="item-cuota-ref" title="Refinanciado"></div>';
                        }
                    ?>
                    <tr>
                        <td class="text-center" ><?php echo $mes;?></td>
                        <td class="text-center align-items-center"><?php echo $etiqueta;?></td>
                        <td class="text-center" ><?php if($fecha_fin!=NULL && $fecha_inicio!=NULL ){ echo date("d/m/Y",strtotime($fecha_inicio));}else{echo "-";}?></td>
                        <td class="text-center" ><?php if($fecha_fin!=NULL && $fecha_inicio!=NULL ){ echo date("d/m/Y",strtotime($fecha_fin));}else{echo "-";}?></td>
                        <td class="text-center" ><?php echo date("d/m/Y H:i:s", strtotime($fecha_aportacion));?></td>
                        <td class="text-center" >
                            <div id="c-btn-cuota-id<?php echo $id_aportacion;?>">
                                <?php
                                /*echo $url_boucher."---".$id_aportacion_fk;
                                if($url_boucher==NULL && $id_aportacion_fk==NULL){              
                                    ?> 
                                    <label class="btn btn-primary mb-0" for="boucher-id" style="display:none;">Subir</label>
                                    <button class="btn btn-primary pl-3 pr-3 ver-comprobante" url_boucher="<?php echo $url_boucher;?>" id="v<?php echo $id_aportacion;?>">Subir</button>   
                                    <input type="file" name="" id="boucher-id" style="display:none;">             
                                    <?php               
                                }else{*/
                                    if($id_aportacion_fk==NULL){
                                        if($metodo_pago==3){
                                        ?>
                                        <p>Pago en Linea</p>
                                        <?php
                                        }else{
                                            ?>
                                            <button class="btn btn-success pl-3 pr-3 ver-comprobante" url_boucher="<?php echo $url_boucher;?>" id="v<?php echo $id_aportacion;?>">Ver</button>   
                                            <?php
                                        }
                                    
                                    }else{
                                        ?>
                                        <button class="btn btn-secondary pl-3 pr-3 ver-comprobante"  disabled>Ver</button>   
                                        <?php  
                                    }
                                //}
                                ?>
                        </div>
                        </td>
                    </tr>
                    <div class="vista-modal-boucher form-inline justify-content-center mt-0 p-0" id="vmod<?php echo $id_aportacion;?>">
                         
                            <div class="modal-img col-lg-8 col-md-10 col-sm-12 col-xs-12 pt-5" id="modal-img-id<?php echo $id_aportacion;?>">
                                <div class="col-12 d-flex c-titulo">
                                        <h4 class="text-center col-10 titulo-vista">Detalle Comprobante</h4>
                                        <p class="btn-salir-noti cerrar-mod-img col-2" id="close<?php echo $id_aportacion;?>" >X</p>
                                        
                                    </div>
                                <!--<button class="ng-danger align-items-start cerrar-mod-img bg-danger" id="close<?php echo $id_aportacion;?>">X</button>   -->
                                <?php 
                                


                                /*
                                if($id_estado==1){
                                  
                                        $query2="SELECT * FROM `aportacion` WHERE `aportacion`.`id_aportacion` = '$id_aportacion'";
                                        $resultado2 =$conexion->query($query2);
                                        $row2=$resultado2->fetch_assoc();
                                        $id_comprobante=$row2['comprobante_id_comprobante'];


                                        $query3="SELECT * FROM `comprobante` WHERE `comprobante`.`id_comprobante` = '$id_comprobante'";
                                        $resultado3 =$conexion->query($query3);
                                        $row3=$resultado3->fetch_assoc();    
                                        $nro_operacion=$row3['nro_operacion'];
                                        ?>
                                        
                                        <?php

                                }else if ($id_estado==2) {
                                    # code...
                                }else if ($id_estado==3) {
                                    # code...
                                }*/



                                
                                if($id_aportacion_fk==NULL){
                                    if($metodo_pago==3){
                                        ?>
                                        <?php
                                    }else{
                                        ?>
                                    
                                        <img class="" src="<?php echo URL;?>client/img/users/<?php echo $id_colegiado;?>/boucher/<?php echo $url_boucher;?>" alt="">
                                    
                                    <?php
                                    }
                                    
                                }

                                /*else{
                                    ?>
                                    <form class="col-12 justify-content-center form-comp-atra d-flex" id="form<?php echo $id_aportacion;?>" id_form_cuota="<?php echo $id_aportacion;?>" method="post" enctype="multipart/form-data">
                                        <div class="col-6 c-total-pago comp-atra" style="background:#fff;">
                                            <div class="c-img-boucher-pago pb-2 pt-2">
                                                <div class="" url_boucher="<?php echo $url_boucher;?>" id="v<?php echo $id_aportacion;?>">
                                                    <img src="<?php echo URL;?>client/img/ico-boucher-2.png" alt="">
                                                </div>
                                                <label class="btn-adjuntar-comp" for="boucher<?php echo $id_aportacion;?>" >Adjuntar Comprobante</label>
                                                <input style="display:none;" type="file" name="boucher[]" class="control-boucher" id="boucher<?php echo $id_aportacion;?>" id_cuota="<?php echo $id_aportacion;?>" value="Subir Boucher">
                                                <button type="button" class="btn btn-success btn-save-boucher" id_cuota="<?php echo $id_aportacion;?>" id_colegiado="<?php echo $id_colegiado;?>">Guardar</button>
                                            </div>
                                        </div>
                                    
                                    </form>  
                                    <?php
                                }*/
                                ?>
                            </div>
                    </div>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        <?php
    }else{
        echo "<p class='mensaje-sin-cuotas pt-3 '>Usted no tiene nignuna Cuota Registrada</p>";
    }

    ?>
    

    
            <?php
            //reemplazo meses numeros a letras
            function reemplazo_meses($mes){
                /** NOMBRANDO MES */
                switch ($mes) {
                    case '01':
                        $mes='Enero';
                        break;
                        case '02':
                        $mes='Febrero';
                        break;
                        case '03':
                        $mes='Marzo';
                        break;
                        case '04':
                        $mes='Abril';
                        break;
                        case '05':
                        $mes='Mayo';
                        break;
                        case '06':
                        $mes='Junio';
                        break;
                        case '07':
                        $mes='Julio';
                        break;
                        case '08':
                        $mes='Agosto';
                        break;
                        case '09':
                        $mes='Setiembre';
                        break;
                        case '10':
                        $mes='Octubre';
                        break;
                        case '11':
                        $mes='Noviembre';
                        break;
                        case '12':
                        $mes='Diciembre';
                        break;
                    default:
                        $mes='ERROR';
                        break;
                }
                return $mes;
            }
            ?>
<!-- 
    <h4 class="pt-4 pb-2 text-center">Lista de Cuotas</h4>
    
   
    <ul class="cuotas pt-2 pb-2 col-12">
        <?php
        

        
        ?>
        <li class="li-item <?php echo $estilo; ?>" id="<?php echo $id_aportacion;?>"><?php echo $mes."-".$year_now." ".$etiqueta;?></li>
        <div class="det-item-cuota row" id="d<?php echo $id_aportacion;?>">
            <div class="grupo-cuota d-flex">
                <div class="form-group col-4">
                    <label for="">Fecha Inicio:</label><p><?php echo $fecha_inicio;?></p>
                </div>
                <div class="form-group col-4">
                    <label for="">Fecha fin:</label><p><?php echo $fecha_fin;?></p>
                </div>
                <div class="form-group col-4">
                    <label for="">Fecha Aportacion:</label><p><?php echo $fecha_aportacion;?></p>
                </div>
            </div>  
            <?php
            if($url_boucher==NULL && $id_aportacion_fk==NULL){              
                    ?>  
                    <form class="" id="form<?php echo $id_aportacion;?>" id_form_cuota="<?php echo $id_aportacion;?>" method="post" enctype="multipart/form-data">
                        <div class="form-group col-12">
                            <label for="">Subir Boucher</label>
                            <input type="file" name="boucher[]" class="control-boucher" id="boucher<?php echo $id_aportacion;?>" id_cuota="<?php echo $id_aportacion;?>" value="Subir Boucher">
                            <div class="vista-previa" url_boucher="<?php echo $url_boucher;?>" id="v<?php echo $id_aportacion;?>"><img src="<?php echo URL;?>client/img/ico-boucher.png" alt=""></div>
                        </div>    
                        <div class="form-group col-6">
                            <button type="button" class="btn btn-success btn-save-boucher" id_cuota="<?php echo $id_aportacion;?>" id_colegiado="<?php echo $id_colegiado;?>">Guardar</button>
                        </div>
                    </form>                    
                    <?php               
            }else{
                if($id_aportacion_fk==NULL){
                ?>
                    <form class="grupo-cuota" id="form<?php echo $id_aportacion;?>">
                        <div class="vista-previa" url_boucher="<?php echo $url_boucher;?>" id="v<?php echo $id_aportacion;?>"><img src="<?php echo URL;?>client/img/users/<?php echo $id_colegiado;?>/boucher/<?php echo $url_boucher;?>" alt=""></div>
                    </form>
                    <?php
                }
            }
            ?>
        </div>
        
        <?php
        
        ?>    
    </ul>


    
    -->