<?php
include "../../libs/dompdf/autoload.inc.php";
include_once "../../../server/conex.php";
use Dompdf\Dompdf;
$dompdf = new Dompdf();

$id_aportacion=$_GET['id_aportacion'];

ob_start(); //iniciamos un output buffer

/*definir que documento generar
$query_col="SELECT * FROM `aportacion` WHERE `aportacion`.`id_aportacion`='$id_aportacion'";
$resultado_col =$conexion->query($query_col);
$row_col=$resultado_col->fetch_assoc();
$id_colegiado_fer=$row_col['colegiado_id_colegiado'];

$query_politica="SELECT * FROM `detalle_politica` 
                WHERE `detalle_politica`.`colegiado_id_colegiado`='$id_colegiado_fer'
                AND `detalle_politica`.`fecha_limite` IS NOT NULL";
$resultado_politica =$conexion->query($query_politica);
$num_politica=mysqli_num_rows($resultado_politica);*/

/*if($num_politica==0){*/
  require_once('ref.php'); // llamamos el archivo que se supone contiene el html y dejamoso que se renderize
  
/*}else{
  require_once('ref_covid.php'); // llamamos el archivo que se supone contiene el html y dejamoso que se renderize
}*/


$dompdf->load_html(ob_get_clean());//y ponemos todo lo que se capturo con ob_start() para que sea capturado por DOMPDF

$dompdf->set_paper('A4', 'portrait');
$dompdf->render();
$dompdf->stream('document.pdf');


function zero_fill ($valor, $long = 0)
{
    return str_pad($valor, $long, '0', STR_PAD_LEFT);
}

function fechaCastellano ($fecha) {
    $fecha = substr($fecha, 0, 10);
    $numeroDia = date('d', strtotime($fecha));
    $dia = date('l', strtotime($fecha));
    $mes = date('F', strtotime($fecha));
    $anio = date('Y', strtotime($fecha));
    $dias_ES = array("lunes", "martes", "miércoles", "jueves", "viernes", "sábado", "domingo");
    $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
    $nombredia = str_replace($dias_EN, $dias_ES, $dia);
  $meses_ES = array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");
    $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
    return $nombredia." ".(int) $numeroDia." de ".$nombreMes." de ".$anio;
  }
?>
