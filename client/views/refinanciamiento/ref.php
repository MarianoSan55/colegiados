<?php

include_once "../../../server/conex.php";

//$id_aportacion=$_GET['id_aportacion'];

$query_cer="SELECT * FROM `aportacion` AS ap 
            INNER JOIN `colegiado` AS col 
            ON ap.`colegiado_id_colegiado` = col.`id_colegiado` 
            WHERE ap.`id_aportacion`='$id_aportacion' 
            AND ap.`concepto_aportacion_id_concepto_aportacion`='2'";
            //echo $query_cer;

            $nombe_colegiado=' - ';
            $apellido_paterno=' - ';
            $apellido_materno=' - ';
            $dni_colegiado=' - ';
            $telefono_colegiado=' - ';
            $correo_colegiado=' - ';
            $direccion_colegiado=' - ';
            $nro_colegiatura=' - ';
            $fecha_suscripcion=' - ';
            $fecha_aportacion=' - ';


$resultado_cer =$conexion->query($query_cer);
$row_cer=$resultado_cer->fetch_assoc();
$id_colegiado=$row_cer['id_colegiado'];
$nombe_colegiado=$row_cer['nombre_colegiado'];
$apellido_paterno=$row_cer['apellido_paterno'];
$apellido_materno=$row_cer['apellido_materno'];
$dni_colegiado=$row_cer['dni_colegiado'];
$estado_colegiado=$row_cer['estado_colegiado'];
if($row_cer['telefono_colegiado']!=NULL || $row_cer['telefono_colegiado']!=''){
    $telefono_colegiado=$row_cer['telefono_colegiado'];
}
if($row_cer['correo_colegiado']!=NULL || $row_cer['correo_colegiado']!=''){
    $correo_colegiado=$row_cer['correo_colegiado'];
}
if($row_cer['direccion_colegiado']!=NULL || $row_cer['direccion_colegiado']!=''){
    $direccion_colegiado=$row_cer['direccion_colegiado'];
}

$nro_colegiatura=$row_cer['nro_colegiatura'];
$fecha_suscripcion=$row_cer['fecha_suscripcion'];
$monto_aportacion=$row_cer['cantidad_aportacion'];

$fecha_inicio=$row_cer['fecha_inicio'];
$fecha_fin=$row_cer['fecha_fin'];

$nombre_completo=$apellido_paterno." ".$apellido_materno." ".$nombe_colegiado;

//echo $fecha_fin."#####".$fecha_actual;
$datetime1=new DateTime($fecha_inicio);
$datetime2=new DateTime($fecha_fin);

# obtenemos la diferencia entre las dos fechas
$interval=$datetime2->diff($datetime1);

# obtenemos la diferencia en meses
$intervalMeses=$interval->format("%m");

# obtenemos la diferencia en años y la multiplicamos por 12 para tener los meses
$intervalAnos = $interval->format("%y")*12;
            
$diferencia= $intervalMeses+$intervalAnos;

/*recalcular monto de promocion*/
$meses_desc=0;
$nueva_diferencia=0;
$monto_desc_promocion=0;
$monto_promocion=0;
$contador=$diferencia;
$porcentaje=0;

if($diferencia >= 13 && $diferencia <= 48){
    $meses_desc=$diferencia/12;
    $meses_desc=intval($meses_desc);
    $nueva_diferencia=$diferencia-$meses_desc;
    echo $diferencia;
    while($meses_desc>0){
        
        $monto_desc_promocion += 10;
        $meses_desc--;
        $contador--;
    }
    $porcentaje=20;
    $monto_promocion = $monto_aportacion - $monto_desc_promocion;
}else if($diferencia >= 49 && $diferencia <= 144){
    $meses_desc=$diferencia/12;
    $meses_desc=intval($meses_desc);
    $nueva_diferencia=$diferencia-$meses_desc;
    echo $diferencia;
    while($meses_desc>0){
        
        $monto_desc_promocion+=10;
        $meses_desc--;
        $contador--;
    }
    $porcentaje=10;
    $monto_promocion = $monto_aportacion - $monto_desc_promocion;
}else if($diferencia >= 145){
    $meses_desc=$diferencia/24;
    $meses_desc=intval($meses_desc)*2;
    $nueva_diferencia=$diferencia-$meses_desc;
    echo $diferencia;
    while($meses_desc>0){
        
        $monto_desc_promocion += 10;
        $meses_desc--;
        $contador--;
    }
    $porcentaje=5;
    $monto_promocion = $monto_aportacion - $monto_desc_promocion;
}

/*consulta detalle*/
$query_cer1="SELECT * FROM `aportacion` AS ap             
            WHERE ap.`aportacion_id_aportacion`='$id_aportacion'";
$resultado_cer1 =$conexion->query($query_cer1);
$row_cer1=$resultado_cer1->fetch_assoc();
$num_val1=mysqli_num_rows($resultado_cer1);

$monto_cuota=$row_cer1['cantidad_aportacion'];

/*identificar adelanto*/
$id_ap_adelanto=0;
$monto_adelanto=0;
while($id_ap_adelanto<5){
    $query_adel="SELECT * FROM `aportacion` 
    WHERE `aportacion`.`id_aportacion` 
    AND `aportacion`.`concepto_aportacion_id_concepto_aportacion`='5'
    AND `aportacion`.`colegiado_id_colegiado`='$id_colegiado'";
    $resultado_adel =$conexion->query($query_adel);
    $row_adel=$resultado_adel->fetch_assoc();
    $id_ap_adelanto=$row_adel['id_aportacion'];
    $monto_adelanto=$row_adel['cantidad_aportacion'];
    $fecha_gen_cert=$row_adel['fecha_aportacion'];
}

/*
$query1="SELECT * FROM `aportacion` WHERE `aportacion`.`colegiado_id_colegiado`='$id_colegiado' AND `aportacion`.`colegiado_id_colegiado` IS NOT NULL AND `aportacion`.`estado_aportacion`='1' ORDER BY `aportacion`.`fecha_fin` DESC LIMIT 1 ";
$resultado1 =$conexion->query($query1);
$num=mysqli_num_rows($resultado1);
$row1=$resultado1->fetch_assoc();

$fecha_actual=date("Y-m-d");
if($row1>0){                        
                        $fecha_fin=$row1['fecha_fin'];
                    
                    }else{
                        $fecha_fin=$fecha_suscripcion;
                    }



*/

/*calculo porcentaje*/
$porcentaje=$monto_adelanto*100/$monto_promocion;
$porcentaje=round($porcentaje,2);

/*definir cuota de aportacion*/
$year_fin = date("Y",strtotime($fecha_fin));
$mes_fin = date("m",strtotime($fecha_fin));

$nuevo_mes_fin=$mes_fin+1;
$nuevo_year_fin=$year_fin;
if($nuevo_mes_fin>12){
    $nuevo_mes_fin=1;
    $nuevo_year_fin=$year_fin+1;
}

$nuevo_dia_fin= date("d",(mktime(0,0,0,$nuevo_mes_fin+1,1,$nuevo_year_fin)-1));
$nueva_fecha_fin=$nuevo_year_fin."-".zero_fill($nuevo_mes_fin,2)."-".$nuevo_dia_fin;

$cuota_regular_con_ref=0;
if(date("Y",strtotime($nueva_fecha_fin))<=2019){
    $cuota_regular_con_ref=10;
}else{
    $cuota_regular_con_ref=15;
}

if ($estado_colegiado==4) {
    $cuota_regular_con_ref=0;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FORMATO</title>

    <link rel="stylesheet" href="ref.css">
</head>
<body>
    <div class="general">
        
        <div class="cont">
            <div class="c-t-nombre nro-ref">
                <h4 class="cod-ref">Nro. <?php echo $id_aportacion; ?> </h4>
            </div>
            
            <h1>FORMATO</h1>
            <h4>REFINANCIAMIENTO DE DEUDA POR EL IMPACTO DEL COVID19</h4>
            
            <br>
            <div class="c-bold-doc">
                <label for="">SEÑORES: ASOC. CIVIL COLEGIO DE ECONOMISTAS DE CAJAMARCA </label>
            </div>
            <div class="c-bold-doc">
                <label for="">A SOLICITUD DEL ASOCIADO</label>
            </div>
            <div class="c-bold-doc">
                <label for="">1. DATOS DEL SOLICITANTE</label>
            </div>
            <div class="c-t-nombre">
                <table class="t-nombre"> 
                    <tbody>
                        <tr>
                            <td><p><?php echo $apellido_paterno;?></p>  </td>
                            <td><p><?php echo $apellido_materno;?></p> </td>
                            <td><p><?php echo $nombe_colegiado;?></p> </td>
                        </tr>
                        <tr>
                            <td><strong>Apellido Paterno</strong>  </td>
                            <td><strong>Apellido Materno</strong> </td>
                            <td><strong>Nombres</strong> </td>
                        </tr>
                    </tbody>               
                        
                </table>
            </div>
            
            <br>
            <div class="c-t-nombre">
                <table>
                    <tr>
                        <td>DNI Nro:</td>
                        <td><p class="box"><?php echo $dni_colegiado; ?></p></td>
                        <td>Teléfono:</td>
                        <td><p class="box"><?php echo $telefono_colegiado; ?></p></td>
                        <td>Correo Electrónico:</td>
                        <td><p class="box"><?php echo $correo_colegiado; ?></p></td> 
                    </tr>
                    <tr>
                        <td>Domicialiado en:</td>
                        <td><p class="box"><?php echo $direccion_colegiado; ?></p></td>
                        <td>Reg. CEC Nro</td>
                        <td><p class="box"><?php echo $nro_colegiatura;?></p></td>
                    </tr>
                </table>
            </div>
            
            <br>
            <div class="c-bold-doc">
                <label for="">2. CONTENIDO DE LA SOLICITUD</label>
            </div>       
            <div class="c-det-condicion">
                <div class="item-formato">
                    <p>1. Solicito acogerme a la politica de cobranza para refinanciamiento de mi deuda total general que asciende a: <strong class="box"><?php echo "S/. ".$monto_aportacion; ?></strong></p>
                </div><br>
                <div class="item-formato">
                    <p>2. Que de acuerdo al sistema de pago del Colegio de Economistas de Cajamarca, mi deuda refinanciada incluye el beneficio, es de: <strong class="box"><?php echo "S/. ".$monto_promocion;?></strong></p>
                </div><br>
                <div class="item-formato">
                    <p>3. Me comprometo a cancelar el importe inicial de <strong class="box"><?php echo "S/. ".$monto_adelanto; ?></strong> que representa el <strong class="box"><?php echo $porcentaje; ?></strong>% del total de mi deuda refinanciada y la diferencia de<strong class="box"></strong>a ser refinanciada en <strong class="box"><?php echo $num_val1;?></strong> cuotas sin interés.</p>
                </div><br>             
               
            </div>  
            <br>
            <div class="c-bold-doc">
                <label for="">3. PLAN DE REFINANCIAMIENTO MENSUAL</label>
            </div>         
            <div class="c-det-condicion">
                <table>
                    <tbody>
                        <tr>
                            <td>Monto de cuota refinanciada:</td>
                            <td><label for="" class="box"><?php echo $monto_cuota; ?></label></td>
                        </tr>
                        <tr>
                            <td>Monto de cuota de aportacion regular: </td>
                            <td><label for="" class="box"><?php echo $cuota_regular_con_ref; ?></label></td>
                        </tr>
                        <tr>
                            <?php $nuevo_monto1=$monto_cuota+$cuota_regular_con_ref;?>
                            <td>Monto de cuota final a pagar:</td>
                            <td><label for="" class="box"><?php echo $nuevo_monto1;?></label></td>
                        </tr>
                    </tbody>
                </table>
                            
               
            </div> 
            <br>
            <div class="c-mensaje-pie">
                <p>En caso de incumplimiento del compromiso pactado, procederé a cancelar mi deuda total general especificada en el ITEM 1 del presente formato, deducido de los pagos realizados hasta la ultima fecha. </p>
                
            </div>
            
            <div class="c-t-nombre">
                <?php 
                //$fecha_actual1=date("Y-m-d");
                ?>
                <h4 class="cod-ref"><?php echo fechaCastellano($fecha_gen_cert); ?></h4>
            </div>
            
            
        </div>
    </div>
</body>
</html>




