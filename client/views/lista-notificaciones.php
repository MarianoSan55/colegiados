
<?php
            include_once "../../server/conex.php";
            $query2="SELECT * FROM `anuncio` ORDER BY `anuncio`.`fecha_anuncio` DESC";
            $resultado2 =$conexion->query($query2);
            $num=mysqli_num_rows($resultado2);
            
    $id_colegiado=$_POST['id_colegiado'];

            


            ?>
            <div class="col-12 d-flex c-titulo">
                <h4 class="text-center col-10 titulo-vista">Notificaciones</h4>
                <p class="btn-salir-noti col-2" id_colegiado="<?php echo $id_colegiado; ?>">X</p>
                
            </div>
                
              

                <div class="modal-noti">
                    <div class="c-modal-noti col-8 container m-3 p-3" id="modal-noti-id">
                        
                    </div>
                </div>

                <div class="modal-noti-confirm">
                    <div class="c-modal-noti-confirm col-8 m-3 p-3">
                        <h4 class="mensaje-del-noti"></h4>
                        <div class="">
                            <button type="submit" class="btn btn-success aceptar-del-noti" id_anuncio="0">Aceptar</button>
                            <button type="button" class="btn btn-danger btn-cerrar-confir-noti">Cerrar</button>
                        </div>
                    </div>
                </div>  

                <div class="c-lista-noti pt-3">
                    <ul class="mr-5">
                        <?php
                        if($num>0){
                            while ($row2=$resultado2->fetch_assoc()) {

                                $estado_nuevo='';

                                $id_anuncio=$row2['id_anuncio'];
                                $titulo_anuncio=$row2['titulo_anuncio'];
                                $descripcion_anuncio=$row2['descripcion_anuncio'];
                                $fecha_anuncio=$row2['fecha_anuncio'];
                                $url_anuncio=$row2['url_anuncio'];
                                
                                /*Validacion de anuncio y usuario*/
                                $query="SELECT * FROM `estado_anuncio` WHERE `estado_anuncio`.`colegiado_id_colegiado`='$id_colegiado' AND `estado_anuncio`.`anuncio_id_anuncio`='$id_anuncio'";
                                $resultado =$conexion->query($query);
                                $num=mysqli_num_rows($resultado);

                                if($num==0){
                                    $estado_nuevo='<p class="estado-noti">Nuevo</p>';
                                }

                                ?>
                                <li class="item-lista-noti p-2 mb-3">
                                    <div class="form-group d-flex mb-0">
                                        <h3 class="tit-noti col-10 mb-0"><img src="../../client/img/icon-noti.png" alt=""><?php echo $titulo_anuncio; ?> <?php echo $estado_nuevo; ?></h3>
                                        <p class="fecha-noti p-1 col-2 mb-0" for=""><?php echo date("d/m/Y h:i A",strtotime($fecha_anuncio));?></p>
                                    </div>
                                    <div class="form-group">
                                        <p class="desc-noti p-3"><?php echo $descripcion_anuncio; ?></p>
                                    </div>
                                    <div class="form-group">
                                        <?php
                                        if($url_anuncio!=NULL){ 
                                           ?>
                                           <a class="archivo-adjunto" target="_blank" href="<?php echo "../../../intranet/client/img/notificaciones/".$url_anuncio; ?>" target="_blank">Descargar Archivo Adjunto</a>
                                           <?php
                                        }
                                        ?>
                                        
                                    </div>
                                  
                                </li>
                                <?php
                            }
                        }else{
                            ?>
                            <p class="text-center mt-3 mb-2">Sin Notificaciones</p>
                            <?php
                        }
                        
                        ?>
                        
                    </ul>
                </div>