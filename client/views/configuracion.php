<?php 

$id_colegiado=$_SESSION['id_colegiado'];
date_default_timezone_set("America/Lima");

$query="SELECT * FROM `colegiado` AS col 
        WHERE col.`id_colegiado` = '$id_colegiado' ";
        $resultado =$conexion->query($query);
        $num=mysqli_num_rows($resultado);

        $row=$resultado->fetch_assoc();

        $nombre_colegiado=$row['nombre_colegiado'];
        $apellido_paterno=$row['apellido_paterno'];
        $apellido_materno=$row['apellido_materno'];
        $dni_colegiado=$row['dni_colegiado'];
        $sexo_colegiado=$row['sexo_colegiado'];
       
        $fecha_nacimiento=$row['fecha_nacimiento'];
        if($fecha_nacimiento==NULL){
            $fecha_nacimiento='';
        }else{
            $fecha_nacimiento=date('d-m-Y',strtotime($fecha_nacimiento));
        }
        
        $telefono_colegiado=$row['telefono_colegiado'];
        $correo_colegiado=$row['correo_colegiado'];
        $direccion_colegiado=utf8_encode($row['direccion_colegiado']);
        $nro_colegiatura=$row['nro_colegiatura'];
        $fecha_suscripcion=$row['fecha_suscripcion'];
        if($fecha_suscripcion==NULL){
            $fecha_suscripcion='';
        }

        /** VALIDACION DE ESTADO DE COLEGIADO */
        $cant_deuda=0;
                    $query1="SELECT * FROM `aportacion` WHERE `aportacion`.`colegiado_id_colegiado`='$id_colegiado' AND `aportacion`.`colegiado_id_colegiado` IS NOT NULL AND `aportacion`.`estado_aportacion`='1' ORDER BY `aportacion`.`fecha_fin` DESC LIMIT 1 ";
                    $resultado1 =$conexion->query($query1);
                    $num=mysqli_num_rows($resultado1);
                    $row1=$resultado1->fetch_assoc();

                    $fecha_actual=date("Y-m-d");

                    if($row1>0){                        
                        $fecha_fin=$row1['fecha_fin'];
                    
                    }else{
                        $fecha_fin=$fecha_suscripcion;
                    }


                    $estado_deuda="";            

                    if($estado_colegiado==3){
                        $id_estado_colegiado='Desasociado';
                    }else {
                        if($fecha_actual<=$fecha_fin){
                            $id_estado_colegiado='Habilitado';
                        }else{
                            $cant_deuda++;
                            $id_estado_colegiado='Inhabilitado';
                        }
                    }


                    

        /*$id_estado_colegiado=$row['estado_colegiado'];
        if($id_estado_colegiado==1){
            $id_estado_colegiado='Habilitado';
        }else if($id_estado_colegiado==2){
            $id_estado_colegiado='Inhabilitado';
        }else if($id_estado_colegiado==3){
            $id_estado_colegiado='Desasociado';
        }*/
        
        $foto_colegiado=$row['foto_colegiado'];
        $nickname_colegiado=$row['nickname_colegiado'];
        $clave_colegiado=$row['clave_colegiado'];

?>
<div class="c-cuotas container align-self-end col-lg-9 col-md-12 col-sm-12 col-xs-12 pt-5 pr-0 pl-5">
    <h3 class="titulo-vista1">Configuración</h3>
    <form class="c-form-config row mt-3" id="<?php echo $id_colegiado;?>" method="post" enctype="multipart/form-data">
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="form-group">
                <label for="formGroupExampleInput">Nombre Colegiado:</label>
                <input type="text" class="form-control" id="nombre_colegiado" placeholder="Nombre Colegiado" value="<?php echo $nombre_colegiado;?>" >
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Apellido Paterno:</label>
                <input type="text" class="form-control" id="apellido_paterno" placeholder="Apellido Paterno" value="<?php echo $apellido_paterno;?>" >
            </div> 
            <div class="form-group">
                <label for="formGroupExampleInput">Apellido Materno:</label>
                <input type="text" class="form-control" id="apellido_materno" placeholder="Apellido Materno" value="<?php echo $apellido_materno;?>" >
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">DNI:</label>
                <input type="text" class="form-control" id="dni_colegiado" placeholder="DNI" value="<?php echo $dni_colegiado;?>" >
            </div> 
            <div class="form-group">
                
                <label for="inputEmail4">Sexo</label>
                                        <select id="sexo_colegiado" class="form-control">
                                            <option value="0" <?php if($sexo_colegiado==0){echo "selected";}?>>Elija El Sexo</option>
                                            <option value="1" <?php if($sexo_colegiado==1){echo "selected";}?>>Masculino</option>
                                            <option value="2" <?php if($sexo_colegiado==2){echo "selected";}?>>Femenino</option>
                                        </select>
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Fecha Nacimiento:</label>
                <input type="text" class="form-control" id="datepicker" placeholder="dd-mm-aaaa" value="<?php echo $fecha_nacimiento;?>" >
            </div> 
            
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="form-group">
                <label for="formGroupExampleInput">Nro Colegiatura:</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Nro Colegiatura" value="<?php echo $nro_colegiatura;?>" disabled>
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Fecha Suscripcion:</label>
                <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Fecha Suscripción" value="<?php echo date('d-m-Y',strtotime($fecha_suscripcion));?>" disabled>
            </div> 
            <div class="form-group">
                <label for="formGroupExampleInput">Estado:</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Estado" value="<?php echo $id_estado_colegiado;?>" disabled>
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Telefono:</label>
                <input type="text" class="form-control" id="telefono_colegiado" placeholder="Telefono" value="<?php echo $telefono_colegiado;?>">
            </div> 
            <div class="form-group">
                <label for="formGroupExampleInput2">Dirección:</label>
                <input type="text" class="form-control" id="direccion_colegiado" placeholder="Dirección" value="<?php echo $direccion_colegiado;?>">
            </div> 
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="form-group">
                <label for="formGroupExampleInput">Correo Electrónico:</label>
                <input type="text" class="form-control" id="correo_colegiado" placeholder="Correo Electrónico" value="<?php echo $correo_colegiado;?>">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Usuario:</label>
                <input type="text" class="form-control" id="nickname_colegiado" placeholder="Usuario" value="<?php echo $nickname_colegiado;?>" >
            </div> 
            <div class="form-group">
                <label for="formGroupExampleInput2">Contraseña:</label>
                <input type="password" class="form-control" id="clave_colegiado" placeholder="Contraseña" value="<?php echo $clave_colegiado;?>">
            </div> 
            <div class="form-group justify-content-center">
                <label for="formGroupExampleInput2">Colocar Foto de Perfil:</label>
                <input type="file" class="form-control" id="foto_perfil_col" name="foto_perfil_col[]" placeholder="Subir Foto de Perfil" >
                <?php
                if($foto_colegiado=='user.jpg'){
                    $foto_colegiado='client/img/user.png';
                }else{
                    $foto_colegiado='client/img/users/'.$_SESSION['id_colegiado'].'/'.$foto_colegiado; 
                }
                ?>
                <div class="vista-previa-foto col-6" id="vista-previa-foto-id"><img src="<?php echo URL.$foto_colegiado;?>" alt=""></div>
            </div> 
        </div>
        <div class="form-group col-6">
            <button type="button" class="btn btn-success btn-save-config">Guardar</button>
        </div>
    </form> 
</div>