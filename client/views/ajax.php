<?php 
if(isset($_POST['valor'])){
    $valor=$_POST['valor'];
    switch ($valor) {
        case 'usuarios':
            $resultado=include_once "usuarios.php";
        break;
        case 'colegiados':
            $resultado=include_once "colegiados.php";
        break;
        case 'universidades':
            $resultado=include_once "universidades.php";
        break;
        case 'aportes':
            $resultado=include_once "aportes.php";
        break;
        case 'configuracion':
            $resultado=include_once "configuracion.php";
        break;


        case 'form-usuario':
            $resultado=include_once "form-usuario.php";
        break;
        default:
            
            break;
    }
}

if(isset($_POST['busqueda-usuario'])){
    $resultado=include_once "search-usuario.php";
}

if(isset($_POST['clave_actual']) && isset($_POST['clave_nueva'])){
    $resultado=include_once "../../server/configuracion.php";
}

if(isset($_POST['nombre_universidad']) && isset($_POST['id_universidad'])){
    $resultado=include_once "../../server/universidades.php";
}

if(isset($_POST['id_universidad']) && isset($_POST['estado'])){
    $resultado=include_once "../../server/del-universidades.php";
}

if(isset($_POST['id_usuario']) && isset($_POST['nuevo_usu'])){
    $resultado=include_once "../../server/nuevo-usuario.php";
}

if(isset($_POST['id_usuario']) && isset($_POST['nombre_usuario']) && isset($_POST['apellido_usuario']) && isset($_POST['nickname']) && isset($_POST['clave']) && isset($_POST['tipo']) && isset($_POST['estado'])){
    $resultado=include_once "../../server/guardar-usuario.php";
}

if(isset($_POST['nombre_universidad']) && isset($_POST['val_exixtencia_uni'])){
    $resultado=include_once "../../server/val-universidad.php";
}

if(isset($_POST['busqueda_colegiado']) && isset($_POST['filtro'])){
    include_once "search-colegiado.php";
}

if(isset($_POST['id_colegiado']) && 
    isset($_POST['nombre_colegiado']) &&
    isset($_POST['apellido_paterno']) &&
    isset($_POST['apellido_materno']) &&
    isset($_POST['dni']) &&
    isset($_POST['telefono_colegiado']) &&
    isset($_POST['correo']) &&
    isset($_POST['direccion']) &&
    isset($_POST['estado_civil']) &&
    isset($_POST['nro_colegiatura']) &&
    isset($_POST['fecha_suscripcion']) &&
    isset($_POST['lugar_nacimiento']) &&
    isset($_POST['fecha_nacimiento']) &&
    isset($_POST['estado']) &&
    isset($_POST['id_universidad']) ){
        $resultado=include_once "../../server/guardar-colegiado.php";
}

if(isset($_POST['id_colegiado']) && isset($_POST['nuevo_col'])){
    $resultado=include_once "../../server/nuevo-colegiado.php";
}

if(isset($_POST['year'])){
    $resultado=include_once "../../client/views/cuotas_1.php";
}

if(isset($_POST['load_notificaciones'])){
    $resultado=include_once "../../client/views/lista-notificaciones.php";
}

if(isset($_POST['id_colegiado']) && isset($_POST['actualizar_estados'])){
    $resultado=include_once "../../server/actualizar-notificaciones.php";
}

if(isset($_POST['id_colegiado']) && isset($_POST['load_mensajeria'])){
    $resultado=include_once "../../server/buzon.php";
}

if(isset($_POST['load_form_mensaje']) && isset($_POST['id_detalle_inbox'])){
    $resultado=include_once "../../server/form-mensaje.php";
}

if(isset($_POST['enviar_mensaje']) && isset($_POST['asunto_detalle_inbox']) && isset($_POST['descripcion_detalle_inbox']) && isset($_POST['jsonCorreos'])){
    $resultado=include_once "../../server/enviar-mensaje.php";
}

if(isset($_POST['load_bandeja']) && isset($_POST['id_tag'])){
    $resultado=include_once "../../server/lista-mensaje.php";
}

if(isset($_POST['load_detalle_mensaje']) && isset($_POST['asunto_detalle_inbox'])){
    $resultado=include_once "../../server/detalle-mensaje.php";
}

if(isset($_POST['id_det_politica'])){
    $resultado=include_once "../../server/actualizar-fecha-limite.php";
}
if(isset($_POST['load_mercado'])){
    $resultado=include_once "../../client/views/mercado-prueba.php";
}
?>