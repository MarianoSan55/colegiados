

<div class="c-cuotas container align-self-end col-lg-9 col-md-12 col-sm-12 col-xs-12 pt-5 pr-0 pl-5">    

    <div class="c-cuotas-desc mt-4 container">
                
                        <ul class="pb-4 pr-3">
                            <?php
                            $query_desc="SELECT * FROM `comprobante` AS comp 
                            INNER JOIN `aportacion` AS ap 
                            ON comp.`id_comprobante` = ap.`comprobante_id_comprobante` 
                            WHERE comp.`estado_comprobante` = 2 
                            AND ap.`colegiado_id_colegiado` = '$id_colegiado' ";
                            
                            $resultado_desc =$conexion->query($query_desc);
                            $num_desc=mysqli_num_rows($resultado_desc);
        
                            if($num_desc>0){
                                ?>
                                <h3>ATENCIÓN!!! Tiene usted operaciones rechazadas: </h3>
                                <?php
                                while ($row_desc=$resultado_desc->fetch_assoc()) {
                                    $id_comprobante=$row_desc['id_comprobante'];
                                    $estado_comprobante=$row_desc['estado_comprobante'];
                                    $nro_comprobante=$row_desc['nro_operacion'];
                                    ?>
                                    <li class="item-cuota-desc">
                                        <div class="col-12 tit-cuota-desc alert alert-danger">
                                            <label>Nro de Operacion: <?php echo $nro_comprobante;?></label>
                                        </div>
                                        <div class="det-cuota-desc">
                                        <?php
                                        $query="SELECT * FROM `aportacion`  
                                        WHERE `aportacion`.`comprobante_id_comprobante` = '$id_comprobante' ";
                                        
                                        $resultado4 =$conexion->query($query);
                                        $num4=mysqli_num_rows($resultado4);

                                        if($num4!=0){
                                                            ?>
                                                            <div>
                                                            
                                                            <?php
                                                            $i=1;
                                                            ?>
                                                            <div class="c-list-cuotas">
                                                                        <label>Cuotas relacionadas con esta operacion: </label>
                                                            <?php
                                                            while ($row4=$resultado4->fetch_assoc()) {
                                                                $id_tipo_aportacion=$row4['concepto_aportacion_id_concepto_aportacion'];
                                                                $monto=$row4["cantidad_aportacion"];
                                                                if($id_tipo_aportacion==3){
                                                                    $id_aportacion_ref='REF-'.$row4['aportacion_id_aportacion'];                                
                                                                    $monto='C.'.$i.'(S/. '.$monto.')';
                                                                    ?>
                                                                    <p class="alert alert-info"><?php echo $id_aportacion_ref.' '.$monto; ?></p>
                                                                    <?php
                                                                    
                                                                }else{
                                                                    $fecha_inicio=$row4['fecha_inicio'];
                                                                    $fecha_fin=$row4['fecha_fin'];

                                                                    $mes = date('m',strtotime($fecha_inicio));
                                                                    $year_i = date('Y',strtotime($fecha_inicio));
                                                                    if($id_tipo_aportacion==1){
                                                                        $mes = reemplazo_meses1($mes);
                                                                        $mes = $mes."-".$year_i;
                                                                    }else if($id_tipo_aportacion==2){
                                                                        $mes = 'REF'.$id_aportacion;
                                                                    }else if($id_tipo_aportacion==3){
                                                                        $mes = 'CUOTA';
                                                                    }else if($id_tipo_aportacion==4 || $id_tipo_aportacion==1 || $id_tipo_aportacion==16){
                                                                        $mes_i = reemplazo_meses1($mes);
                                                                        $year_i = date('Y',strtotime($fecha_inicio));
                                                                        $mes_f = date('m',strtotime($fecha_fin));
                                                                        $mes_f = reemplazo_meses1($mes_f);
                                                                        $year_f = date('Y',strtotime($fecha_fin));
                                                                        $mes = '('.$mes_i.'-'.$year_i.') - ('.$mes_f.'-'.$year_f.')';
                                                                    }else if($id_tipo_aportacion>4){
                                                                        $mes=$nombre_tipo_aportacion;
                                                                    }
                                                                    ?>
                                                                    <p class="alert alert-info"><?php echo $mes.' - (S/. '.$monto.')'?></p>
                                                                    <?php
                                                                }
                                                            
                                                                $i++;
                                                            }?>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                        </div>
                                    </li>
                                    <?php
                                }                            
                            
                            }
                            ?>
                        </ul>
                </div>
                


    <h3 class="titulo-vista1 ">Lista de Cuotas</h3>  

    <div class="form-group c-year-select col-12 d-flex pl-0 pr-0 pt-0 mt-0 mb-0 pb-0">
        <label class="col-4 etiq_sel_year mb-0 pb-0" for="">Año:</label>
                    <?php
                    /** OBTENER AÑOS CON PAGOS */
                    $query1="SELECT YEAR(`aportacion`.`fecha_inicio`) AS year_ini FROM `aportacion` 
                    WHERE `aportacion`.`colegiado_id_colegiado` = '$id_colegiado' 
                    AND `aportacion`.`fecha_inicio` IS NOT NULL
                    AND `aportacion`.`concepto_aportacion_id_concepto_aportacion`<>'3' 
                    AND `aportacion`.`concepto_aportacion_id_concepto_aportacion`<>'2'                   
                    GROUP BY YEAR(`aportacion`.`fecha_inicio`) ORDER BY YEAR(`aportacion`.`fecha_inicio`) DESC";
                    $resultado1 =$conexion->query($query1);
                    $num1=mysqli_num_rows($resultado1);

                    /** AÑO ACTUAL */
                    $year_now=date('Y');
                    $year_filtro = '';
                    ?>
                    <select name="" id="id_year" class="col-8" id_colegiado="<?php echo $_SESSION['id_colegiado'];?>">
                        <?php
                        $contador=0;
                        
                        while ($row1=$resultado1->fetch_assoc()) {
                            $year=$row1['year_ini'];
                            
                            if($contador==0){
                                $year_filtro =$year;
                            }
                            $contador++;

                        ?>
                            <option value="<?php echo $year;?>" <?php if($year_filtro==$year){echo 'selected';}?>><?php echo $year;?></option>
                        <?php                        
                        }  
                        echo $year_filtro;                      
                        ?>
                    </select>             
                  
                </div>

    <div class="col-12 pr-0 pl-0" id="c-cuotas-res-id">
        <?php include_once "cuotas_1.php"; ?> 
    </div>
    
    
</div>