<?php 

include_once "../../server/conex.php";

?>
<?php include_once "config.php";?>
<div class="c-cuotas container align-self-end col-lg-9 col-md-12 col-sm-12 col-xs-12 pt-5 pr-0 pl-5">   
    <h3 class="titulo-vista1 mb-0">Lista de Cuotas de Refinanciacion</h3> 
    <?php

    $query="SELECT * FROM `aportacion` AS ap
    INNER JOIN `concepto_aportacion` AS cap
    ON ap.`concepto_aportacion_id_concepto_aportacion`=cap.`id_concepto_aportacion`
    INNER JOIN `tipo_aportacion` AS tap
    ON cap.`tipo_aportacion_id_tipo_aportacion` = tap.`id_tipo_aportacion`
     WHERE ap.`colegiado_id_colegiado` = '$id_colegiado' AND ap.`concepto_aportacion_id_concepto_aportacion` = 2 ";
    $resultado =$conexion->query($query);
    $num=mysqli_num_rows($resultado);

    if($num>0){
       while ($row=$resultado->fetch_assoc()) {

            $estado_aportacion=$row['estado_aportacion'];
            if($estado_aportacion!=1){
                    $id_aportacion=$row['id_aportacion'];
                        $fecha_aportacion=$row['fecha_aportacion'];
                        if($fecha_aportacion==NULL){
                            $fecha_aportacion="-";
                        }
                        $fecha_inicio=$row['fecha_inicio'];
                        $fecha_fin=$row['fecha_fin'];
                        $url_boucher=$row['comprobante_id_comprobante'];

                                                        if($url_boucher!=NULL){
                                                            $query_comp="SELECT * FROM `comprobante` WHERE `comprobante`.`id_comprobante` = '$url_boucher'";
                                                            $resultado_comp =$conexion->query($query_comp);
                                                            //$num_comp=mysqli_num_rows($resultado_comp);
                                                            while ($row_comp=$resultado_comp->fetch_assoc()) {
                                                                $url_boucher=$row_comp['url_comprobante'];
                                                            }
                                                        }  
                        $id_estado=$row['estado_aportacion'];
                        $id_tipo_aportacion=$row['id_concepto_aportacion'];
                        $nombre_tipo_aportacion=$row['nombre_concepto_aportacion'];
                        $id_aportacion_fk=$row['aportacion_id_aportacion'];
                        $mes='';
                        $mes = date('m',strtotime($fecha_inicio));
                        if($id_tipo_aportacion==1){
                            
            
                            $mes = reemplazo_meses($mes);
                            //$mes = $mes."-".$year_now;
                        }else if($id_tipo_aportacion==2){
                            $mes = 'REF'.$id_aportacion;
                        }else if($id_tipo_aportacion==3){
                            $mes = 'CUOTA';
                        }else if($id_tipo_aportacion==4){
                            $mes_i = reemplazo_meses($mes);
                            $year_i = date('Y',strtotime($fecha_inicio));
                            $mes_f = date('m',strtotime($fecha_fin));
                            $mes_f = reemplazo_meses($mes_f);
                            $year_f = date('Y',strtotime($fecha_fin));
                            $mes = '('.$mes_i.'-'.$year_i.') - ('.$mes_f.'-'.$year_f.')';
                        }
            
                        /** ESTILO ESTADO APORTACION*/
                        $etiqueta='';
                        if($id_estado==1 && $id_aportacion_fk==NULL){
                            $estilo='item-cuota-check';
                            $etiqueta='<div class="item-cuota-check" title="Pagada"></div>';
                        }else if($id_estado==2 && $id_aportacion_fk==NULL){
                            $estilo='item-cuota-atrazada';
                            $etiqueta='<div class="item-cuota-atrazada" title="Atrazada"></div>';
                        }else if($id_estado==3 && $id_aportacion_fk==NULL){
                            $estilo='item-cuota';
                            $etiqueta='<div class="item-cuota" title="Pendiente"></div>';
                        }else if($id_estado==2 && $id_aportacion_fk!=NULL){
                            $estilo='item-cuota-ref';
                            $etiqueta='<div class="item-cuota-ref" title="Refinanciado"></div>';
                        }
            

            ?>

            <table class="table-cuotas-ref container p-2">
                <thead>
                    <tr class="text-center">
                        <th scope="col"><?php echo $mes;?></th>
                        <th scope="col"><?php echo $etiqueta;?></th>
                        <th scope="col">Vencimiento</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <!--<th scope="col">Comprobante</th>-->
                    </tr>
                </thead>
                <tbody>
                    <?php

                    $query="SELECT * FROM `aportacion` AS ap 
                            INNER JOIN `concepto_aportacion` AS cap
                            ON ap.`concepto_aportacion_id_concepto_aportacion`=cap.`id_concepto_aportacion`
                            INNER JOIN `tipo_aportacion` AS tap
                            ON ap.`concepto_aportacion_id_concepto_aportacion` = tap.`id_tipo_aportacion`
                            WHERE ap.`aportacion_id_aportacion` = '$id_aportacion' AND ap.`concepto_aportacion_id_concepto_aportacion`=3 ";
                         
                    $resultado =$conexion->query($query);
                    $num=mysqli_num_rows($resultado);
                        $cont=1;
                    while ($row=$resultado->fetch_assoc()) {
                        $id_aportacion=$row['id_aportacion'];
                        $fecha_aportacion=$row['fecha_aportacion'];
                        if($fecha_aportacion==NULL){
                            $fecha_aportacion="-";
                        }
                        $fecha_inicio=$row['fecha_inicio'];
                        $fecha_fin=$row['fecha_fin'];
                        $url_boucher=$row['comprobante_id_comprobante'];

                                                        if($url_boucher!=NULL){
                                                            $query_comp="SELECT * FROM `comprobante` WHERE `comprobante`.`id_comprobante` = '$url_boucher'";
                                                            $resultado_comp =$conexion->query($query_comp);
                                                            //$num_comp=mysqli_num_rows($resultado_comp);
                                                            while ($row_comp=$resultado_comp->fetch_assoc()) {
                                                                $url_boucher=$row_comp['url_comprobante'];
                                                            }
                                                        }  
                        $id_comprobante_est=$row['comprobante_id_comprobante']; 
                        $id_estado=$row['estado_aportacion'];
                        $id_tipo_aportacion=$row['id_concepto_aportacion'];
                        $nombre_tipo_aportacion=$row['nombre_concepto_aportacion'];
                        $id_aportacion_fk=$row['aportacion_id_aportacion'];
                        $cantidad_cuota=$row['cantidad_aportacion'];
                        $mes='';
                        $mes = date('m',strtotime($fecha_inicio));
                        if($id_tipo_aportacion==1){
                            
            
                            $mes = reemplazo_meses($mes);
                            $mes = $mes."-".$year_now;
                        }else if($id_tipo_aportacion==2){
                            $mes = 'REF'.$id_aportacion;
                        }else if($id_tipo_aportacion==3){
                            $mes = 'C.'.$cont++.'(S/. '.$cantidad_cuota.')';
                        }else if($id_tipo_aportacion==4){
                            $mes_i = reemplazo_meses($mes);
                            $year_i = date('Y',strtotime($fecha_inicio));
                            $mes_f = date('m',strtotime($fecha_fin));
                            $mes_f = reemplazo_meses($mes_f);
                            $year_f = date('Y',strtotime($fecha_fin));
                            $mes = '('.$mes_i.'-'.$year_i.') - ('.$mes_f.'-'.$year_f.')';
                        }
            
                        /** ESTILO ESTADO APORTACION*/
                        $etiqueta1='';
                        if($id_estado==1 && $id_aportacion_fk!=NULL){
                            $estilo1='item-cuota-check';
                            $etiqueta1='<div class="item-cuota-check" title="Pagada"></div>';
                        }else if($id_estado==2 && $id_aportacion_fk!=NULL){
                            $estilo1='item-cuota-atrazada';
                            $etiqueta1='<div class="item-cuota-atrazada" title="Atrazada"></div>';
                        }else if($id_estado==3 && $id_aportacion_fk!=NULL){
                            $estilo1='item-cuota';
                            $etiqueta1='<div class="item-cuota" title="Pendiente"></div>';
                        }else if($id_estado==2 && $id_aportacion_fk!=NULL){
                            $estilo1='item-cuota-ref';
                            $etiqueta1='<div class="item-cuota-ref" title="Refinanciado"></div>';
                        }

                        /*PREGUNTAR SI CUOTAS DE REFINANCIACION ESTAN EN ESTADO PENDIENTE*/
                            $query_est_ref="SELECT * FROM `comprobante` AS comp
                                        INNER JOIN `aportacion` AS ap
                                        ON comp.`id_comprobante`=ap.`comprobante_id_comprobante`
                                        WHERE comp.`id_comprobante`='$id_comprobante_est'
                                        AND ap.`estado_aportacion`=3";
                                        //echo $query_est_ref;
                                        $resultado_est_ref =$conexion->query($query_est_ref);
                                        $num_est_ref=mysqli_num_rows($resultado_est_ref);
                                        //echo $num_est_ref;
                                        if($num_est_ref>0){
                                            $estilo1='item-cuota';
                                            $etiqueta1='<div class="item-cuota" title="Pendiente"></div>';
                                        }
                        
                    ?>
                    <tr>
                        <td class="text-center" ><?php echo $mes;?></td>
                        <td class="text-center align-items-center"><?php echo $etiqueta1;?></td>
                        <td class="text-center" ><?php echo date("d/m/Y", strtotime($fecha_fin));?></td>
                        <td class="text-center" ></td>
                        <td class="text-center" ></td>
                        <!--<td class="text-center" >
                            <div id="c-btn-cuota-id<?php echo $id_aportacion;?>">
                                <?php
                                if($url_boucher==NULL && $id_aportacion_fk!=NULL){              
                                    ?> 
                                    <label class="btn btn-primary mb-0" for="boucher-id" style="display:none;">Subir</label>
                                    <button class="btn btn-primary pl-3 pr-3 ver-comprobante" url_boucher="<?php echo $url_boucher;?>" id="v<?php echo $id_aportacion;?>">Subir</button>   
                                    <input type="file" name="" id="boucher-id" style="display:none;">             
                                    <?php               
                                }else{
                                    if($id_aportacion_fk!=NULL){
                                    ?>
                                    <button class="btn btn-success pl-3 pr-3 ver-comprobante" url_boucher="<?php echo $url_boucher;?>" id="v<?php echo $id_aportacion;?>">Ver</button>   
                                    <?php
                                    }else{
                                        ?>
                                        <button class="btn btn-secondary pl-3 pr-3"  disabled>Ver</button>   
                                        <?php  
                                    }
                                }
                                ?>
                        </div>
                        </td>-->
                    </tr>
                    <div class="vista-modal-boucher form-inline justify-content-center" id="vmod<?php echo $id_aportacion;?>">
                        <button class="ng-danger align-items-start cerrar-mod-img bg-danger" id="close<?php echo $id_aportacion;?>">X</button>    
                            <div class="modal-img form-inline col-lg-8 col-md-10 col-sm-11" id="modal-img-id<?php echo $id_aportacion;?>">
                                <?php 
/*
                                    if($id_estado==1){
                                        ?>
                                        
                                        <?php

                                        
                                        $query2="SELECT * FROM `aportacion` WHERE `aportacion`.`id_aportacion` = '$id_aportacion'";
                                        $resultado2 =$conexion->query($query2);
                                        $row2=$resultado2->fetch_assoc();
                                        $id_comprobante=$row2['comprobante_id_comprobante'];


                                        $query3="SELECT * FROM `comprobante` WHERE `comprobante`.`id_comprobante` = '$id_comprobante'";
                                        $resultado3 =$conexion->query($query3);
                                        $row3=$resultado3->fetch_assoc();    
                                        $nro_operacion=$row3['nro_operacion'];
                                        ?>
                                        <div class="col-4">
                                                        <label class="label_nro_op_det">Nro. Operación: </label>
                                                        <label class="nro_op_det"><?php echo $nro_operacion; ?></label>
                                                    <?php
                                                        $query4="SELECT * FROM `aportacion` WHERE `aportacion`.`comprobante_id_comprobante` = '$id_comprobante'";
                                                        $resultado4 =$conexion->query($query4);
                                                        $num=mysqli_num_rows($resultado4);
                                                        
                                                        if($num!=0){
                                                            ?>
                                                            <div>
                                                            
                                                            <?php
                                                            $i=1;
                                                            ?>
                                                            <div class="c-list-cuotas">
                                                                        <label>Cuotas relacionadas con esta operacion: </label>
                                                            <?php
                                                            while ($row4=$resultado4->fetch_assoc()) {
                                                                $id_tipo_aportacion=$row4['concepto_aportacion_id_concepto_aportacion'];
                                                                $monto=$row4["cantidad_aportacion"];
                                                                if($id_tipo_aportacion==3){
                                                                    $id_aportacion_ref='REF-'.$row4['aportacion_id_aportacion'];                                
                                                                    $monto='C.'.$i.'(S/. '.$monto.')';
                                                                    ?>
                                                                    <p class="alert alert-info"><?php echo $id_aportacion_ref.' '.$monto; ?></p>
                                                                    <?php
                                                                    
                                                                }else{
                                                                    $fecha_inicio=$row4['fecha_inicio'];
                                                                    $fecha_fin=$row4['fecha_fin'];

                                                                    $mes = date('m',strtotime($fecha_inicio));
                                                                    $year_i = date('Y',strtotime($fecha_inicio));
                                                                    if($id_tipo_aportacion==1){
                                                                        $mes = reemplazo_meses3($mes);
                                                                        $mes = $mes."-".$year_i;
                                                                    }else if($id_tipo_aportacion==2){
                                                                        $mes = 'REF'.$id_aportacion;
                                                                    }else if($id_tipo_aportacion==3){
                                                                        $mes = 'CUOTA';
                                                                    }else if($id_tipo_aportacion==4){
                                                                        $mes_i = reemplazo_meses3($mes);
                                                                        $year_i = date('Y',strtotime($fecha_inicio));
                                                                        $mes_f = date('m',strtotime($fecha_fin));
                                                                        $mes_f = reemplazo_meses3($mes_f);
                                                                        $year_f = date('Y',strtotime($fecha_fin));
                                                                        $mes = '('.$mes_i.'-'.$year_i.') - ('.$mes_f.'-'.$year_f.')';
                                                                    }
                                                                    ?>
                                                                    <p class="alert alert-info"><?php echo $mes.' - (S/. '.$monto.')'?></p>
                                                                    <?php
                                                                }
                                                            
                                                                $i++;
                                                            }?>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                        </div>                   
                                        </div>
                                                    
                                        <div class="col-8 c-img2">
                                            <img class="" src="../../../colegiados/client/img/users/<?php echo $id_colegiado;?>/boucher/<?php echo $url_boucher;?>" alt="">
                                        </div>
                                            
                                        <?php
                                    }else if($id_estado==2){
                                        ?>
                                            <form class="col-12 justify-content-center form-comp-atra d-flex" id="form<?php echo $id_aportacion;?>" id_form_cuota="<?php echo $id_aportacion;?>" method="post" enctype="multipart/form-data">
                                                <div class="col-6 c-total-pago comp-atra" style="background:#fff;">
                                                    <div class="c-img-boucher-pago pb-2 pt-2">
                                                        
                                                            <label for="">Nro Operacion:</label>
                                                            <input type="text" name="" id="nro_operacion" class="col-12" placeholder="Nro Operacion"/>
                                                        
                                                        <div class="" url_boucher="<?php echo $url_boucher;?>" id="v<?php echo $id_aportacion;?>">
                                                            <img src="../../../colegiados/client/img/ico-boucher-2.png" alt="">
                                                        </div>
                                                        
                                                        <div class="form-control">
                                                        
                                                            <label class="btn-adjuntar-comp" for="boucher<?php echo $id_aportacion;?>" >Adjuntar Comprobante</label>
                                                            <input style="display:none;" type="file" name="boucher[]" class="control-boucher" id="boucher<?php echo $id_aportacion;?>" id_cuota="<?php echo $id_aportacion;?>" value="Subir Boucher">
                                                        </div>
                                                        <div class="form-control">
                                                            <button type="button" class="btn btn-success btn-save-boucher" id_cuota="<?php echo $id_aportacion;?>" id_colegiado="<?php echo $id_colegiado;?>">Guardar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                            </form>  
                                            <?php
                                    }else if($id_estado==3){
                                        ?>
                                         <?php

                                     
                                        $query2="SELECT * FROM `aportacion` WHERE `aportacion`.`id_aportacion` = '$id_aportacion'";
                                        $resultado2 =$conexion->query($query2);
                                        $row2=$resultado2->fetch_assoc();
                                        $id_comprobante=$row2['comprobante_id_comprobante'];


                                        $query3="SELECT * FROM `comprobante` WHERE `comprobante`.`id_comprobante` = '$id_comprobante'";
                                        $resultado3 =$conexion->query($query3);
                                        $row3=$resultado3->fetch_assoc();    
                                        $nro_operacion=$row3['nro_operacion'];
                                        ?>
                                        <div class="col-4">
                                                        <label class="label_nro_op_det">Nro. Operación: </label>
                                                        <label class="nro_op_det"><?php echo $nro_operacion; ?></label>
                                                    <?php
                                                        $query4="SELECT * FROM `aportacion` WHERE `aportacion`.`comprobante_id_comprobante` = '$id_comprobante'";
                                                        $resultado4 =$conexion->query($query4);
                                                        $num=mysqli_num_rows($resultado4);
                                                        
                                                        if($num!=0){
                                                            ?>
                                                            <div>
                                                            
                                                            <?php
                                                            $i=1;
                                                            ?>
                                                            <div class="c-list-cuotas">
                                                                        <label>Cuotas relacionadas con esta operacion: </label>
                                                            <?php
                                                            while ($row4=$resultado4->fetch_assoc()) {
                                                                $id_tipo_aportacion=$row4['concepto_aportacion_id_concepto_aportacion'];
                                                                $monto=$row4["cantidad_aportacion"];
                                                                if($id_tipo_aportacion==3){
                                                                    $id_aportacion_ref='REF-'.$row4['aportacion_id_aportacion'];                                
                                                                    $monto='C.'.$i.'(S/. '.$monto.')';
                                                                    ?>
                                                                    <p class="alert alert-info"><?php echo $id_aportacion_ref.' '.$monto; ?></p>
                                                                    <?php
                                                                    
                                                                }else{
                                                                    $fecha_inicio=$row4['fecha_inicio'];
                                                                    $fecha_fin=$row4['fecha_fin'];

                                                                    $mes = date('m',strtotime($fecha_inicio));
                                                                    $year_i = date('Y',strtotime($fecha_inicio));
                                                                    if($id_tipo_aportacion==1){
                                                                        $mes = reemplazo_meses($mes);
                                                                        $mes = $mes."-".$year_i;
                                                                    }else if($id_tipo_aportacion==2){
                                                                        $mes = 'REF'.$id_aportacion;
                                                                    }else if($id_tipo_aportacion==3){
                                                                        $mes = 'CUOTA';
                                                                    }else if($id_tipo_aportacion==4){
                                                                        $mes_i = reemplazo_meses($mes);
                                                                        $year_i = date('Y',strtotime($fecha_inicio));
                                                                        $mes_f = date('m',strtotime($fecha_fin));
                                                                        $mes_f = reemplazo_meses($mes_f);
                                                                        $year_f = date('Y',strtotime($fecha_fin));
                                                                        $mes = '('.$mes_i.'-'.$year_i.') - ('.$mes_f.'-'.$year_f.')';
                                                                    }
                                                                    ?>
                                                                    <p class="alert alert-info"><?php echo $mes.' - (S/. '.$monto.')'?></p>
                                                                    <?php
                                                                }
                                                            
                                                                $i++;
                                                            }?>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                        </div>                   
                                        </div>
                                                    
                                        <div class="col-8 c-img2">
                                            <img class="" src="../../../colegiados/client/img/users/<?php echo $id_colegiado;?>/boucher/<?php echo $url_boucher;?>" alt="">
                                        </div>
    
                                         <?php
                                    }
*/
                                    ?>
                            </div>
                    </div>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
                    <?php
                }

                
            }
    }else{
        echo "<p class='mensaje-sin-cuotas pt-3 '>Usted no tiene nignuna Refinanciacion</p>";
    }

    
            //reemplazo meses numeros a letras
            function reemplazo_meses($mes){
                /** NOMBRANDO MES */
                switch ($mes) {
                    case '01':
                        $mes='Enero';
                        break;
                        case '02':
                        $mes='Febrero';
                        break;
                        case '03':
                        $mes='Marzo';
                        break;
                        case '04':
                        $mes='Abril';
                        break;
                        case '05':
                        $mes='Mayo';
                        break;
                        case '06':
                        $mes='Junio';
                        break;
                        case '07':
                        $mes='Julio';
                        break;
                        case '08':
                        $mes='Agosto';
                        break;
                        case '09':
                        $mes='Setiembre';
                        break;
                        case '10':
                        $mes='Octubre';
                        break;
                        case '11':
                        $mes='Noviembre';
                        break;
                        case '12':
                        $mes='Diciembre';
                        break;
                    default:
                        $mes='ERROR';
                        break;
                }
                return $mes;
            }
            ?>
<!-- 
    <h4 class="pt-4 pb-2 text-center">Lista de Cuotas</h4>
    
   
    <ul class="cuotas pt-2 pb-2 col-12">
        <?php
        

        
        ?>
        <li class="li-item <?php echo $estilo; ?>" id="<?php echo $id_aportacion;?>"><?php echo $mes."-".$year_now." ".$etiqueta;?></li>
        <div class="det-item-cuota row" id="d<?php echo $id_aportacion;?>">
            <div class="grupo-cuota d-flex">
                <div class="form-group col-4">
                    <label for="">Fecha Inicio:</label><p><?php echo $fecha_inicio;?></p>
                </div>
                <div class="form-group col-4">
                    <label for="">Fecha fin:</label><p><?php echo $fecha_fin;?></p>
                </div>
                <div class="form-group col-4">
                    <label for="">Fecha Aportacion:</label><p><?php echo $fecha_aportacion;?></p>
                </div>
            </div>  
            <?php
            if($url_boucher==NULL && $id_aportacion_fk==NULL){              
                    ?>  
                    <form class="" id="form<?php echo $id_aportacion;?>" id_form_cuota="<?php echo $id_aportacion;?>" method="post" enctype="multipart/form-data">
                        <div class="form-group col-12">
                            <label for="">Subir Boucher</label>
                            <input type="file" name="boucher[]" class="control-boucher" id="boucher<?php echo $id_aportacion;?>" id_cuota="<?php echo $id_aportacion;?>" value="Subir Boucher">
                            <div class="vista-previa" url_boucher="<?php echo $url_boucher;?>" id="v<?php echo $id_aportacion;?>"><img src="<?php echo URL;?>client/img/ico-boucher.png" alt=""></div>
                        </div>    
                        <div class="form-group col-6">
                            <button type="button" class="btn btn-success btn-save-boucher" id_cuota="<?php echo $id_aportacion;?>" id_colegiado="<?php echo $id_colegiado;?>">Guardar</button>
                        </div>
                    </form>                    
                    <?php               
            }else{
                if($id_aportacion_fk==NULL){
                ?>
                    <form class="grupo-cuota" id="form<?php echo $id_aportacion;?>">
                        <div class="vista-previa" url_boucher="<?php echo $url_boucher;?>" id="v<?php echo $id_aportacion;?>"><img src="<?php echo URL;?>client/img/users/<?php echo $id_colegiado;?>/boucher/<?php echo $url_boucher;?>" alt=""></div>
                    </form>
                    <?php
                }
            }
            ?>
        </div>
        
        <?php
        
        ?>    
    </ul>


    
    -->
    
</div>