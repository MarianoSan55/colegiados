<?php 

include_once "../../server/conex.php";
if(isset($_POST['year'])){
    $year_now=$_POST['year'];
    $id_colegiado=$_POST['id_colegiado'];
}

/*--------------------------------CUOTA ACTUAL -----------------------------------

$query="SELECT * FROM `conf_cuota` ORDER BY `id_conf_cuota` DESC LIMIT 1";
            $resultado =$conexion->query($query);
            $row=$resultado->fetch_assoc();
            $cantidad_cuota=$row['cantidad_cuota'];
*/

?>

<input class="cantidad-cuota-actual" value="<?php //echo $cantidad_cuota; ?>" />

<?php include_once "config.php";?>
<div class="c-cuotas container align-self-end col-lg-9 col-md-12 col-sm-12 col-xs-12 mt-5 pt-5 pr-0 pl-0">   
    <div class="mensaje-pagos">
        <h5>Estimado Colegiado</h5>
        <p class="mb-0">El Consejo Directivo del Colegio de Economistas de Cajamarca hace de tu conocimiento que podrás acogerte a la política de cobranza para el presente período.
        Podrás cancelar 12 cuotas y sólo pagarás por 10, además de manera gratuita obtendrás un certificado de habilidad, existe más beneficios que el Colegio a preparado para ti.</p>
        <label class="mb-0">Comunícate al email: contacto@economistacajamarca.org.pe</label>
        <label class="mb-0">Celular: 978264406</label><br>
        <label class="mb-0">para mas información revise: 
            <a target="_blank" href="https://economistascajamarca.org.pe/politica-de-cobranza/">https://economistascajamarca.org.pe/politica-de-cobranza/</a>
        </label>
    </div>
    <div class="container form-inline">
        <h3 class="titulo-vista1">Realizar un nuevo pago</h3>
        <div class="c-link-otros1">
            <a href="vista-pagos.php"><<--Volver a Formulario de Pagos</a>
        </div>
        
        <?php 

        if(isset($_POST['id_colegiado'])){
            $id_colegiado=$_POST['id_colegiado'];

        }
        if(isset($_POST['year'])){
            $year_now=$_POST['year'];
            $id_colegiado=$_POST['id_colegiado'];
        }else{
            $year_now=date("Y");
        }

        /*--------------------------------CUOTA ACTUAL -----------------------------------

        $query="SELECT * FROM `conf_cuota` ORDER BY `id_conf_cuota` DESC LIMIT 1";
                    $resultado =$conexion->query($query);
                    $row=$resultado->fetch_assoc();
                    $cantidad_cuota=$row['cantidad_cuota'];*/

                    
        ?>
        <input class="cantidad-cuota-actual" value="<?php //echo $cantidad_cuota; ?>" />
        <?php include_once "config.php";?>
        <div class="c-cuotas row col-12">    
            <div class="form-group col-12 c-resultado-total pr-0 pl-0">
                <div class="col-12 d-flex c-titulo">
                    <h4 class="text-center col-10 titulo-vista">FORMULARIO DE PAGO</h4>
                </div>
                <!--VALIDACION DE ATRAZO-->
                <?php
                $cantidad_aportacion_atrasada=0;
                $fecha_actual_atrasado=date("Y-m-d");
                $query_val_atra="SELECT * FROM `aportacion` WHERE `aportacion`.`colegiado_id_colegiado` = '$id_colegiado' AND `aportacion`.`concepto_aportacion_id_concepto_aportacion`='2' AND `aportacion`.`estado_aportacion`='2'";
                $resultado_val_atra =$conexion->query($query_val_atra);
                $num_val_atra=0;
                
                $row_val_atra=$resultado_val_atra->fetch_assoc();
                $id_refinanciacion_atrasada=$row_val_atra['id_aportacion'];
                
                
                $query_cuota_ref_atra="SELECT * FROM `aportacion` WHERE `aportacion`.`aportacion_id_aportacion` = '$id_refinanciacion_atrasada' ";
                $resultado_cuota_ref_atra =$conexion->query($query_cuota_ref_atra);

                $contador_cuota_atra=0;
                $total_cuotas_canceladas=0;
                while ($row_cuota_ref_atra=$resultado_cuota_ref_atra->fetch_assoc()) {
                    $fecha_fin_cuota_atra=$row_cuota_ref_atra['fecha_fin'];
                    $estado_aportacion_atra=$row_cuota_ref_atra['estado_aportacion'];
                    $cantidad_aportacion_atra=$row_cuota_ref_atra['cantidad_aportacion'];

                    if($estado_aportacion_atra==2){
                        if($fecha_actual_atrasado > $fecha_fin_cuota_atra){
                            $contador_cuota_atra++;
                        }
                    }else if($estado_aportacion_atra==1){
                        $total_cuotas_canceladas=$total_cuotas_canceladas+$cantidad_aportacion_atra;
                    }
                    
                }


                /*CONSULTAR POR EXONERACION DE ATRAZO DE DEUDA*/
                $query_exis1="SELECT * FROM `detalle_politica` WHERE `detalle_politica`.`colegiado_id_colegiado`='$id_colegiado'";
                $resultado_exis1 =$conexion->query($query_exis1);
                $num_exis1=mysqli_num_rows($resultado_exis1);

                if($contador_cuota_atra>0 && $num_exis1==0){
                    $num_val_atra=mysqli_num_rows($resultado_val_atra);

                    /*CALCULO DE TOTAL A PAGAR*/

                    $query_exis_ultpago="SELECT * FROM `aportacion` AS ap
                                INNER JOIN `concepto_aportacion` AS cap
                                ON ap.`concepto_aportacion_id_concepto_aportacion`=cap.`id_concepto_aportacion`
                                WHERE ap.`colegiado_id_colegiado` = '$id_colegiado' 
                                AND (cap.`tipo_aportacion_id_tipo_aportacion` = '1' OR cap.`tipo_aportacion_id_tipo_aportacion` = '3') 
                                AND ap.`aportacion_id_aportacion` IS NULL
                                ORDER BY ap.`id_aportacion` DESC LIMIT 1";

                                $resultado_exis_ultpago =$conexion->query($query_exis_ultpago);
                                $row_exis_ultpago=$resultado_exis_ultpago->fetch_assoc();

                                $fecha_fin_ultpago=$row_exis_ultpago['fecha_fin'];

                                $fecha_actual_ultpago=date("Y-m-d");


                                /*$datetime1=new DateTime($fecha_fin_ultpago);
                                    $datetime2=new DateTime($fecha_actual_ultpago);
                                    
                                    # obtenemos la diferencia entre las dos fechas
                                    $interval=$datetime2->diff($datetime1);
                                    
                                    # obtenemos la diferencia en meses
                                    $intervalMeses=$interval->format("%m");
                                    
                                    # obtenemos la diferencia en años y la multiplicamos por 12 para tener los meses
                                    $intervalAnos = $interval->format("%y")*12;
                                                
                                    $diferencia= $intervalMeses+$intervalAnos+1;
                                    echo $diferencia;*/

                                    //añadir un mes
                                $mes_nuevo_final_at=date("m", strtotime($fecha_fin_ultpago));
                                $year_nuevo_final_at=date("Y", strtotime($fecha_fin_ultpago));
                                $mes_nuevo_final_at=$mes_nuevo_final_at+1;
                                
                                if($mes_nuevo_final_at>12){
                                    $mes_nuevo_final_at=1;
                                    $year_nuevo_final_at=$year_nuevo_final_at+1;
                                }
                                $fecha_nuevo_inicio_at=$year_nuevo_final_at."-".zero_fill($mes_nuevo_final_at,2)."-01";
                            
                                $fecha_inicio_cuota_adel=$fecha_nuevo_inicio_at;
                                $monto_cuotas_pen_atra=0;  
                                    while ($fecha_nuevo_inicio_at <= $fecha_actual_ultpago) {
                                        //echo $fecha_nuevo_inicio_at."<=".$fecha_actual_ultpago."<br>";
                                        $year_calculo=date("Y", strtotime($fecha_nuevo_inicio_at));

                                        if ($year_calculo<=2019) {
                                            $monto_cuotas_pen_atra=$monto_cuotas_pen_atra+10;
                                        }else if ($year_calculo>=2020){
                                            $monto_cuotas_pen_atra=$monto_cuotas_pen_atra+15;
                                        }

                                        //añadir un mes
                                        $mes_nuevo_final_at=date("m", strtotime($fecha_nuevo_inicio_at));
                                        $year_nuevo_final_at=date("Y", strtotime($fecha_nuevo_inicio_at));
                                        $mes_nuevo_final_at=$mes_nuevo_final_at+1;
                                        
                                        if($mes_nuevo_final_at>12){
                                            $mes_nuevo_final_at=1;
                                            $year_nuevo_final_at=$year_nuevo_final_at+1;
                                        }
                                        $fecha_nuevo_inicio_at=$year_nuevo_final_at."-".zero_fill($mes_nuevo_final_at,2)."-01";
                                        //echo $fecha_nuevo_inicio_at;
                                    }

/*IDENTIFICAR ADELANTE RELACIONADO A REFINANCIAMIENTO*/  
            $id_adelanto_ref=0;           
            while($id_adelanto_ref != 5){
                $query_adelanto_ref="SELECT * FROM `aportacion` WHERE `aportacion`.`colegiado_id_colegiado` = '$id_colegiado' AND `aportacion`.`concepto_aportacion_id_concepto_aportacion`='5'";
                $resultado_adelanto_ref =$conexion->query($query_adelanto_ref);
                $row_adelanto_ref=$resultado_adelanto_ref->fetch_assoc();
                $id_adelanto_ref=$row_adelanto_ref['concepto_aportacion_id_concepto_aportacion'];
                $adelanto_ref=$row_adelanto_ref['cantidad_aportacion'];
            }
            //echo $monto_cuotas_pen_atra;
            $cantidad_aportacion_atrasada=(($row_val_atra['cantidad_aportacion'] - $total_cuotas_canceladas) - $adelanto_ref) +$monto_cuotas_pen_atra;
            //echo $row_val_atra['cantidad_aportacion']."-".$total_cuotas_canceladas."+".$monto_cuotas_pen_atra;

                }else{
                    $cantidad_aportacion_atrasada=0;
                    $num_val_atra=0;
                }
                ?>
                
                <?php
                //if($num_val_atra==0){
                ?>
                <!--LISTA DE CONCEPTOS-->
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ml-0 mr-0 c-total-pago c-total-pago11" >                    
                    <div class="c-tag-conceptos1 d-flex">
                        <div class="col-6 d-flex">
                            <input checked type="radio" name="tag-conceptos" id="tag-conceptos1">
                            <label for="tag-conceptos1">Concepto</label>
                        </div>

                        <div class="col-6 d-flex">
                            <input type="radio" name="tag-conceptos" id="tag-conceptos2">  
                            <label for="tag-conceptos2">Cursos</label> 
                        </div>
                    </div>
                    <?php
                    /*----------------------------------OBTENER FECHA FINAL DE ULTIMO PAGO-----------------------------*/
                    /*array para mapeo de cuotas*/
                    $array_cuotas= Array();
                    
                    $boton_guardar='';
                            
                            $query="SELECT * FROM `colegiado` WHERE `colegiado`.`id_colegiado`='$id_colegiado'";

                                    $resultado =$conexion->query($query);
                                    $num=mysqli_num_rows($resultado);

                                    $row=$resultado->fetch_assoc();

                                    $fecha_suscripcion=$row['fecha_suscripcion'];



                            $query1="SELECT * FROM `aportacion` 
                                    WHERE `aportacion`.`colegiado_id_colegiado`='$id_colegiado' 
                                    AND `aportacion`.`colegiado_id_colegiado` IS NOT NULL 
                                    AND `aportacion`.`fecha_fin` IS NOT NULL 
                                    AND `aportacion`.`estado_aportacion`='1' 
                                    ORDER BY `aportacion`.`id_aportacion` DESC LIMIT 1 ";

                            $resultado1 =$conexion->query($query1);
                            $num=mysqli_num_rows($resultado1);
                            $row1=$resultado1->fetch_assoc();

                            $fecha_actual=date("Y-m-d");

                            if($row1>0){                        
                                $fecha_fin=$row1['fecha_fin'];
                               
                            }else{
                                $fecha_fin=$fecha_suscripcion;
                            }
                            /*---------------------------------------------*/

                    $query_exis_apor="SELECT * FROM `aportacion` 
                                        WHERE `aportacion`.`colegiado_id_colegiado`='$id_colegiado'";
                    $resultado_exis_apor =$conexion->query($query_exis_apor);
                    $num_exis_apor=mysqli_num_rows($resultado_exis_apor);
                   
                    /*----------------------------------VALIDACION DE INICIO DE FECHA INICIO---------------------*/
                            //OBTENER FECHA E INICIO DEL SIGUIENTE PAGO
                            $query="SELECT MONTH(MAX(`aportacion`.`fecha_fin`)) AS mes_max,YEAR(MAX(`aportacion`.`fecha_fin`)) as year_max, `aportacion`.`comprobante_id_comprobante` AS id_comprobante 
                                    FROM `aportacion` WHERE `aportacion`.`colegiado_id_colegiado`='$id_colegiado' 
                                    AND `aportacion`.`concepto_aportacion_id_concepto_aportacion` <> '3' 
                                    ORDER BY `aportacion`.`fecha_fin` DESC";
                            $resultado =$conexion->query($query);
                            
                            $num=mysqli_num_rows($resultado);
                            
                            $row=$resultado->fetch_assoc();
                            $id_comprobante=$row['id_comprobante'];
                            
                            //IDENTIFICADOR DE CONSULTA - SI SE USA MES Y AÑO DE SUSCRIPCION
                            $ident=0;
                            
                            if($id_comprobante=NULL){
                                $mes_max=$row['mes_max'];
                                $year_max=$row['year_max'];
                                //echo "////".$mes_max."////".$year_max."////";
                            }else{
                                //SEGUNDA CONSULTA
                                $query1="SELECT MONTH(MAX(ap.`fecha_fin`)) 
                                AS mes_max,YEAR(MAX(ap.`fecha_fin`)) as year_max, comp.`estado_comprobante` 
                                AS estado_comprobante FROM `aportacion` AS ap 
                                INNER JOIN `comprobante` AS comp 
                                ON ap.`comprobante_id_comprobante`=comp.`id_comprobante` 
                                WHERE ap.`colegiado_id_colegiado`='$id_colegiado' 
                                AND ap.`concepto_aportacion_id_concepto_aportacion` <> '3' 
                                ORDER BY ap.`fecha_fin` DESC ";

                                $resultado1 =$conexion->query($query1);
                                $row1=$resultado1->fetch_assoc();
                                $estado_comprobante=$row1['estado_comprobante'];
                                if($estado_comprobante==1){
                                    $mes_max=$row1['mes_max'];
                                    $year_max=$row1['year_max']; 
                                }else{
                                    $query3="SELECT * FROM `colegiado` WHERE `colegiado`.`id_colegiado`='$id_colegiado'";
                                    $resultado3 =$conexion->query($query3);
                                    $row3=$resultado3->fetch_assoc();
                                    $fecha_suscripcion=$row3['fecha_suscripcion'];
                                    $mes_max=date('m',strtotime($fecha_suscripcion));
                                    $year_max=date('Y',strtotime($fecha_suscripcion));
                                    $ident++;
                                }
                                
                            }

                            

                            if($num>0){
                                                                
                                if($mes_max==NULL || $year_max==NULL){
                                    $query1="SELECT * FROM `colegiado` WHERE `colegiado`.`id_colegiado`='$id_colegiado'";
                                    $resultado1 =$conexion->query($query1);
                                    $row1=$resultado1->fetch_assoc();
                                    $fecha_suscripcion=$row1['fecha_suscripcion'];
                                    $mes_max=date('m',strtotime($fecha_suscripcion));
                                    $year_max=date('Y',strtotime($fecha_suscripcion));
                                    
                                }else{
                                    if($ident==0){
                                        if($mes_max==12){
                                            $mes_max=1;
                                            $year_max++;
                                        }else{
                                            $mes_max++;
                                        }
                                    }                                    
                                    
                                }
                                
                            }else{
                                
                                $query1="SELECT * FROM `colegiado` WHERE `colegiado`.`id_colegiado`='$id_colegiado'";
                                $resultado1 =$conexion->query($query1);
                                $row1=$resultado1->fetch_assoc();
                                $fecha_suscripcion=$row1['fecha_suscripcion'];
                                $mes_max=date('m',strtotime($fecha_suscripcion));
                                $year_max=date('Y',strtotime($fecha_suscripcion));
                                
                            }

                            if(strlen($mes_max)<2){
                                $mes_max="0".$mes_max;
                            }
                    ?>
                    <ul class="list-conceptos pl-0 mb-0" id="list-conceptos-id1">
                        <?php
                        
                        $query_con="SELECT * FROM `concepto_aportacion` AS cap 
                                    INNER JOIN `tipo_aportacion` AS tap 
                                    ON cap.`tipo_aportacion_id_tipo_aportacion`=tap.`id_tipo_aportacion` 
                                    WHERE cap.`estado_concepto_aportacion` = '1' 
                                    AND tap.`id_tipo_aportacion` = '2'
                                    ORDER BY cap.`nombre_concepto_aportacion` ASC ";
                 
                        $resultado_con =$conexion->query($query_con);

                        while ($row_con=$resultado_con->fetch_assoc()) {
                            $id_concepto_aportacion=$row_con['id_concepto_aportacion'];
                            $nombre_concepto_aportacion=$row_con['nombre_concepto_aportacion'];
                            $cantidad_concepto_aportacion=$row_con['cantidad_concepto_aportacion'];
                            $tipo_aportacion=$row_con['tipo_aportacion_id_tipo_aportacion'];
                            $duracion_meses=$row_con['duracion_meses'];
                            $fecha_inicio_concepto_aportacion=$row_con['fecha_inicio_concepto_aportacion'];
                            $fecha_fin_concepto_aportacion=$row_con['fecha_fin_concepto_aportacion'];
                            $item='';

                            
                            if($tipo_aportacion==1){
                                $item='item-obligaorio';
                            }else if($tipo_aportacion==2){
                                $item='item-opcional';
                            }

                            $nombre_titulo=$nombre_concepto_aportacion;
                            $cantidad_mostrada='';
                            
                            if($cantidad_concepto_aportacion!=0){
                                $cantidad_mostrada="(S/. ".$cantidad_concepto_aportacion.")";
                            }

                            $nombre_concepto_aportacion = substr($nombre_concepto_aportacion, 0, 22);
                            
                            if(strlen($nombre_concepto_aportacion)>=22){
                                $nombre_concepto_aportacion = $cantidad_mostrada.$nombre_concepto_aportacion."...";
                            }else{
                                $nombre_concepto_aportacion = $cantidad_mostrada.$nombre_concepto_aportacion;
                            }
                            
                            /*validacion fecha de inicio*/
                            if($tipo_aportacion==1){
                                $fecha_inicio=$year_max."-".$mes_max."-01";
                            }else{
                                $fecha_inicio="-";
                            }
                           
                            /*---------------------------------VALIDACION PARA MOSTRAR CONCEPTOS CORRESPONDIENTES----------------------------------------------*/
                            /*consultar si existen aportaciones*/
                            
                            $query_exis="SELECT * FROM `aportacion` AS ap
                            INNER JOIN `concepto_aportacion` AS cap
                            ON ap.`concepto_aportacion_id_concepto_aportacion`=cap.`id_concepto_aportacion`
                            WHERE ap.`colegiado_id_colegiado` = '$id_colegiado' 
                            AND (cap.`tipo_aportacion_id_tipo_aportacion` = '1' OR cap.`tipo_aportacion_id_tipo_aportacion` = '3') 
                            AND ap.`aportacion_id_aportacion` IS NULL
                            ORDER BY ap.`id_aportacion` DESC LIMIT 1";


                            $resultado_exis =$conexion->query($query_exis);
                            $row_exis=$resultado_exis->fetch_assoc();

                            $fecha_fin=$row_exis['fecha_fin'];

                            $num_exis=mysqli_num_rows($resultado_exis);

                            if($num_exis==0){
                                $fecha_fin=$fecha_suscripcion;
                             }

                            /*---------------------COMPARANDO SI EL ATRASO ES SUPERIOR A 12 MESES---------------*/
                            $fecha_actual=date("Y-m-d H:i:s");

                            if($fecha_fin <= $fecha_actual){
                                //echo $fecha_fin."#####".$fecha_actual;
                                $datetime1=new DateTime($fecha_fin);
                                $datetime2=new DateTime($fecha_actual);
                                
                                # obtenemos la diferencia entre las dos fechas
                                $interval=$datetime2->diff($datetime1);
                                
                                # obtenemos la diferencia en meses
                                $intervalMeses=$interval->format("%m");
                                
                                # obtenemos la diferencia en años y la multiplicamos por 12 para tener los meses
                                $intervalAnos = $interval->format("%y")*12;
                                            
                                $diferencia= $intervalMeses+$intervalAnos;
                            }else{
                                $diferencia=0;
                            }

                            $mes_nuevo_final=date("m", strtotime($fecha_fin));
                            $year_nuevo_final=date("Y", strtotime($fecha_fin));
                            $mes_nuevo_final=$mes_nuevo_final+1;
                            
                            if($mes_nuevo_final>12){
                                $mes_nuevo_final=1;
                                $year_nuevo_final=$year_nuevo_final+1;
                            }
                           
                          
                            /*validacion fecha de inicio*/
                            if($tipo_aportacion==1){
                                $fecha_nuevo_inicio="01/".zero_fill($mes_nuevo_final,2)."/".$year_nuevo_final;
                            }else{
                                $fecha_nuevo_inicio="-";
                            }

                            if($num_exis==0){
                                if($id_concepto_aportacion==6 || $id_concepto_aportacion==14){
                                    
                                    ?>
                                    <li class="item-list-concepto d-flex" lista="1" id="item-list-id<?php echo $id_concepto_aportacion;?>">
                                        <a class="btn-add-con" id_colegiado="<?php echo $id_colegiado;?>" tipo_aportacion="<?php echo $tipo_aportacion; ?>" fecha_inicio="<?php echo $fecha_nuevo_inicio; ?>" id_concepto_aportacion="<?php echo $id_concepto_aportacion; ?>" nombre_concepto_aportacion="<?php echo $nombre_titulo; ?>" cantidad_concepto_aportacion="<?php echo $cantidad_concepto_aportacion; ?>" duracion_meses="<?php echo $duracion_meses; ?>" type="button">+</a>
                                        <p class="mb-0 nom-list-concepto <?php echo $item; ?>" title="<?php echo $nombre_titulo; ?>"><?php echo $nombre_concepto_aportacion; ?></p>
                                        
                                    </li>
                                    <?php 

                                }
                            }else{
                                
                                //if($diferencia<=12){
                                    if($tipo_aportacion==1){
                                        
                                            if($id_concepto_aportacion!=6){
                                                
                                                    ?>
                                                    <li class="item-list-concepto d-flex" lista="1" id="item-list-id<?php echo $id_concepto_aportacion;?>">
                                                        <a class="btn-add-con" id_colegiado="<?php echo $id_colegiado;?>" tipo_aportacion="<?php echo $tipo_aportacion; ?>" fecha_inicio="<?php echo $fecha_nuevo_inicio; ?>" id_concepto_aportacion="<?php echo $id_concepto_aportacion; ?>" nombre_concepto_aportacion="<?php echo $nombre_titulo; ?>" cantidad_concepto_aportacion="<?php echo $cantidad_concepto_aportacion; ?>" duracion_meses="<?php echo $duracion_meses; ?>" type="button">+</a>
                                                        <p class="mb-0 nom-list-concepto <?php echo $item; ?>" title="<?php echo $nombre_titulo; ?>"><?php echo $nombre_concepto_aportacion; ?></p>
                                                        
                                                    </li>
                                                    <?php 
                                               
                                            }                
                    
                                    }else{
                                        ?>
                                            <li class="item-list-concepto d-flex" lista="1" id="item-list-id<?php echo $id_concepto_aportacion;?>">
                                                <a class="btn-add-con" id_colegiado="<?php echo $id_colegiado;?>" tipo_aportacion="<?php echo $tipo_aportacion; ?>" fecha_inicio="<?php echo $fecha_nuevo_inicio; ?>" id_concepto_aportacion="<?php echo $id_concepto_aportacion; ?>" nombre_concepto_aportacion="<?php echo $nombre_titulo; ?>" cantidad_concepto_aportacion="<?php echo $cantidad_concepto_aportacion; ?>" duracion_meses="<?php echo $duracion_meses; ?>" type="button">+</a>
                                                <p class="mb-0 nom-list-concepto <?php echo $item; ?>" title="<?php echo $nombre_titulo; ?>"><?php echo $nombre_concepto_aportacion; ?></p>
                                                
                                            </li>
                                            <?php
                                    }
                                //}
                            } 

                        }
                        ?>
                        
                    </ul>
                    <ul class="list-conceptos pl-0" id="list-conceptos-id2">
                        <?php
                        $query_con="SELECT * FROM `concepto_aportacion` AS cap 
                                    INNER JOIN `tipo_aportacion` AS tap 
                                    ON cap.`tipo_aportacion_id_tipo_aportacion`=tap.`id_tipo_aportacion` 
                                    WHERE tap.`id_tipo_aportacion` = '4' 
                                    AND cap.`estado_concepto_aportacion` = '1'
                                    ORDER BY cap.`nombre_concepto_aportacion` ASC ";

                        $resultado_con =$conexion->query($query_con);

                        while ($row_con=$resultado_con->fetch_assoc()) {
                            $id_concepto_aportacion=$row_con['id_concepto_aportacion'];
                            $nombre_concepto_aportacion=$row_con['nombre_concepto_aportacion'];
                            $cantidad_concepto_aportacion=$row_con['cantidad_concepto_aportacion'];
                            $tipo_aportacion=$row_con['tipo_aportacion_id_tipo_aportacion'];
                            $duracion_meses=$row_con['duracion_meses'];
                            $fecha_ini_vigencia=$row_con['fecha_inicio_concepto_aportacion'];
                            $fecha_fin_vigencia=$row_con['fecha_fin_concepto_aportacion'];
                            $item='';

                            if($tipo_aportacion==1){
                                $item='item-obligaorio';
                            }else if($tipo_aportacion==2){
                                $item='item-opcional';
                            }

                            $nombre_titulo=$nombre_concepto_aportacion;
                            $cantidad_mostrada='';
                            
                            if($cantidad_concepto_aportacion!=0){
                                $cantidad_mostrada="(S/. ".$cantidad_concepto_aportacion.")";
                            }

                            $nombre_concepto_aportacion = substr($nombre_concepto_aportacion, 0, 22);
                            
                            if(strlen($nombre_concepto_aportacion)>=22){
                                $nombre_concepto_aportacion = $cantidad_mostrada.$nombre_concepto_aportacion."...";
                            }else{
                                $nombre_concepto_aportacion = $cantidad_mostrada.$nombre_concepto_aportacion;
                            }
                            
                            /*validacion fecha de inicio*/
                            if($tipo_aportacion==1){
                                $fecha_inicio="01/".$mes_max."/".$year_max;
                            }else{
                                $fecha_inicio="-";
                            }
                            
                            $fecha_actual_vigencia=date("Y-m-d");


                            if($fecha_ini_vigencia<=$fecha_actual_vigencia && $fecha_fin_vigencia>=$fecha_actual_vigencia){
                            ?>
                            <li class="item-list-concepto d-flex" lista="2" id="item-list-id<?php echo $id_concepto_aportacion;?>">
                                <a class="btn-add-con" id_colegiado="<?php echo $id_colegiado;?>" tipo_aportacion="<?php echo $tipo_aportacion; ?>"  fecha_inicio="<?php echo $fecha_inicio; ?>" id_concepto_aportacion="<?php echo $id_concepto_aportacion; ?>" nombre_concepto_aportacion="<?php echo $nombre_titulo; ?>" cantidad_concepto_aportacion="<?php echo $cantidad_concepto_aportacion; ?>" duracion_meses="<?php echo $duracion_meses; ?>" type="button">+</a>
                                <p class="mb-0 nom-list-concepto <?php echo $item; ?>" title="<?php echo $nombre_titulo; ?>"><?php echo $nombre_concepto_aportacion; ?></p>
                                
                            </li>
                            <?php
                            }
                        }
                        ?>
                        
                    </ul>
                </div>
                <?php
                //}
                ?>
                <!--FORMULARIO DE PAGO-->   
                <?php
                   /* if($num_val_atra!=0){
                        ?>
                        <h5 valor="<?php echo $num_val_atra;?>" class="mensaje-atrazo col-12">COLEGIADO DEBE CANCELAR TODA SU DEUDA POR ATRASO EN SUS CUOTAS <br><img src="../img/ico-admiracion.png"></h5>
                            <div class="col-12 c-t-pagar1">
                                
                                <div class="form-group">
                                    <label class="mb-0 pb-0 etiq-total" for="">TOTAL: </label>
                                    <?php
                                    $clase_total='';
                                    if($num_val_atra==0){
                                        $clase_total='total-comprobante';
                                    }else{
                                        $clase_total='total-comprobante1';
                                    }
                                    ?>
                                <p class="<?php echo $clase_total;?> mb-0 pb-0" total_a_pagar="<?php echo $cantidad_aportacion_atrasada; ?>"><?php echo " S/. ". $cantidad_aportacion_atrasada; ?></p>
                                </div>
                            </div>
                        <?php
                    }*/

                    ?>
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 c-total-pago c-total-pago2 row f-gris pl-0 pr-0 ml-0 mr-0">

                        <form class="c-img-comprobante d-flex col-12" id="form-adel-pago" method="post" enctype="multipart/form-data">
                            
                            <div class="c-modal-img-boucher">
                                <div class="col-lg-5 col-md-8 col-sm-10 col-xs-10 c-img-boucher-pago mb-0 mt-5 pl-0 pr-0">
                                    <div class="col-12 d-flex c-titulo">
                                        <h4 class="text-center col-10 titulo-vista">Deposito/Transferencia</h4>
                                        <p class="btn-salir-noti col-2" >X</p>
                                        
                                    </div>
                                <!--<p class="btn-salir-noti" >X Salir</p>-->
                                    <div class="p-3">
                                        <div class="form-group">
                                            <label>Realice un deposito a la siguiente cuenta y coloque una imagen del comprobante (foto de Voucher/ScreenShot de Transferencia):</label>
                                            <p class="nro_cta">BCP: 245-97778041-0-22</p>
                                            <p class="nro_cta">o a Cci: 00224519777804102299</p>
                                        </div>
                                        <div class="img-boucher" id="img-boucher-id">
                                            <img src="<?php echo URL;?>client/img/ico-boucher.png" alt="">
                                        </div>
                                        <div class="form-group">
                                            <label class="btn-adjuntar-comp btn-primary mt-0 mb-0" for="boucher-id" >
                                                <img src="<?php echo URL;?>client/img/ico-subir.png" alt="">
                                            </label>
                                                <input type="file" name="boucher-id[]" id="boucher-id" style="display:none;" >
                                                
                                            </div>    
                                        <div class="form-group">
                                            <label for="">Coloque el numero de Operacion</label>
                                            <input class="text-center col-12" type="text" name="" id="nro_operacion" placeholder="Nro Operacion"/> 
                                        </div>  
                                        <?php
                                        /*if($num_val_atra!=0){
                                            $boton_guardar='';
                                            ?>
                                            <button <?php echo $boton_guardar; ?> type="button" class="btn btn-success btn-guardar-pago-total col-8 m-3" id_colegiado="<?php echo $id_colegiado; ?>" id_refinanciacion_atrasada="<?php echo $id_refinanciacion_atrasada;?>" fecha_nuevo_inicio_at="<?php echo $fecha_inicio_cuota_adel; ?>" fecha_actual_ultpago="<?php echo $fecha_actual_ultpago; ?>" monto_cuotas_pen_atra="<?php echo $monto_cuotas_pen_atra; ?>" >Registrar Pago</button>
                                            <?php
                                        }else{*/
                                            if($boton_guardar!=''){
                                                ?>
                                                <button type="button" class="btn btn-success btn-gen-ref" diferencia="<?php echo $diferencia; ?>" id_colegiado="<?php echo $id_colegiado; ?>">Generar</button>
                                                <?php
                                            }
                                            ?>
                                            <button <?php echo $boton_guardar; ?> type="button" class="btn btn-success btn-guardar-pago1 col-8 m-3" id_colegiado="<?php echo $id_colegiado; ?>" >Registrar pago (deposito transferencia)</button>  
                                            <?php
                                        //}
                                        ?>
                                        
                                    </div>
                                      

                                </div>
                            </div>

                        </form>
                        <?php
                        //if($num_val_atra==0){
                        ?>
                        <div class="c-fecha-inicio1 p-1 d-flex">
                            <?php
                             if($fecha_fin==''){
                                $fecha_fin=$fecha_suscripcion;
                             }
                            ?>
                            <label class="mb-0 mt-0" for="">Validez ult. Pago: </label>
                            <p class="mb-0"><?php echo date("d/m/Y", strtotime($fecha_fin)); ?></p>
                            <?php 
                            $mes_nuevo_final=date("m", strtotime($fecha_fin));
                            $year_nuevo_final=date("Y", strtotime($fecha_fin));
                            $mes_nuevo_final=$mes_nuevo_final+1;
                            
                            if($mes_nuevo_final>12){
                                $mes_nuevo_final=1;
                                $year_nuevo_final=$year_nuevo_final+1;
                            }

                            $fecha_nuevo_inicio="01/".zero_fill($mes_nuevo_final,2)."/".$year_nuevo_final;
                           
                            ?>
                        </div>
                        <div class="c-fecha-fin1">

                        </div>
                        <div class="c-lista-comprobante1 col-12">
                            <table class="table p-0">
                                <thead class="p-0">
                                    <tr class="p-0">
                                        <th class="p-0" scope="col">Nombre Concepto</th>
                                        <th class="p-0" scope="col">Cta. Desde</th>
                                        <th class="p-0" scope="col">Monto</th>
                                        <!--<th class="p-0" scope="col">Meses</th>-->
                                        <th class="p-0" scope="col">Cantidad</th>
                                        <th class="p-0" scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody class="p-0" id="lista-items-pago">
                                    <?php
                                    $num3=0;
                                    $query_con="SELECT * FROM `concepto_aportacion` AS cap 
                                    INNER JOIN `tipo_aportacion` AS tap 
                                    ON cap.`tipo_aportacion_id_tipo_aportacion`=tap.`id_tipo_aportacion` 
                                    WHERE cap.`estado_concepto_aportacion` = '1' AND
                                    tap.`id_tipo_aportacion` = '1'                              
                                    ORDER BY cap.`id_concepto_aportacion` DESC";

                                    $resultado_con =$conexion->query($query_con);


                                    /*consultar por cuota refinanciada*/
                                    
                                    $cont_c_ref=0;
                                    

                                    $query2="SELECT * FROM `aportacion` 
                                    WHERE `aportacion`.`colegiado_id_colegiado` = '$id_colegiado' 
                                    AND `aportacion`.`concepto_aportacion_id_concepto_aportacion`='2'
                                    AND `aportacion`.`estado_aportacion`='2'";
                                    $resultado2 =$conexion->query($query2);
                                    $row2=$resultado2->fetch_assoc();
                                    $id_aportacion=$row2['id_aportacion'];
                                    $fecha_gen_ref_partner=$row2['fecha_aportacion'];
                                    $estado_aportacion=$row2['estado_aportacion'];
                                   $fecha_xxx=$row2['fecha_inicio'];


                                    $query3="SELECT * FROM `aportacion` 
                                            WHERE `aportacion`.`aportacion_id_aportacion` = '$id_aportacion' 
                                            AND `aportacion`.`concepto_aportacion_id_concepto_aportacion`='3'
                                            AND `aportacion`.`estado_aportacion`='2'";
                                    $resultado3 =$conexion->query($query3);
                                    $num3=mysqli_num_rows($resultado3);
                                    $array_refin= Array();
                                    while ($row3=$resultado3->fetch_assoc()) {
                                       

                                        if($estado_aportacion==2){
                                            $array_refin[$cont_c_ref][0]=$row3['id_aportacion'];
                                            $array_refin[$cont_c_ref][1]=$row3['estado_aportacion'];
                                            $array_refin[$cont_c_ref][2]=$row3['cantidad_aportacion'];
                                            //$id_aportacion_cuota=$row3['id_aportacion'];
                                            //$estado_aportacion=$row3['estado_aportacion'];
                                            //$cantidad_aportacion=$row3['cantidad_aportacion'];
                                            $cont_c_ref++;
                                        }
                                    }
                                    ?>
                                    <div id="cont_c_ref" valor="<?php echo $cont_c_ref; ?>"></div>
                                    <?php
                                    $contador_cuotas_ref=0;
                                    while ($row_con=$resultado_con->fetch_assoc()) {
                                        $id_concepto_aportacion=$row_con['id_concepto_aportacion'];
                                        $duracion_meses=$row_con['duracion_meses'];
                                        $tipo_aportacion=$row_con['tipo_aportacion_id_tipo_aportacion'];
                                        $nombre_concepto_aportacion=$row_con['nombre_concepto_aportacion'];
                                        $cantidad_concepto_aportacion=$row_con['cantidad_concepto_aportacion'];

                                        /*definir valores de cuotas refinanciadas*/
                                        $id_aportacion_cuota=0;
                                        $cantidad_aportacion=0;
                                        $monto_cuota='';

                                        
                                        
                                        /*validacion fecha de inicio*/
                                        if($tipo_aportacion==1){
                                            $fecha_inicio="01/".$mes_max."/".$year_max;
                                        }else{
                                            $fecha_inicio="-";
                                        }
                                        
                                        /*validacion para mostrar concepto correspondiente*/
/*
                                        if($num_exis==0){
                                            if($id_concepto_aportacion==6){
                                                ?>
                                                    <tr class="class-item-pagar p-0" id_cuota_refinanciacion="<?php echo $id_aportacion_cuota."$".$cantidad_aportacion; ?>" duracion_meses="<?php echo $duracion_meses; ?>" tipo_aportacion="<?php echo $tipo_aportacion; ?>" id="id-item-pagar<?php echo $id_concepto_aportacion;?>" id_concepto_aportacion="<?php echo $id_concepto_aportacion;?>">
                                                        <th class="p-0" scope="row"><?php echo $nombre_concepto_aportacion; ?></th>
                                                        <td class="p-0"><p class="id_fecha_inicio" fecha_inicio="<?php echo $fecha_nuevo_inicio; ?>"><?php echo $fecha_nuevo_inicio; ?></p></td>
                                                        <td class="p-0"><p class="id_cantidad_concepto_aportacion mb-0" cantidad_concepto_aportacion="<?php echo $cantidad_concepto_aportacion;?>"><?php echo "S/. ".$cantidad_concepto_aportacion;?></p><?php echo $monto_cuota; ?></td>
                                                        <td class="p-0"><input class="input-nro-items" type="text" num_c_pend="<?php echo $num3; ?>" value="1" onkeypress="return validanum(event)" onchange="return validanum(event)" /></td>
                                                        <!--<td class="p-0"><input class="btn-del-item-pago btn btn-danger p-0" type="button" id_concepto_aportacion="<?php echo $id_concepto_aportacion; ?>" value="X"></td>-->
                                                    </tr>
                                                    <?php
                                                   
                                            }
                                        }else{
                                           
                                            if($diferencia<=12){
                                                if($tipo_aportacion==1 && $id_concepto_aportacion!=6 || $id_concepto_aportacion!=14){
                                                    $year_fecha_fin=date("Y",strtotime($fecha_fin."+ 1 month"));
                                                  
                                                    $cont_key=0;

                                                        if(($year_fecha_fin<=2019 && $id_concepto_aportacion==1) || ($year_fecha_fin>2019 && $id_concepto_aportacion==4)){
                                               
                                                            $query="SELECT * FROM `aportacion` AS ap
                                                            INNER JOIN `concepto_aportacion` AS cap
                                                            ON ap.`concepto_aportacion_id_concepto_aportacion`=cap.`id_concepto_aportacion`
                                                            WHERE ap.`colegiado_id_colegiado` = '$id_colegiado' 
                                                            AND (cap.`tipo_aportacion_id_tipo_aportacion` = '1' OR cap.`tipo_aportacion_id_tipo_aportacion` = '3') 
                                                            AND ap.`aportacion_id_aportacion` IS NULL
                                                            ORDER BY ap.`id_aportacion` DESC LIMIT 1";
                                                            
                                                            $resultado =$conexion->query($query);

                                                            $num=mysqli_num_rows($resultado);

                                                            $row=$resultado->fetch_assoc();
                                                            
                                                            $fecha_fin=$row['fecha_fin'];
                                                            $id_aportacion_fk=$row['aportacion_id_aportacion'];
                                                            $id_tipo_aportacion=$row['concepto_aportacion_id_concepto_aportacion'];
                                                        
                                                            if($fecha_fin!=NULL){
                                                                $fecha_fin=date("Y-m-d",strtotime($fecha_fin)); 
                                                            }else{
                                                                if($id_tipo_aportacion=='3'){
                                                                    $query1="SELECT * FROM `aportacion` 
                                                                    WHERE `aportacion`.`id_aportacion` = '$id_aportacion_fk'";
                                                                    $resultado1 =$conexion->query($query1);
                                                                    $num1=mysqli_num_rows($resultado1);
                                                                    $row1=$resultado1->fetch_assoc();
                                                                    $fecha_fin=$row1['fecha_fin'];
                                                                }
                                                                
                                                            }
                                                        
                                                            $url_boucher=$row['comprobante_id_comprobante'];

                                                            if($url_boucher=NULL){
                                                                $query_comp="SELECT * FROM `comprobante` WHERE `comprobante`.`id_comprobante` = '$url_boucher'";
                                                                $resultado_comp =$conexion->query($query_comp);
                                                        
                                                                while ($row_comp=$resultado_comp->fetch_assoc()) {
                                                                    $url_boucher=$row_comp['url_comprobante'];
                                                                }
                                                            }          

                                                            $estado_aportacion = $row['estado_aportacion'];            
                                                            $fecha_actual=date("Y-m-d"); 
                                                            $monto_total_deuda=0;

                                                           
                                                            $array_cuotas= Array();

                                                            if($num>0){
                                                                $mes_fin=date("m",strtotime($fecha_fin)); 
                                                                $year_fin=date("Y",strtotime($fecha_fin)); 
                                                                
                                                            
                                                                $query_val="SELECT * FROM `aportacion` AS ap
                                                                INNER JOIN `concepto_aportacion` AS cap
                                                                ON ap.`concepto_aportacion_id_concepto_aportacion`=cap.`id_concepto_aportacion`
                                                                WHERE ap.`colegiado_id_colegiado` = '$id_colegiado' 
                                                                AND cap.`tipo_aportacion_id_tipo_aportacion` = '3'
                                                                AND ap.`aportacion_id_aportacion` IS NULL
                                                                AND ap.`estado_aportacion` = '2'";
                                                                $resultado_val =$conexion->query($query_val);
                                                                $num_val=mysqli_num_rows($resultado_val);
                                                                
                                                                if($estado_aportacion == 1 ){

                                                                    
                                                                        if($fecha_actual>$fecha_fin && $fecha_fin!=NULL){
                                                                            $mes_fin++;
                                                                            if($mes_fin>=12){
                                                                                $mes_fin=1; 
                                                                                $year_fin++;
                                                                            }
                                                                            $fechaComoEntero = strtotime($fecha_fin);
                                                                            //$mes_fin = date("m", $fechaComoEntero);
                                                                            //$year_fin = date("Y", $fechaComoEntero);
                                                                            $mes_actual = date("m");
                                                                            $year_actual = date("Y");
                                                                            
                                                                            //echo $fecha_fin."//".$mes_actual."//".$year_actual."====";
                                                                            $meses_reales = Array();
                                                                            $meses_reales = calculo_meses_atrazados($mes_fin,$year_fin,$mes_actual,$year_actual,$diferencia,$conexion);
                                                                            $size_array = count($meses_reales);
                                                                        
                                                                            foreach ($meses_reales as $key => $value) {
                                                                                
                                                                                $monto_total_deuda=$monto_total_deuda+$meses_reales[$key][2];
                                                                                $array_cuotas[$key]=$meses_reales[$key][2];
                                                                                
                                                                                
                                                                                $query="SELECT * FROM `concepto_aportacion` 
                                                                                WHERE `concepto_aportacion`.`tipo_aportacion_id_tipo_aportacion`='1' 
                                                                                AND `concepto_aportacion`.`estado_concepto_aportacion`='1'
                                                                                AND `concepto_aportacion`.`duracion_meses`='1'";
                                                                                $resultado =$conexion->query($query);
                                                                                $num=mysqli_num_rows($resultado);
                                                                                
                                                                                while ($row=$resultado->fetch_assoc()) {
                                                                                    $id_concepto_aportacion=$row['id_concepto_aportacion'];
                                                                                    
                                                                                    if($meses_reales[$key][1]<=2019 && $id_concepto_aportacion==1){
                                                                                        $cantidad_cuota=$row['cantidad_concepto_aportacion'];
                                                                                    }else if($meses_reales[$key][1]>2019 && $id_concepto_aportacion==4){
                                                                                        $cantidad_cuota=$row['cantidad_concepto_aportacion'];
                                                                                    }
                                                                                }
                                                                            
                                                                                $disabled_cantidad='';
                                                                                if(($key+1)<$size_array){
                                                                                    $disabled_cantidad='disabled';
                                                                                }

                                                                                
                                                                                if($contador_cuotas_ref < $cont_c_ref){
                                               
                                                                                    $id_aportacion_cuota=$array_refin[$contador_cuotas_ref][0];
                                                                                    $estado_aportacion=$array_refin[$contador_cuotas_ref][1];
                                                                                    $cantidad_aportacion=$array_refin[$contador_cuotas_ref][2];
                                                                                    $cantidad_concepto_aportacion=10;
                                                                                    $monto_cuota='<p class="can-cuota-ref mb-0" title="Cuota de Refinanciacion" >S/. '.$cantidad_aportacion.'</p>';
                                                                                    $contador_cuotas_ref++;
                                                                                    
                                                                                }else{
                                                                                    $id_aportacion_cuota=0;
                                                                                    $estado_aportacion=0;
                                                                                    $cantidad_aportacion=0;
                                                                                    $monto_cuota='';
                                                                                    
                                                                                }
                                                                              
                                                                                //echo $meses_reales[$key][0]."----".$meses_reales[$key][1];
                                                                                if($meses_reales[$key][0]>12){
                                                                                    $meses_reales[$key][0]=1;
                                                                                    $meses_reales[$key][1]=$meses_reales[$key][1]+1;
                                                                                }          

                                                                               
                                                                               $year_gen_ref_partner=date("Y",strtotime($fecha_gen_ref_partner));

                                                                               if($year_gen_ref_partner<=2019 && $monto_cuota!=''){
                                                                                   $meses_reales[$key][2]=10;
                                                                               }

                                                                                if($id_tipo_aportacion!=2){
                                                                        
                                                                                    $fecha_nuevo_inicio="01/".zero_fill($meses_reales[$key][0],2)."/".$meses_reales[$key][1];
                                                                                }else{
                                                                                    $meses_reales[$key][0]=$meses_reales[$key][0]+1;
                                                                                    if($meses_reales[$key][0]>12){
                                                                                        $meses_reales[$key][0]=1;
                                                                                        $meses_reales[$key][1]=$meses_reales[$key][1]+1;
                                                                                    }
                                                                                    
                                                                                }
                                                                                $fecha_nuevo_inicio="01/".zero_fill($meses_reales[$key][0],2)."/".$meses_reales[$key][1];
                                                                                ?>
                                                                                <tr class="class-item-pagar p-0" id_cuota_refinanciacion="<?php echo $id_aportacion_cuota."$".$cantidad_aportacion; ?>" duracion_meses="<?php echo $duracion_meses; ?>" tipo_aportacion="<?php echo $tipo_aportacion; ?>" id="id-item-pagar<?php echo $id_concepto_aportacion;?>" id_concepto_aportacion="<?php echo $id_concepto_aportacion;?>">
                                                                                    <th class="p-0" scope="row"><?php echo $nombre_concepto_aportacion; ?></th>
                                                                                    <td class="p-0"><p class="id_fecha_inicio" fecha_inicio="<?php echo $fecha_nuevo_inicio; ?>"><?php echo $fecha_nuevo_inicio; ?></p></td>
                                                                                    <?php
                                                                                    //echo $monto_cuota;
                                                                                    ?>
                                                                                    <td class="p-0"><p class="id_cantidad_concepto_aportacion mb-0" cantidad_concepto_aportacion="<?php echo $meses_reales[$key][2];?>"><?php echo "S/. ".$meses_reales[$key][2];?></p><?php echo $monto_cuota; ?></td>
                                                                                    <td class="p-0"><input <?php echo $disabled_cantidad; ?> class="input-nro-items" type="text" num_c_pend="<?php echo $num3; ?>" value="1" onkeypress="return validanum(event)" onchange="return validanum(event)" /></td>
                                                                                   <!-- <td class="p-0"><input class="btn-del-item-pago btn btn-danger p-0" type="button" id_concepto_aportacion="<?php echo $id_concepto_aportacion; ?>" value="X"></td>-->
                                                                                </tr>
                                                                                <!--<div class="form-check">
                                                                                    <input disabled checked class="form-check-input mes-ref" type="checkbox" value="<?php echo $cantidad_cuota;?>" name="mes-atra-ref[]" cantidad="<?php echo count($meses_reales);?>" posicion="<?php echo $key;?>" mes="<?php echo $meses_reales[$key][0];?>" year="<?php echo $meses_reales[$key][1];?>" id="mes-atra-ref<?php echo $key; ?>" <?php if($key!=0){echo "disabled";}?>>
                                                                                    <label disabled class="form-check-label label-filtro-checkbox" for="mes-atra-ref<?php echo $key; ?>">
                                                                                        <?php echo reemplazo_meses($meses_reales[$key][0])."-".$meses_reales[$key][1];?> 
                                                                                    </label>
                                                                                </div>-->
                                                                                <?php
                                                                            }
                                                                    
                                                                    }else{
                                                                        //echo '<p class="text-center m-sin-registros" >Aun tienes un refinanciacion pendiente de pago</p>';
                                                                    }                
                                                                /*}else{
                                                                    //echo '<p class="text-center m-sin-registros" >Antes de Refinanciar tienes que dar de alta tus cuotas pendientes de Revision</p>';
                                                                }           
                                                            }else{
                                                                $query1="SELECT * FROM `colegiado` WHERE `colegiado`.`id_colegiado` = '$id_colegiado' ";
                                                                $resultado1 =$conexion->query($query1);
                                                                $row1=$resultado1->fetch_assoc();
                                                        
                                                                $fecha_suscripcion = $row1['fecha_suscripcion'];

                                                                
                                                                    $fechaComoEntero = strtotime($fecha_suscripcion);
                                                                    $mes_fin = date("m", $fechaComoEntero);
                                                                    $year_fin = date("Y", $fechaComoEntero);
                                                                    $mes_actual = date("m");
                                                                    $year_actual = date("Y");
                                                                    //echo "***".$mes_fin;
                                                                    $meses_reales = Array();
                                                                    $meses_reales = calculo_meses_atrazados($mes_fin,$year_fin,$mes_actual,$year_actual,$diferencia,$conexion);
                                                                    $size_array = count($meses_reales);
                                                                
                                                                    foreach ($meses_reales as $key => $value) {
                                                                    
                                                                        $monto_total_deuda=$monto_total_deuda+$meses_reales[$key][2];
                                                                        $array_cuotas[$key]=$meses_reales[$key][2];
                                                                        

                                                                        $query="SELECT * FROM `concepto_aportacion` 
                                                                        WHERE `concepto_aportacion`.`tipo_aportacion_id_tipo_aportacion`='1'";
                                                                        $resultado =$conexion->query($query);
                                                                        
                                                                        while ($row=$resultado->fetch_assoc()) {
                                                                            $id_concepto_aportacion=$row['id_concepto_aportacion'];
                                                                            
                                                                            if($meses_reales[$key][1]<=2019 && $id_concepto_aportacion==1){
                                                                                $cantidad_cuota=$row['cantidad_concepto_aportacion'];
                                                                            }else if($meses_reales[$key][1]>2019 && $id_concepto_aportacion==4){
                                                                                $cantidad_cuota=$row['cantidad_concepto_aportacion'];
                                                                            }
                                                                        }

                                                                        $disabled_cantidad='';
                                                                        if(($key+1)<$size_array){
                                                                            $disabled_cantidad='disabled';
                                                                        }

                                                  
                                                                        if($contador_cuotas_ref < $cont_c_ref){
                                               
                                                                            $id_aportacion_cuota=$array_refin[$contador_cuotas_ref][0];
                                                                            $estado_aportacion=$array_refin[$contador_cuotas_ref][1];
                                                                            $cantidad_aportacion=$array_refin[$contador_cuotas_ref][2];
                                                                            //$cantidad_concepto_aportacion=10;
                                                                            $monto_cuota='<p class="can-cuota-ref mb-0" title="Cuota de Refinanciacion" >S/. '.$cantidad_aportacion.'</p>';
                                                                            $contador_cuotas_ref++;
                                                                            
                                                                        }else{
                                                                            $id_aportacion_cuota=0;
                                                                            $estado_aportacion=0;
                                                                            $cantidad_aportacion=0;
                                                                            $monto_cuota='';
                                                                            
                                                                        }
                                                                       
                                                                       

                                                                        
                                                                       $year_gen_ref_partner=date("Y",strtotime($fecha_gen_ref_partner));

                                                                       if($year_gen_ref_partner<=2019 && $monto_cuota!=''){
                                                                           $meses_reales[$key][2]=10;
                                                                       }

                                                                        
                                                                        $fecha_nuevo_inicio="01/".zero_fill($meses_reales[$key][0],2)."/".$meses_reales[$key][1];
                                                                        ?>
                                                                        <tr class="class-item-pagar p-0" id_cuota_refinanciacion="<?php echo $id_aportacion_cuota."$".$cantidad_aportacion; ?>" duracion_meses="<?php echo $duracion_meses; ?>" tipo_aportacion="<?php echo $tipo_aportacion; ?>" id="id-item-pagar<?php echo $id_concepto_aportacion;?>" id_concepto_aportacion="<?php echo $id_concepto_aportacion;?>">
                                                                            <th class="p-0" scope="row"><?php echo $nombre_concepto_aportacion; ?></th>
                                                                            <td class="p-0"><p class="id_fecha_inicio" fecha_inicio="<?php echo $fecha_nuevo_inicio; ?>"><?php echo $fecha_nuevo_inicio; ?></p></td>
                                                                            <?php
                                                                                    echo $monto_cuota;
                                                                                    ?>
                                                                            <td class="p-0"><p class="id_cantidad_concepto_aportacion mb-0" cantidad_concepto_aportacion="<?php echo $meses_reales[$key][2];?>"><?php echo "S/. ".$meses_reales[$key][2];?></p><?php echo $monto_cuota; ?></td>
                                                                            <td class="p-0"><input <?php echo $disabled_cantidad; ?> class="input-nro-items" type="text" num_c_pend="<?php echo $num3; ?>" value="1" onkeypress="return validanum(event)" onchange="return validanum(event)" /></td>
                                                                            <!--<td class="p-0"><input class="btn-del-item-pago btn btn-danger p-0" type="button" id_concepto_aportacion="<?php echo $id_concepto_aportacion; ?>" value="X"></td>-->
                                                                        </tr>
                                                                            <!--<div class="form-check">
                                                                                <input disabled checked class="form-check-input mes-ref" type="checkbox" value="<?php echo $cantidad_cuota;?>" name="mes-atra-ref[]" cantidad="<?php echo count($meses_reales);?>" posicion="<?php echo $key;?>" mes="<?php echo $meses_reales[$key][0];?>" year="<?php echo $meses_reales[$key][1];?>" id="mes-atra-ref<?php echo $key; ?>" <?php if($key!=0){echo "disabled";}?>>
                                                                                    <label disabled class="form-check-label label-filtro-checkbox" for="mes-atra-ref<?php echo $key; ?>">
                                                                                        <?php echo reemplazo_meses($meses_reales[$key][0])."-".$meses_reales[$key][1];?> 
                                                                                    </label>
                                                                            </div>-->
                                                                            <?php
                                                                    }
                                                                
                                                                
                                                                }
                                                        }
                                                        if($fecha_actual<=$fecha_fin && $id_concepto_aportacion==4){
                                                          
                                                            if($contador_cuotas_ref < $cont_c_ref){
                                               
                                                                $id_aportacion_cuota=$array_refin[$contador_cuotas_ref][0];
                                                                $estado_aportacion=$array_refin[$contador_cuotas_ref][1];
                                                                $cantidad_aportacion=$array_refin[$contador_cuotas_ref][2];
                                                                
                                                                $monto_cuota='<p class="can-cuota-ref mb-0" title="Cuota de Refinanciacion" >S/. '.$cantidad_aportacion.'</p>';
                                                                $contador_cuotas_ref++;
                                                                
                                                            }else{
                                                                $id_aportacion_cuota=0;
                                                                $estado_aportacion=0;
                                                                $cantidad_aportacion=0;
                                                                $monto_cuota='';
                                                                
                                                            }
                                                            
                                                            
                                                           $year_gen_ref_partner=date("Y",strtotime($fecha_gen_ref_partner));

                                                           if($year_gen_ref_partner<=2019 && $monto_cuota!=''){
                                                               $meses_reales[$cont_key][2]=10;
                                                           }

                                                            ?>
                                                                <tr class="class-item-pagar p-0" id_cuota_refinanciacion="<?php echo $id_aportacion_cuota."$".$cantidad_aportacion; ?>" duracion_meses="<?php echo $duracion_meses; ?>" tipo_aportacion="<?php echo $tipo_aportacion; ?>" id="id-item-pagar<?php echo $id_concepto_aportacion;?>" id_concepto_aportacion="<?php echo $id_concepto_aportacion;?>">
                                                                    <th class="p-0" scope="row"><?php echo $nombre_concepto_aportacion; ?></th>
                                                                    <td class="p-0"><p class="id_fecha_inicio" fecha_inicio="<?php echo $fecha_nuevo_inicio; ?>"><?php echo $fecha_nuevo_inicio; ?></p></td>
                                                                    <?php
                                                                    
                                                                                    
                                                                                    ?>
                                                                    <td class="p-0"><p class="id_cantidad_concepto_aportacion mb-0" cantidad_concepto_aportacion="<?php echo $cantidad_concepto_aportacion;?>"><?php echo "S/. ".$cantidad_concepto_aportacion;?></p><?php echo $monto_cuota; ?></td>
                                                                    <td class="p-0"><input class="input-nro-items" type="text" num_c_pend="<?php echo $num3; ?>" value="1" onkeypress="return validanum(event)" onchange="return validanum(event)"/></td>
                                                                    <!--<td class="p-0"><input class="btn-del-item-pago btn btn-danger p-0" type="button" id_concepto_aportacion="<?php echo $id_concepto_aportacion; ?>" value="X"></td>-->
                                                                </tr>
                                                                <?php
                                                        }             
                                
                                                }
                                            }else{
                                                ?>
                                                
                                                <?php
                                            }
                                        } */
                                        
                                    }
                                    ?>
                                    
                                </tbody>
                            </table>
                        </div>
                        <div class="col-12 c-t-pagar1">
                                
                                <div class="form-group">
                                    <label class="mb-0 pb-0 etiq-total" for="">TOTAL: </label>
                                    <?php
                                    $clase_total='';
                                    
                                        $clase_total='total-comprobante';
                                   
                                    ?>
                                <p class="<?php echo $clase_total; ?> mb-0 pb-0" ><?php echo " S/. ". $cantidad_aportacion_atrasada; ?></p>
                                </div>
                            </div>
                        <?php
                //}
                ?>
                     </div>
                   
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pl-0 pr-0">
                <button  class="btn-modal-img-boucher btn btn-success col-12 m-0">Registrar Pago (Deposito/Transferencia)</button>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pl-0 pr-0">
                <?php
                            
                ?>
                <button  type="button" disabled class="btn btn-primary btn-emergencia1 col-12 m-0" id_colegiado="<?php echo $id_colegiado; ?>" >Pago con Tarjeta (Opcion Deshabilitada Temporalmente)</button>
                <?php
             
                ?>
            </div>
             
        </div>
        <div class="c-modal-pago">
            <div class="col-lg-6 col-md-8 col-sm-10 mt-3 c-btn-cerrar-modal-pago">
                <input type="button" class="btn btn-danger btn-cerrar-modal-pago" value="X">
            </div>
            
    <div class="modal-pago col-lg-6 col-md-8 col-sm-10 pb-3" id="modal-pago-id">
   
   
    </div>
</div>
        <?php
        function calculo_meses_atrazados($mes_inicio,$year_inicio,$mes_fin,$year_fin,$diferencia_meses,$conexion){
            $meses_atra = Array();
            $cantidad_meses = 0;

            if($year_fin == $year_inicio){
                $cantidad_meses = ($mes_fin - $mes_inicio) + 1; 
            }else if($year_inicio < $year_fin){
                $diferencia = $year_fin - $year_inicio;
                if($diferencia == 1){
                    $cantidad_meses = ((12 - $mes_inicio) + 1) + $mes_fin; 
                }else if($diferencia > 1){
                    $cantidad_meses = (($diferencia - 1) * 12) + ((12 - $mes_inicio) + 1) + $mes_fin;
                }
            }

            /*VALIDACION CANTIDAD DE MESES SEGUN CASO*/
            if($diferencia_meses >= 13 && $diferencia_meses <= 48){
                $cantidad_meses = $cantidad_meses - 1;
            }else if($diferencia_meses >= 49 && $diferencia_meses <= 144){
                $cantidad_meses = $cantidad_meses - 1;
            }else if($diferencia_meses >= 145){
                $cantidad_meses = $cantidad_meses - 5;
            }

            $i = 0;
            //echo "****".$mes_inicio;
            $cont_meses = $mes_inicio;
            $cont_year = $year_inicio;

            while($i < $cantidad_meses){
                if($cont_meses == 12){
                    

                    $meses_atra[$i][0] = $cont_meses;
                    $meses_atra[$i][1] = $cont_year;

                    if($cont_year<=2019){
                        $meses_atra[$i][2]=10;
                    }else{
                        $meses_atra[$i][2]=15;
                    }

                    $cont_meses = 1;
                    $cont_year++;

                }else{
                    

                    $meses_atra[$i][0] = $cont_meses;
                    $meses_atra[$i][1] = $cont_year;

                    if($cont_year<=2019){
                        $meses_atra[$i][2]=10;
                    }else{
                        $meses_atra[$i][2]=15;
                    }

                    $cont_meses++;
                }

                

                $i++;
            }

            return $meses_atra;
        }

        //reemplazo meses numeros a letras
        function reemplazo_meses($mes){
            /** NOMBRANDO MES */
            switch ($mes) {
                case '01':
                    $mes='Enero';
                    break;
                    case '02':
                    $mes='Febrero';
                    break;
                    case '03':
                    $mes='Marzo';
                    break;
                    case '04':
                    $mes='Abril';
                    break;
                    case '05':
                    $mes='Mayo';
                    break;
                    case '06':
                    $mes='Junio';
                    break;
                    case '07':
                    $mes='Julio';
                    break;
                    case '08':
                    $mes='Agosto';
                    break;
                    case '09':
                    $mes='Setiembre';
                    break;
                    case '10':
                    $mes='Octubre';
                    break;
                    case '11':
                    $mes='Noviembre';
                    break;
                    case '12':
                    $mes='Diciembre';
                    break;
                default:
                    $mes='ERROR';
                    break;
            }
            return $mes;
        }


        ?>        



    </div> 
    
</div>