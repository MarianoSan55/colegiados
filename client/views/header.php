<head><meta charset="">
    <?php include_once "config.php";?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">  
    <link rel="stylesheet" href="<?php echo URL;?>client/css/bootstrap.min.css">  
    <link rel="stylesheet" href="<?php echo URL;?>client/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo URL;?>client/css/estilos.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    
    <title>Zona Colegiados | Colegio de Economistas</title>
     <div class="c-mensaje center-block">
            <p class="mensaje-rojo col-lg-6 col-md-8 col-sm-10">Usuario registrado correctamente</p>
        </div>
        <div class="c-form-recu center-block ">
            <div class="form-recu pr-3 pl-3 pt-3 pb-3">
                
                <div class="form-group">
                    <p>Se le enviara su usuario y contraseña al correo electronico con el que este registrado en nuestra base de datos.</p>
                    <label for="exampleInputEmail1">Coloque su Correo electrónico:</label>
                    <input type="text" name="correo-recu"  class="form-control" id="correo-recu" aria-describedby="emailHelp" placeholder="Correo Electrónico">
                </div>
                <div class="col-md-12 text-center ">
                    <button type="button" class=" btn btn-block mybtn btn-primary tx-tfm color-guinda btn-enviar-recu">Enviar</button>
                </div>
                <div class="form-group">
                    <p class="text-center"><a href="#" class="cerrar-form-recu" id="">Cerrar</a></p>
                </div>
            </div>
        </div>
        <div class="c-form-adelanto">
        
        </div>

        <!--<div class="modal-aviso">
            <div class="aviso pl-0 pr-0 col-lg-6 col-md-7 col-sm-10 mt-5 text-center">
                <p class="alert alert-warning text-center">
                Comunicado
                </p>
                <p class="pb-2">
                    Estamos realizando actualizaciones en nuestro sistema, en la proximas 24 horas estaremos reestableciendo el acceso de forma habitual agradecemos la comprensión.
                    <br>
                    <br>
                    <strong>Colegio de Economistas de Cajamarca</strong> 
                </p>
                
            </div>
        </div>-->
</head>
