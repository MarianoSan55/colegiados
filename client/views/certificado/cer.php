<?php

include_once "../../../server/conex.php";


$query_cer="SELECT * FROM `aportacion` AS ap
            INNER JOIN `colegiado` AS col
            ON ap.`colegiado_id_colegiado` = col.`id_colegiado`
            WHERE ap.`id_aportacion`='$id_aportacion'";
$resultado_cer =$conexion->query($query_cer);
$row_cer=$resultado_cer->fetch_assoc();
$id_colegiado=$row_cer['id_colegiado'];
$nombe_colegiado=$row_cer['nombre_colegiado'];
$apellido_paterno=$row_cer['apellido_paterno'];
$apellido_materno=$row_cer['apellido_materno'];
$estado_colegiado=$row_cer['estado_colegiado'];
$nro_colegiatura=$row_cer['nro_colegiatura'];
$fecha_suscripcion=$row_cer['fecha_suscripcion'];
$fecha_aportacion=$row_cer['fecha_aportacion'];

$fecha_inicio_cert=$row_cer['fecha_inicio'];
$fecha_fin_cert=$row_cer['fecha_fin'];

$cantidad_aportacion=$row_cer['cantidad_aportacion'];
$id_comprobante=$row_cer['comprobante_id_comprobante'];
/*obtener validez de certificado*/
$datetime1=new DateTime($fecha_inicio_cert);
                            $datetime2=new DateTime($fecha_fin_cert);
                            
                            # obtenemos la diferencia entre las dos fechas
                            $interval=$datetime2->diff($datetime1);
                            
                            # obtenemos la diferencia en meses
                            $intervalMeses=$interval->format("%m");
                            
                            # obtenemos la diferencia en años y la multiplicamos por 12 para tener los meses
                            $intervalAnos = $interval->format("%y")*12;
                                        
                            $diferencia= $intervalMeses+$intervalAnos+1;
                            if($diferencia<=1){
                                $diferencia=30;
                            }else if($diferencia>=2 && $diferencia<3){
                                $diferencia=60;
                            }else {
                                $diferencia=90;
                            }

$nombre_completo=$apellido_paterno." ".$apellido_materno." ".$nombe_colegiado;

/*determinar numero*/
$contador_ini=0;
$query4="SELECT * FROM `aportacion` WHERE `concepto_aportacion_id_concepto_aportacion` = 8 ORDER BY  `fecha_aportacion`";
$resultado4 =$conexion->query($query4);

$id_aportacion4=0;

while($row4=$resultado4->fetch_assoc()){
    $id_aportacion4=$row4['id_aportacion'];
    $fecha4=$row4['fecha_aportacion'];
    $year_certificado=date("Y",strtotime($fecha4));
    if($id_aportacion!=$id_aportacion4){
        $contador_ini++;
    }else{
    break;
    }
    
}
$contador_ini=$contador_ini+1;

/*obtener fecha final */
$num2=0;
if($cantidad_aportacion==0){
    $query2="SELECT * FROM `aportacion` 
        WHERE `aportacion`.`colegiado_id_colegiado`='$id_colegiado' 
        AND `aportacion`.`comprobante_id_comprobante`='$id_comprobante'
        AND `aportacion`.`concepto_aportacion_id_concepto_aportacion`='5'";
    $resultado2 =$conexion->query($query2);
    $num2=mysqli_num_rows($resultado2);
    $row2=$resultado2->fetch_assoc();
    $fecha_fin=$row2['fecha_fin'];
}

if($num2==0){
    $query1="SELECT * FROM `aportacion` 
        WHERE `aportacion`.`colegiado_id_colegiado`='$id_colegiado' 
        AND `aportacion`.`colegiado_id_colegiado` IS NOT NULL 
        AND `aportacion`.`estado_aportacion`='1' 
        AND (`aportacion`.`concepto_aportacion_id_concepto_aportacion` = '1'
                                    OR `aportacion`.`concepto_aportacion_id_concepto_aportacion` = '4'
                                    OR `aportacion`.`concepto_aportacion_id_concepto_aportacion` = '5'
                                    OR `aportacion`.`concepto_aportacion_id_concepto_aportacion` = '6'
                                    OR `aportacion`.`concepto_aportacion_id_concepto_aportacion` = '16') 
        ORDER BY `aportacion`.`id_aportacion` DESC LIMIT 1
        ";
    $resultado1 =$conexion->query($query1);
    $num=mysqli_num_rows($resultado1);


    $fecha_actual=date("Y-m-d");
    if($num>0){     
        while ($row1=$resultado1->fetch_assoc()) {
            $id_aportacion_anterior=$row1['id_aportacion'];
            /*if($id_aportacion_anterior<$id_aportacion){*/
                $fecha_fin=$row1['fecha_fin'];
            /*} */       
        }
    }else{
        $fecha_fin=$fecha_suscripcion;
    }
}


/*añadir un mes */
$year_fin = date("Y",strtotime($fecha_fin));
$mes_fin = date("m",strtotime($fecha_fin));

$nuevo_mes_fin=$mes_fin+1;
$nuevo_year_fin=$year_fin;
if($nuevo_mes_fin>12){
    $nuevo_mes_fin=1;
    $nuevo_year_fin=$year_fin+1;
}

$nuevo_dia_fin= date("d",(mktime(0,0,0,$nuevo_mes_fin+1,1,$nuevo_year_fin)-1));
$nueva_fecha_fin=$nuevo_year_fin."-".zero_fill($nuevo_mes_fin,2)."-".$nuevo_dia_fin;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Constancia de Habilitación</title>
    <link rel="stylesheet" href="cer.css">
</head>
<body>
    <div class="general">
        <div class="cont">
            <img class="top" src="top.jpg" alt="">
            <h1>CERTIFICADO</h1>
            <br>
            <h3>
                DE HABILITACIÓN PROFESIONAL <br>
                Nro. <?php echo $contador_ini." - ".$year_certificado;?>  - CECCAJ
            </h3>
            <br style="margin-top: 20px;">
            <h4>Acredita que el (la) economista</h4>
            <h2><?php echo $nombre_completo; ?></h2>
            <br>
            <p class="parrafo">Identificado(a) con <strong>Colegiatura Nro <?php echo $nro_colegiatura?> </strong>	miembro de la orden del Colegio de  Economistas de  Cajamarca  y  como  tal  se  encuentra habilitado(a)  
            <?php
            if ($estado_colegiado==4) {
                echo "de forma VITALICIA";
            }else{
                ?>
                hasta el <strong><?php echo fechaCastellano($nueva_fecha_fin); ?></strong>  
                <?php
            }
            ?>
            
            <?php
            
            ?>
            en  el   ejercicio  de  la profesión   de  acuerdo a las leyes N° 15488 y 24531. </p> <br>
            <p class="parrafo">Se  otorga  el  presente, a  su  solicitud,  para  los fines  que  estime pertinente.</p> <br>
            <p class="parrafo">Este certificado, es valido por <strong><?php echo $diferencia;?></strong> días a  partir  de  la  fecha  de emisión.</p> <br>
            <p class="parrafo">Otorgado  en  la  ciudad  de  Cajamarca el día <?php echo fechaCastellano($fecha_aportacion); ?></p> <br>
            <p class="parrafo">Fecha de Colegiatura:<strong><?php echo fechaCastellano($fecha_suscripcion); ?></strong></p>
  
            <div class="subcont" style="text-align: center;">
                <?php
                if ($fecha_aportacion<='2022-12-31 23:59:59') {
                   ?>
                    <img class="firm" src="firma.png" alt="">
                    <?php  
                }else{
                    ?>
                    <img class="firm" src="firma1.jpeg" alt="">
                    <?php  
                }               
                
                ?>
            </div>
            <img class="foot" src="foot.png" alt="">
        </div>
    </div>
</body>
</html>