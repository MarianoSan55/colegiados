<?php 

include_once "../../server/conex.php";
if(isset($_POST['year'])){
    $year_now=$_POST['year'];
    $id_colegiado=$_POST['id_colegiado'];
}else{
    //$year_now=$year_filtro;
}

?>
<?php include_once "config.php";?>
<div class="c-cuotas container align-self-end col-lg-9 col-md-12 col-sm-12 col-xs-12 pt-5 pr-0 pl-5">    

    <div class="c-certificados">
        <h3 class="titulo-vista1 pt-3">Lista de Certificados generados</h3>
        <ul class="lista-cert mb-0">
            <?php
            $query2="SELECT * FROM `aportacion` 
                    WHERE `aportacion`.`colegiado_id_colegiado` = '$id_colegiado'
                    AND `aportacion`.`concepto_aportacion_id_concepto_aportacion` = '8' AND `aportacion`.`estado_aportacion` <> '3'";
            $resultado2 =$conexion->query($query2);
            $num2=mysqli_num_rows($resultado2);

            /******************************************ESTADO DE HABILIDAD*****************************************************/
            
                    $cant_deuda=0;
                    $query1="SELECT * FROM `aportacion` WHERE `aportacion`.`colegiado_id_colegiado`='$id_colegiado' AND `aportacion`.`colegiado_id_colegiado` IS NOT NULL AND `aportacion`.`estado_aportacion`='1' ORDER BY `aportacion`.`fecha_fin` DESC LIMIT 1 ";
                    $resultado1 =$conexion->query($query1);
                    $num=mysqli_num_rows($resultado1);
                    $row1=$resultado1->fetch_assoc();

                    $fecha_actual=date("Y-m-d");

                    if($row1>0){                        
                        $fecha_fin=$row1['fecha_fin'];
                    
                    }else{
                        $fecha_fin=$fecha_suscripcion;
                    }


                    $estado_deuda="";            

                    
                    /*añadir un mes */
                    $year_fin = date("Y",strtotime($fecha_fin));
                    $mes_fin = date("m",strtotime($fecha_fin));

                    $nuevo_mes_fin=$mes_fin+1;
                    $nuevo_year_fin=$year_fin;
                    if($nuevo_mes_fin>12){
                        $nuevo_mes_fin=1;
                        $nuevo_year_fin=$year_fin+1;
                    }
                    
                    $nuevo_dia_fin= date("d",(mktime(0,0,0,$nuevo_mes_fin+1,1,$nuevo_year_fin)-1));
                    $nueva_fecha_fin=$nuevo_year_fin."-".zero_fill($nuevo_mes_fin,2)."-".$nuevo_dia_fin;

                    $restriccion=0;

                    if($estado_colegiado==3){
                        $restriccion=0;
                    }else{
                       if($fecha_actual<=$nueva_fecha_fin){
                            $restriccion=1;
                        }else{
                            $cant_deuda++;
                            $restriccion=0;
                        } 
                    }

                    
            if($restriccion==1){
                if($num2>0){
                    while ($row2=$resultado2->fetch_assoc()) {
                        $id_aportacion_cert=$row2['id_aportacion'];
                        $fecha_aportacion_cert=$row2['fecha_aportacion'];
                        $id_comprobante=$row2['comprobante_id_comprobante'];

                        /*DETERMINAR ESTADO DE CERTIFICADO A TRA VEZ DE APORTACIONES RELACIONADAS*/
                        $query_fk="SELECT * FROM `aportacion` WHERE `aportacion`.`comprobante_id_comprobante`='$id_comprobante' AND `aportacion`.`estado_aportacion` = '3'";
                        $resultado_fk =$conexion->query($query_fk);
                        $num_fk=mysqli_num_rows($resultado_fk);
                       
                        if ($num_fk==0) {
                           ?>
                            <li class="item-cert">
                                <a target="_blank" href="../views/certificado/generar.php?id_aportacion=<?php echo $id_aportacion_cert;?>">Certificado generado(<?php echo date("d/m/Y",strtotime($fecha_aportacion_cert));?>)</a>
                            </li>
                            <?php
                        }else{
                            echo "<p class='mensaje-sin-cuotas pt-3 '>Usted no tiene ningun certificado disponible</p>"; 
                        }
                        
                    }
                }else{
                    echo "<p class='mensaje-sin-cuotas pt-3 '>Usted no tiene ningun certificado disponible</p>";
                }
            }else{
                echo "<p class='mensaje-sin-cuotas pt-3 '>Para poder descargar documentos, primero debe estar al dia en sus pagos</p>";
            }
            
            
            ?>
            
        </ul>
    </div>

    <div class="c-certificados">
        <h3 class="titulo-vista1 pt-3">Reporte de pagos</h3>
        <ul class="lista-cert mb-0">
            <li class="item-cert">
            <?php
                $url_descarga_ind='../../../intranet/server/descargar-reporte-individual.php?id_colegiado='.$id_colegiado;
               
                ?>
                <a target="_blank" href="<?php echo $url_descarga_ind; ?>" id_colegiado="<?php echo $id_colegiado;?>">Reporte</a>
                </tr>
            </li>            
        </ul>
    </div>

    <div class="c-certificados">
        <h3 class="titulo-vista1 pt-3">Documentos de Refinanciación</h3>
        <ul class="lista-cert mb-0">
            
            <?php
            $query_doc="SELECT * FROM `aportacion` 
                        WHERE `aportacion`.`colegiado_id_colegiado` = '$id_colegiado' 
                        AND `aportacion`.`concepto_aportacion_id_concepto_aportacion`='2'";
            $resultado_doc =$conexion->query($query_doc);
            $row_doc=$resultado_doc->fetch_assoc();
            $id_refinanciamiento=$row_doc['id_aportacion'];
            $num_doc=mysqli_num_rows($resultado_doc);
            if($num_doc){

                /**definir formato */
                $query_col="SELECT * FROM `aportacion` WHERE `aportacion`.`id_aportacion`='$id_refinanciamiento'";
                $resultado_col =$conexion->query($query_col);
                $row_col=$resultado_col->fetch_assoc();
                $id_colegiado_fer=$row_col['colegiado_id_colegiado'];

                $query_politica="SELECT * FROM `detalle_politica` 
                                WHERE `detalle_politica`.`colegiado_id_colegiado`='$id_colegiado_fer'
                                AND `detalle_politica`.`fecha_limite` IS NOT NULL";
                $resultado_politica =$conexion->query($query_politica);
                $num_politica=mysqli_num_rows($resultado_politica);

                if($num_politica>0){
                    $url_descarga_ind='../../client/views/refinanciamiento/generar_covid.php?id_aportacion='.$id_refinanciamiento;
                }else{
                    $url_descarga_ind='../../client/views/refinanciamiento/generar.php?id_aportacion='.$id_refinanciamiento;
                }

                
                $stile_ref='';
                if(isset($_GET['id_ref'])){
                    $stile_ref='item-doc-ref';
                }else{
                    $stile_ref='item-cert';
                }

                ?>
                <li class="<?php echo $stile_ref;?>">
                <a target="_blank" href="<?php echo $url_descarga_ind; ?>" id_colegiado="<?php echo $id_colegiado;?>">Refinanciamiento Nro <?php echo $id_refinanciamiento; ?></a>
                </li> 
                </tr>
                <?php 
            }else{
                echo "<p class='mensaje-sin-cuotas pt-3 '>Usted no tiene ningun documento de refinanciacion</p>"; 
            }
                
                
                ?>
                       
        </ul>
    </div>

    
</div>