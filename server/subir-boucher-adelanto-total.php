<?php
include_once "../client/views/config.php";
if(isset($_FILES['boucher-id'])){
    
    $id_colegiado=$_POST['id_colegiado'];
    $nro_operacion=$_POST['nro_operacion'];

    $fecha_aportacion=date("Y-m-d H:i:s");

    $id_refinanciacion_atrasada=$_POST['id_refinanciacion_atrasada'];
    $fecha_nuevo_inicio_at=$_POST['fecha_nuevo_inicio_at'];
    $fecha_actual_ultpago=$_POST['fecha_actual_ultpago'];
    $monto_cuotas_pen_atra=$_POST['monto_cuotas_pen_atra'];
    $total_a_pagar=$_POST['total_a_pagar'];
  
    $reporte=null;
    for($x=0;$x<count($_FILES['boucher-id']['name']);$x++){
        $file=$_FILES['boucher-id'];
        $nombre=$file['name'][$x];
        $tipo=$file['type'][$x];
        $ruta_provicional=$file['tmp_name'][$x];
        $size=$file['size'][$x];
        $dimensiones=getimagesize($ruta_provicional);
        $width=$dimensiones[0];
        $height=$dimensiones[1];
        $carpeta="../../colegiados/client/img/users/".$id_colegiado."/"."boucher/";
    }
    
    if(!file_exists($carpeta)){        
        mkdir($carpeta, 0777,true);
    }

    if($tipo!="image/jpg" && $tipo!="image/jpeg" && $tipo!="image/png" && $tipo!="image/JPG" && $tipo!="image/JPEG" && $tipo!="image/PNG" ){
        echo 0;
    }else{
        


            include_once "conex.php";
        

            /*REGISTRO DE COMPROBANTE*/
            $query3="INSERT INTO `comprobante` (`url_comprobante`, `fecha_reg_comprobante`, `nro_operacion`, `monto_comprobante`, `estado_comprobante`, `observacion_comprobante`, `colegiado_id_colegiado`,`metodo_pago_id_metodo_pago`) 
                    VALUES (NULL, '$fecha_aportacion', '$nro_operacion','$total_a_pagar','1','total','$id_colegiado','2');";
            $resultado3 =$conexion->query($query3);
            $query_id="SELECT @@identity AS id_comprobante";
            $resultado_id =$conexion->query($query_id);
            $row_id=$resultado_id->fetch_assoc();
            $id_aportacion_id = $row_id['id_comprobante'];

            $array_nombre=explode(".",$nombre);
                $archivo=$id_aportacion_id.".".$array_nombre[1];
                $scr=$carpeta.$archivo;

                $query4="UPDATE `comprobante` SET `url_comprobante` = '$archivo' 
                WHERE `comprobante`.`id_comprobante` = '$id_aportacion_id'; ";
            $resultado4 =$conexion->query($query4);

               /*DAR DE ALTA REFINANCIACION*/
                $query5="UPDATE `aportacion` SET 
                `estado_aportacion` = '1'
                WHERE `aportacion`.`id_aportacion` = '$id_refinanciacion_atrasada'; ";
                $resultado5 =$conexion->query($query5);

                $query6="UPDATE `aportacion` SET 
                `estado_aportacion` = '1',
                `comprobante_id_comprobante` = '$id_aportacion_id'
                WHERE `aportacion`.`aportacion_id_aportacion` = '$id_refinanciacion_atrasada' AND `aportacion`.`estado_aportacion`='2'; ";
                $resultado6 =$conexion->query($query6);


                /*registrar pago regular*/

                /*REGISTRO DE PAGO REGULAR OBLIGATORIO*/
                $nuevo_mes_fin=date("m",strtotime($fecha_actual_ultpago));
                $nuevo_year_fin=date("Y",strtotime($fecha_actual_ultpago));
                $nuevo_dia_fin= date("d",(mktime(0,0,0,$nuevo_mes_fin+1,1,$nuevo_year_fin)-1));
                        $nueva_fecha_fin=$nuevo_year_fin."-".zero_fill($nuevo_mes_fin,2)."-".$nuevo_dia_fin;
                   
                $query="INSERT INTO `aportacion` (`cantidad_aportacion`, `fecha_aportacion`, `fecha_inicio`, `fecha_fin`, `year`, `comprobante_id_comprobante`, `aportacion_id_aportacion`, `colegiado_id_colegiado`, `concepto_aportacion_id_concepto_aportacion`,`metodo_pago_id_metodo_pago`, `estado_aportacion`) 
                VALUES ('$monto_cuotas_pen_atra', '$fecha_aportacion', '$fecha_nuevo_inicio_at', '$nueva_fecha_fin', '$nuevo_year_fin', '$id_aportacion_id', NULL, '$id_colegiado', '4', '1', '3');";
                $resultado =$conexion->query($query);


            

            if($resultado && move_uploaded_file($ruta_provicional,$scr)){
                echo 1;
            }else{
                echo 0;
            }


    }
    
    
}

function zero_fill ($valor, $long = 0)
{
    return str_pad($valor, $long, '0', STR_PAD_LEFT);
}
?>