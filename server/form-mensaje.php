<?php

include_once "../../server/conex.php";
include_once "../../client/views/config.php";
$id_detalle_inbox=$_POST['id_detalle_inbox'];
$id_colegiado=$_POST['id_colegiado'];

/*

$query="";
$resultado =$conexion->query($query);
$num=mysqli_num_rows($resultado);
$row=$resultado->fetch_assoc();

$fecha_envio_detalle_inbox=$row['fecha_envio_detalle_inbox'];
$fecha_leido_detalle_inbox=$row['fecha_leido_detalle_inbox'];
$asunto_detalle_inbox=$row['asunto_detalle_inbox'];
$descripcion_detalle_inbox=$row['descripcion_detalle_inbox'];
$inbox_colegiado_id_inbox_colegiado=$row['inbox_colegiado_id_inbox_colegiado'];
$inbox_usuario_id_inbox_usuario=$row['inbox_usuario_id_inbox_usuario'];
$origen=$row['origen'];
*/



?>

<div>
    <div class="form-group groupo-inbox">
        <label for="">Buscar Colegiados a quienes desea enviar mensaje:</label>
        <select class="buscar-correo " id="">
            <?php
            $query="SELECT * FROM `usuario` ORDER BY `usuario`.`apellido_usuario` ASC";
            $resultado =$conexion->query($query);
            $num=mysqli_num_rows($resultado);
            

            while ($row=$resultado->fetch_assoc()) {
                $id_usuario=$row['id_usuario'];
                $nombre_usuario=$row['nombre_usuario'];
                $apellido_usuario=$row['apellido_usuario'];
                ?>
                <option class="item-buscar-correo" value="<?php echo $id_usuario."$".$apellido_usuario." ".$nombre_usuario; ?>"><?php echo $apellido_usuario." ".$nombre_usuario; ?></option>
                <?php
            }
            ?>
            
        </select>
    </div>
    <div class="form-group groupo-inbox">
        <label for="">Mensaje para:</label>
        <div class="c-lista-destino">

        </div>
    </div>
    <div class="form-group groupo-inbox">
        <label for="">Asunto:</label>
        <input type="text" class="col-12" id="asunto_detalle_inbox" placeholder="Asunto...">
    </div>
    <div class="form-group ">
        <textarea id="" class="textarea-email col-12" placeholder="Descripcion de mensaje..."></textarea>
    </div>
    <div class="form-group">
        <input class="btn btn-success btn-enviar-mensaje" id_colegiado="<?php echo $id_colegiado; ?>" id_detalle_inbox="<?php echo $id_detalle_inbox; ?>" type="button" value="Enviar">
    </div>
</div>