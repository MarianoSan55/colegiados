<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

<div class='c-email-recu' style=' width: 100%; text-align: center; text-align: center; font-family: Vegur, 'PT Sans', Verdana, sans-serif !important;'>
    <h3 style='background: #942231;color:#fff; font-weight: bold; padding: 10px; border-radius: 5px;'>Usuario y Contraseña</h3>
    <table>
        <tbody>
            <tr>
                <td><p>Su usuario es: </p></td>
                <td><label style='font-weight: bold;'>".$nickname_colegiado."</label></td>
            </tr>
            <tr>
                <td><p>Su Contraseña es: </p></td>
                <td><label style='font-weight: bold;'>".$clave_colegiado."</label></td>
            </tr>
        </tbody>
    </table>    
    <hr>
    <a href='http://economistascajamarca.org/colegiados/' style='border: none; color: #942231; text-decoration:none;'>Volver a Inicio de Sesión</a>
</div>