
            <?php
            include_once "../../server/conex.php";
             $id_colegiado=$_POST['id_colegiado'];
            ?>
            <div class="c-usuarios col-12 pl-0 pr-0">
                <div class="col-12 d-flex c-titulo">
                                        <h4 class="text-center col-10 titulo-vista">Buzón de Mensajería</h4>
                                        <p class="btn-salir-noti btn-salir-buzon col-2" >X</p>
                                        
                                    </div>
           

                <div class="modal-buzon">
                    <div class="c-modal-buzon col-8 container m-3 p-3" id="modal-buzon-id">
                        
                    </div>
                </div>

                <div class="modal-noti-confirm">
                    <div class="c-modal-noti-confirm col-8 m-3 p-3">
                        <h4 class="mensaje-del-noti"></h4>
                        <div class="">
                            <button type="submit" class="btn btn-success aceptar-del-noti" id_anuncio="0">Aceptar</button>
                            <button type="button" class="btn btn-danger btn-cerrar-confir-noti">Cerrar</button>
                        </div>
                    </div>
                </div>  

                <div class="c-lista-noti row pl-2 pr-3">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 bloque1-buzon" >
                        <div class="c-btn-nueva-noti col-12 pl-0 pr-0 mt-3 mb-2">
                            <input type="button" class="btn btn-primary btn-nueva-mensaje col-12" id_colegiado="<?php echo $id_colegiado;?>" id_detalle_inbox="0" value="+ Nuevo Mensaje">
                        </div>
                        <ul id_colegiado="<?php echo $id_colegiado; ?>" class="tag-buzon col-12 d-flex">

                            <li class="col-12 li-buzon" id-tag="entrada">
                                <div class="form-group mb-0">
                                    <select name="" class="form-control select-tag-buzon col-12" id="">
                                        <option value="1">Bandeja de Entrada</option>
                                        <option value="2">Bandeja de Salida</option>
                                    </select>
                                </div>                                
                            </li>
                        </ul>
                        <ul class="list-buzon" id="list-buzon-id">
                                <?php
                                $query="SELECT * FROM `detalle_inbox` AS din 
                                INNER JOIN `inbox_colegiado` AS incol 
                                ON din.`inbox_colegiado_id_inbox_colegiado`=incol.`id_inbox_colegiado` 
                                WHERE din.`origen`='usuario' AND incol.`colegiado_id_colegiado`='$id_colegiado' 
                                GROUP BY din.`asunto_detalle_inbox`
                                ORDER BY din.`fecha_envio_detalle_inbox` DESC";
                                $resultado =$conexion->query($query);
                                $num=mysqli_num_rows($resultado);

                                while ($row=$resultado->fetch_assoc()) {
                                    $id_detalle_inbox=$row['id_detalle_inbox'];
                                    $fecha_envio_detalle_inbox=$row['fecha_envio_detalle_inbox'];
                                    $fecha_leido_detalle_inbox=$row['fecha_leido_detalle_inbox'];
                                    
                                    $estado_leido='';
                                    if($fecha_leido_detalle_inbox=='' || $fecha_leido_detalle_inbox==NULL){
                                        $estado_leido='ico-estado-buzon';
                                    }else{
                                        $estado_leido='ico-estado-buzon-leido';
                                    }

                                    $asunto_detalle_inbox=$row['asunto_detalle_inbox'];
                                    $descripcion_detalle_inbox=$row['descripcion_detalle_inbox'];
                                    ?>
                                    <li class="item-li-buzon" id_detalle_inbox="<?php echo $id_detalle_inbox; ?>" id_tag="1" asunto_detalle_inbox="<?php echo $asunto_detalle_inbox; ?>">
                                        <div class="form-group d-flex">
                                            <div class="<?php echo $estado_leido;?>"></div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <p class="item-asunto"><?php echo $asunto_detalle_inbox;?></p>
                                            <label class="mb-0" for=""><?php echo date("d/m/Y h:i A",strtotime($fecha_envio_detalle_inbox));; ?></label>
                                        </div>
                                    </li>
                                    <?php
                                }
                                ?>
                            
                        </ul>
                    </div>
                    <div class="col-lg-9 col-md-9 p-2 col-sm-12 col-xs-12" id="c-form-mensaje-id">
                        <h3 class="text-center mensaje-form-vacio">Buzón de Mensajeria Interna</h3>
                        <div class="col-12 text-center mt-3">
                            <img class="col-4" src="../img/ico-vista-email.png" alt="">
                        </div>
                    </div>
                </div>
            </div>

 