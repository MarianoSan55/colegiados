<?php
include_once "conex.php";


if(isset($_FILES['boucher-id'])){
    
   $id_colegiado=$_POST['id_colegiado'];
   $nro_operacion=$_POST['nro_operacion'];
   $json_meses = json_decode(stripslashes($_POST['json_meses']));
   $json_years = json_decode(stripslashes($_POST['json_years']));
   $descuento = $_POST['descuento'];
   $monto = $_POST['monto'] - $descuento;
   $monto_bruto = $_POST['monto_bruto'];
   $fecha_contrato = $_POST['fecha_contrato'];
   $cuotas = $_POST['cuotas'];
   $mon_x_cuota = $_POST['mon_x_cuota'];
   $fecha_aportacion=date("Y-m-d H:i:s");
 
   $reporte=null;
   for($x=0;$x<count($_FILES['boucher-id']['name']);$x++){
       $file=$_FILES['boucher-id'];
       $nombre=$file['name'][$x];
       $tipo=$file['type'][$x];
       $ruta_provicional=$file['tmp_name'][$x];
       $size=$file['size'][$x];
       $dimensiones=getimagesize($ruta_provicional);
       $width=$dimensiones[0];
       $height=$dimensiones[1];
       $carpeta="../../colegiados/client/img/users/".$id_colegiado."/"."boucher/";
   }
   
   if(!file_exists($carpeta)){        
       mkdir($carpeta, 0777,true);
   }

   if($tipo!="image/jpg" && $tipo!="image/jpeg" && $tipo!="image/png" && $tipo!="image/JPG" && $tipo!="image/JPEG" && $tipo!="image/PNG" ){
       echo 0;
   }else{
         include_once "conex.php";

         /*GUARDAR COMPROBANTE Y ADELANTO*/
         $query3="INSERT INTO `comprobante` (`url_comprobante`, `fecha_reg_comprobante`, `nro_operacion`,`monto_comprobante`, `estado_comprobante`, `observacion_comprobante`, `colegiado_id_colegiado`,`metodo_pago_id_metodo_pago`) 
                    VALUES (NULL, '$fecha_aportacion', '$nro_operacion','$descuento','1',NULL,'$id_colegiado','2');";
            $resultado3 =$conexion->query($query3);
            $query_id="SELECT @@identity AS id_comprobante";
            $resultado_id =$conexion->query($query_id);
            $row_id=$resultado_id->fetch_assoc();
            $id_aportacion_id = $row_id['id_comprobante'];

            $array_nombre=explode(".",$nombre);
                $archivo=$id_aportacion_id.".".$array_nombre[1];
                $scr=$carpeta.$archivo;

                $query4="UPDATE `comprobante` SET `url_comprobante` = '$archivo' 
                WHERE `comprobante`.`id_comprobante` = '$id_aportacion_id'; ";
            $resultado4 =$conexion->query($query4);

            /*GENERAR FECHAS PARA GUARDAR ADELANTO*/
            /*añadir un mes */
            $year_fin = date("Y",strtotime($fecha_contrato));
            $mes_fin = date("m",strtotime($fecha_contrato));
            

            $nuevo_mes_fin=$mes_fin;
            $nuevo_year_fin=$year_fin;
                        
            $nuevo_dia_fin= date("d",(mktime(0,0,0,$nuevo_mes_fin+1,1,$nuevo_year_fin)-1));
            $nueva_fecha_fin=$nuevo_year_fin."-".zero_fill($nuevo_mes_fin,2)."-".$nuevo_dia_fin;

            $fecha_ini_adel=$year_fin."-".$mes_fin."-01";
            $fecha_fin_adel=$nueva_fecha_fin;

            $year_adel=date("Y",strtotime($fecha_ini_adel));

            

            /*GENERACION DE LA HABILIDAD GRATUITA*/

            /*añadir dos meses */
            $fecha_inicio_cert=date("Y-m-d",strtotime($fecha_contrato));

            $year_fin = date("Y",strtotime($fecha_inicio_cert));
            $mes_fin = date("m",strtotime($fecha_inicio_cert));

            $nuevo_mes_fin=$mes_fin+1;
            $nuevo_year_fin=$year_fin;
            if($nuevo_mes_fin>12){
                $nuevo_mes_fin=1;
                $nuevo_year_fin=$year_fin+1;
            }
            
            $nuevo_dia_fin= date("d",(mktime(0,0,0,$nuevo_mes_fin+1,1,$nuevo_year_fin)-1));
            $nueva_fecha_fin=$nuevo_year_fin."-".zero_fill($nuevo_mes_fin,2)."-".$nuevo_dia_fin;

            $fecha_inicio_cert=$year_fin."-".$mes_fin."-01";
            $fecha_fin_cert=$nueva_fecha_fin;

            $query_hab="INSERT INTO `aportacion` (`cantidad_aportacion`, `fecha_aportacion`, `fecha_inicio`, `fecha_fin`, `year`, `comprobante_id_comprobante`, `aportacion_id_aportacion`, `colegiado_id_colegiado`, `concepto_aportacion_id_concepto_aportacion`, `metodo_pago_id_metodo_pago`, `estado_aportacion`) 
            VALUES ('0', '$fecha_aportacion', '$fecha_inicio_cert','$fecha_fin_cert', NULL, '$id_aportacion_id', NULL, '$id_colegiado', '8', '1', '3');";
            $resultado_hab =$conexion->query($query_hab);

         /*GENERAR REFINANCIAMIENTO*/
         $matriz_cuotas = array();

         foreach($json_meses as $clave => $d){
            $matriz_cuotas[$clave][0] = $d;
         }

         foreach($json_years as $clave => $d){
            $matriz_cuotas[$clave][1] = $d;
         }
         $year = $matriz_cuotas[0][1];

         //echo $matriz_cuotas[0][0]."-".$matriz_cuotas[0][1];
         $cantidad=count($matriz_cuotas);

         $fecha_inicio_ref=$matriz_cuotas[0][1]."-".$matriz_cuotas[0][0]."-01";

         //armar fecha fin
         
         $month = $matriz_cuotas[0][1]."-".$matriz_cuotas[$cantidad-1][0];
         $aux = date('Y-m-d', strtotime("{$month} + 1 month"));
         $last_day = date('Y-m-d', strtotime("{$aux} - 1 day"));
         $day_fin_ref= substr($last_day,8,2); 
         $fecha_fin_ref=$matriz_cuotas[$cantidad-1][1]."-".$matriz_cuotas[$cantidad-1][0]."-".$day_fin_ref;

         /*VALIDAR MONTO EN 0*/
         if($mon_x_cuota!=0){
            $query2="INSERT INTO `aportacion` (`cantidad_aportacion`, `fecha_aportacion`, `fecha_inicio`, `fecha_fin`, `year`, `comprobante_id_comprobante`, `aportacion_id_aportacion`, `colegiado_id_colegiado`, `concepto_aportacion_id_concepto_aportacion`,`metodo_pago_id_metodo_pago`, `estado_aportacion`) 
                  VALUES ('$monto_bruto', NULL, '$fecha_inicio_ref', '$fecha_fin_ref', '$year', NULL, NULL, '$id_colegiado', '2', '1', '2');";
             $resultado2 =$conexion->query($query2);
            
             $query1="SELECT @@identity AS id_aportacion";
             $resultado1 =$conexion->query($query1);
             $row1=$resultado1->fetch_assoc();
             $id = $row1['id_aportacion'];

             if($id!=0){
                /*añadir dos meses */
                $fecha_inicio_cuotas=date("Y-m-d",strtotime($fecha_fin_adel));

                $year_fin = date("Y",strtotime($fecha_inicio_cuotas));
                $mes_fin = date("m",strtotime($fecha_inicio_cuotas));

                $nuevo_mes_fin=$mes_fin+1;
                $nuevo_year_fin=$year_fin;
                if($nuevo_mes_fin>12){
                    $nuevo_mes_fin=1;
                    $nuevo_year_fin=$year_fin+1;
                }
                
                $nuevo_dia_fin= date("d",(mktime(0,0,0,$nuevo_mes_fin+1,1,$nuevo_year_fin)-1));
                $nueva_fecha_fin=$nuevo_year_fin."-".zero_fill($nuevo_mes_fin,2)."-".$nuevo_dia_fin;

                $fecha_inicio_cuotas=$nuevo_year_fin."-".$nuevo_mes_fin."-01";

                /*mes actual*/
                $mes_actual=date('m',strtotime($fecha_inicio_cuotas));
                $year_actual=date('Y',strtotime($fecha_inicio_cuotas));
                for($i=0;$i<$cuotas;$i++){
                      //armar fecha inicio
                      $fecha_inicio=$year_actual."-".$mes_actual."-01";

                      //armar fecha fin
                   $day_fin = date("d", mktime(0,0,0, $mes_actual+1, 0, $year_actual));
                   $fecha_fin=$year_actual."-".$mes_actual."-".$day_fin;

                      $query="INSERT INTO `aportacion` (`cantidad_aportacion`, `fecha_aportacion`, `fecha_inicio`, `fecha_fin`, `year`, `comprobante_id_comprobante`, `aportacion_id_aportacion`, `colegiado_id_colegiado`, `concepto_aportacion_id_concepto_aportacion`, `metodo_pago_id_metodo_pago`, `estado_aportacion`) 
                      VALUES ('$mon_x_cuota', NULL, '$fecha_inicio', '$fecha_fin', '$year', NULL, '$id', '$id_colegiado', '3', '1', '2');";
                      $resultado =$conexion->query($query);

                      if($mes_actual==12){
                         $mes_actual=1;
                         $year_actual++;
                      }else{
                         $mes_actual++;
                      }
                      
                }
                /*GUARDAR ADELANTO*/
                $query="INSERT INTO `aportacion` (`cantidad_aportacion`, `fecha_aportacion`, `fecha_inicio`, `fecha_fin`, `year`, `comprobante_id_comprobante`, `aportacion_id_aportacion`, `colegiado_id_colegiado`, `concepto_aportacion_id_concepto_aportacion`, `metodo_pago_id_metodo_pago`, `estado_aportacion`) 
                                VALUES ('$descuento', '$fecha_aportacion', '$fecha_ini_adel', '$fecha_fin_adel', '$year_fin', '$id_aportacion_id', NULL, '$id_colegiado', '5', '1', '3');";
                        $resultado =$conexion->query($query);

                echo 1;
             }else{
                echo 0;
             }
         }else{
            /*GUARDAR CUOTA REGULAR*/
            $query="INSERT INTO `aportacion` (`cantidad_aportacion`, `fecha_aportacion`, `fecha_inicio`, `fecha_fin`, `year`, `comprobante_id_comprobante`, `aportacion_id_aportacion`, `colegiado_id_colegiado`, `concepto_aportacion_id_concepto_aportacion`, `metodo_pago_id_metodo_pago`, `estado_aportacion`) 
            VALUES ('$descuento', '$fecha_aportacion', '$fecha_inicio_ref', '$fecha_fin_adel', '$year_fin', '$id_aportacion_id', NULL, '$id_colegiado', '4', '1', '3');";
            $resultado =$conexion->query($query);
         }
         
           
         

         

           if($resultado3 && move_uploaded_file($ruta_provicional,$scr)){
               echo 1;
           }else{
               echo 0;
           }


   }
   
   
}

function zero_fill ($valor, $long = 0)
{
    return str_pad($valor, $long, '0', STR_PAD_LEFT);
}
?>