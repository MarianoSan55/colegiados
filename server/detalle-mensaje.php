<?php

include_once "../../server/conex.php";
include_once "../../client/views/config.php";
$asunto_detalle_inbox=$_POST['asunto_detalle_inbox'];
$id_detalle_inbox=$_POST['id_detalle_inbox'];
$id_tag=$_POST['id_tag'];

$query="SELECT * FROM `detalle_inbox` AS din 
        INNER JOIN `inbox_usuario` AS inusu 
        ON din.`inbox_usuario_id_inbox_usuario`=inusu.`id_inbox_usuario` 
        INNER JOIN `inbox_colegiado` AS incol 
        ON din.`inbox_colegiado_id_inbox_colegiado`=incol.`id_inbox_colegiado` 
        INNER JOIN `colegiado` AS col
        ON col.`id_colegiado`=incol.`colegiado_id_colegiado`
        INNER JOIN `usuario` AS usu
        ON usu.`id_usuario`=inusu.`usuario_id_usuario`
        INNER JOIN `tipo_usuario` AS tusu
        ON tusu.`id_tipo_usuario`=usu.`tipo_usuario`
        WHERE din.`asunto_detalle_inbox`='$asunto_detalle_inbox'";
       
$resultado =$conexion->query($query);
$num=mysqli_num_rows($resultado);

$nombre_colegiado='';
$nombre_usuario='';

while ($row=$resultado->fetch_assoc()) {
    $nombre_tipo_usuario=$row['nombre_tipo_usuario'];
    $nombre_colegiado.=$row['apellido_paterno']." ".$row['apellido_materno']." ".$row['nombre_colegiado'].", ";
    $nombre_usuario.="<label class='mb-0'>".$row['nombre_usuario']." ".$row['apellido_usuario']." - "."</label><label class='etiq-cargo mb-0'>".$nombre_tipo_usuario."</label>, ";

    $fecha_envio_detalle_inbox=$row['fecha_envio_detalle_inbox'];
    $fecha_leido_detalle_inbox=$row['fecha_leido_detalle_inbox'];
    $asunto_detalle_inbox=$row['asunto_detalle_inbox'];
    $descripcion_detalle_inbox=$row['descripcion_detalle_inbox'];
    $inbox_colegiado_id_inbox_colegiado=$row['inbox_colegiado_id_inbox_colegiado'];
    $inbox_usuario_id_inbox_usuario=$row['inbox_usuario_id_inbox_usuario'];
    $origen=$row['origen'];

}
?>

<div>
    <div class="form-group groupo-inbox mb-0 ">
            <label for="" class="mt-4 mb-0">Asunto: </label>   
        <h5 class="asunto mb-0"><?php echo $asunto_detalle_inbox; ?></h5>
    </div>
        <?php
        if($id_tag==1){
            ?>
            <div class="form-group groupo-inbox c-origen d-flex mb-0">
                <div class="col-9 d-flex">
                    <label for="" class="mb-0">De: </label>        
                    <p class="d-flex mb-0"><?php echo $nombre_usuario; ?></p>
                </div>
                <div class="col-3">
                   
                    <p class="mb-0"><?php echo date("d/m/Y h:i A",strtotime($fecha_envio_detalle_inbox)); ?></p>
                </div>
            </div>
            <?php

            /*actualizar estado de mensaje*/
            $fecha_actual=date("Y-m-d H:i:s");
            $query1="UPDATE `detalle_inbox` SET `fecha_leido_detalle_inbox` = '$fecha_actual' 
                    WHERE `detalle_inbox`.`id_detalle_inbox` = '$id_detalle_inbox'; ";
            $resultado1 =$conexion->query($query1);

        }else{
            ?>
            <div class="form-group groupo-inbox">
                <label for="">Destino: </label>        
                <p><?php echo $nombre_usuario; ?></p>
            </div>
            <?php
        }
        ?>
   
    
    <div class="form-group mt-3">
        
        <p><?php echo $descripcion_detalle_inbox; ?></p>
    </div>

</div>